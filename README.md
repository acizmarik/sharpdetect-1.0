# **SharpDetect**

This is an offline bytecode (CIL) instrumentation framework with aim to observe runtime behaviour of analysed .NET programs running under the CoreCLR. Bytecode re-writer is configurable and the whole tool is extensible with simple plugin API. SharpDetect currently contains a couple of analysis plugins that can be used to detect concurrency issues.

[![pipeline status](https://gitlab.com/acizmarik/sharpdetect-1.0/badges/master/pipeline.svg)](https://gitlab.com/acizmarik/sharpdetect/commits/master)

# Main Features
*  **IL-rewriting**: capturing information about runtime events
*  **Configurations**: configure which events to capture (based on event type and/or metadata information)
*  **Extensions**: plugin API to implement custom analysis plugins based on processing captured events. Some are preimplemented:
     * **Echo**: stringify all captured events
     * **Eraser**: implementation of the Eraser algorithm published by Savage et al. (1997). This is a lock-set analysis capable of predicting and detecting data-races.
     * **FastTrack**: implementation of the FastTrack algorithm published by Flanagan et al. (2009). This is a precise vector clock based algorithm capable of detecting data-races.

# State of Development
Project was mostly tested against .NET assemblies created from C#. Other languages, such as F# and Visual Basic .NET were not tested at all. Also for C# not everything is supported yet.
If IL verification is enabled and SharpDetect did not introduce new violations, instrumented assemblies should be safe to execute.

# Requirements
Project was developed and/or tested using the following environment.
*  **Runtime**: .NET Core 2.1
*  **Platform**: Tested on Windows 10, Debian 9, Ubuntu 18.04

# Building the Project
### Option 1 (using powershell)
* execute the following command within the *src/SharpDetect* directory
```text
 .\setup.ps1 -d <output-directory> -t Release
```
* set the environment variable **SHARPDETECT_PLUGINS** to the *output-directory/Plugins* folder

### Option 2 (manual setup)
Alternatively create the following folder tree:
```text
SharpDetect
    | bin 
    | Examples
    | Plugins
```

* build the whole solution using the following command
```bash
dotnet build -c Release
```
* copy content of the *src/SharpDetect/SharpDetect.Console/.../bin* folder to the *SharpDetect/bin*
* copy content of the *src/SharpDetect/SharpDetect.Plugins/../bin* folder to the *SharpDetect/Plugins*
* copy content of the *src/SharpDetect/SharpDetect.Examples* to the *SharpDetect/Examples*
* set the environment variable **SHARPDETECT_PLUGINS** to the *SharpDetect/Plugins* folder

# More Resources
* [Sample](docs/example-dynamic-analysis.md): first steps with SharpDetect. Start with a .NET program, prepare configuration, instrument assemblies, execute dynamic analysis and evaluate obtained results.
* [Plugins](docs/create-analysis-plugin.md): a more advanced document, which covers how to implement custom analysis plugins.
