# Analysis Plugins

This document describes the main extensibility point of SharpDetect - the possibility to implement and use custom analysis plugins. Among other things, we will describe how SharpDetect searches for plugins, how are these plugins implemented, as well as, what are the requirements and possible limitations. 

## Discovering Plugins

Immediately after calling `dotnet SharpDetect.Console.dll run --config "Plugin1|Plugin2"`, SharpDetect tries to find and initialize requested plugins. This is basically a depth-first-search that starts in the root plugins directory, which is specified using the **SHARPDETECT_PLUGINS** environment variable. SharpDetect locates all assemblies within the specified subtree of the file system and checks for available plugins. Once the discovery phase is over, it tries to instantiate requested plugins.

## Implementing Custom Plugins

Every plugin is a class that derive from the `SharpDetect.Plugins.BasePlugin` abstract class. This base class defines various virtual methods that correspond to individual dynamic analysis events. For the purposes of custom analyses, users may override methods that represent events that need to be tracked. The full list of these virtual methods can be seen in the following code listing.

```csharp
// Analysis control events
public virtual void AnalysisBegin(MethodDescriptor method);
public virtual void AnalysisEnd(MethodDescriptor method);

// Arrays-related events
public virtual void ArrayCreated(int threadId, MethodDescriptor method, Array array);
public virtual void ArrayElementRead(int threadId, MethodDescriptor method, Array array, int index);
public virtual void ArrayElementWritten(int threadId, MethodDescriptor method, Array array, int index, object value);

// Class construction (injected at the end of .cctor)
public virtual void ClassConstructed(int threadId, TypeDescriptor type);

// Fields-related events
public virtual void FieldRead(int threadId, object instance, FieldDescriptor field);
public virtual void FieldWritten(int threadId, object instance, FieldDescriptor field, object value);

// Locks-related events
public virtual void LockAcquireAttempted(int threadId, MethodDescriptor method, object lockObj, (int, object)[] parameters);
public virtual void LockAcquireReturned(int threadId, MethodDescriptor method, object lockObj, bool result, (int, object)[] parameters);
public virtual void LockReleased(int threadId, MethodDescriptor method, object lockObj);

// Signals-related events
public virtual void ObjectWaitAttempted(int threadId, MethodDescriptor method, object waitObj, (int, object)[] parameters);
public virtual void ObjectWaitReturned(int threadId, MethodDescriptor method, object waitObj, bool result, (int, object)[] parameters);
public virtual void ObjectPulsedOne(int threadId, MethodDescriptor method, object pulseObj);
public virtual void ObjectPulsedAll(int threadId, MethodDescriptor method, object pulseObj);

// Method calls-related events
public virtual void MethodCalled(int threadId, (int, object)[] parameters, MethodDescriptor method);
public virtual void MethodReturned(int threadId, object returnValue, bool isValid, (int, object)[] parameters, MethodDescriptor method);

// Dynamic allocations
public virtual void ObjectCreated(int threadId, object obj);

// User-created threads-related events
public virtual void UserThreadCreated(int threadId, Thread newThread);
public virtual void UserThreadStarted(int threadId, Thread thread);
public virtual void UserThreadJoined(int threadId, Thread thread);
```

Since users may specify multiple analysis plugins per a single execution of the dynamic analysis, SharpDetect creates **plugins chains**. Individual analysis events are in this case always dispatched to the first plugin in the chain. It may then either decide to consume the event (default behaviour), or forward it to the next plugin in chain. Forwarding can be always achieved by calling the base method's implementation.

In order to implement a new analysis plugin, users only need to derive from the mentioned base class and override required events. Lastly, build the plugin and place the obtained assembly in the plugins root folder (or one of its subfolders).

## Requirements and Limitations

There are a couple of rules and advices that users of SharpDetect need to keep in mind when implementing custom plugins or changing implementations of existing plugins. These guidelines can be expressed in the following points.

- **Threading model**: Every analysis event needs to be handled synchronously by the thread that raised the event. SharpDetect has no
mechanism to distinguish custom analysis threads from the threads used by the subject program. Furthermore, analysis events may be raised concurrently – therefore, a proper thread synchronization might be needed when implementing some plugins.

- **Hijacking threads**: The threading model above hints that SharpDetect actually hijacks the control-flow of the subject program's threads. This happens whenever they need to raise an analysis event. Therefore, it is probably not a good idea to hold on these threads for extended periods of time. Clearly, by holding a thread, SharpDetect actually pauses execution of the subject program. Therefore, all threads should be released immediately after the necessary analysis was performed.

- **Reference as few as possible**: SharpDetect shares memory-space and code with the subject program. Generally, we want to minimize the number of referenced types and methods by analysis plugins in case users decide to instrument them. As long as users obey the first mentioned rule, SharpDetect has a mechanism to distinguish analysis events triggered by SharpDetect code and plugins from those that are triggered by the subject program. But still it is advised to keep the plugin implementations simple.

**Note**: mentioned limitations will be addressed in the future version of SharpDetect. It is possible to perform dynamic analysis of .NET programs in such a way that analysis plugins do not share address space with the subject program. This approach is inspired by the project [DiSL and ShadowVM](https://gitlab.ow2.org/disl/disl).
