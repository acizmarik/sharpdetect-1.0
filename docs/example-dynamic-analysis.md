# Example Dynamic Analysis

We are going to analyse the following C# program that can be found in the *SharpDetect/Examples/Sample* folder
```csharp
using System;
using System.Threading.Tasks;

namespace Sample
{
    class Program
    {
        static int[] Data = new int[10];

        static void Main(string[] args)
        {
            for (var i = 0; i < Data.Length; i++)
                Data[i] = i;

            Parallel.For(0, Data.Length - 1,
                (index) => Data[index + 1] = Data[index] + 1);
        }
    }
}
```
We can clearly see that there is a data-race in the lambda passed to the ```Parallel.For``` call.

## Building the Subject Program

In order to build the subject program, we first need to find the [RID](https://docs.microsoft.com/en-us/dotnet/core/rid-catalog) of the current platform. For example, the RID of Windows 10 64bit is **win10-x64**. Similarly, the RID of ubuntu 18.04 64bit is **ubuntu.18.04-x64**. Then we can use the following command to build the project and publish it with all of its dependencies (i.e. create a self-contained package):

```text
dotnet SharpDetect.Console.dll build ..\Examples\Sample.csproj --rid <rid-of-your-platform>
```

## Configuring the Instrumentation Process

There are multiple ways that can be used to pass configuration to SharpDetect.

**Global configuration**: file `global-config.json` contains default values for settings. We can specify here, which injectors should be enabled by default, as well as, override some instrumentation settings. The default content of this file can be seen in the following code-listing.

```json
{
    "AlwaysIncludeMethodPatterns": [
        "System.Threading.Monitor",
        "System.Threading.Thread::.ctor(",
        "System.Threading.Thread::Start(",
        "System.Threading.Thread::Join("
    ],

    "ArrayInjectors": true,
    "FieldInjectors": true,
    "MethodInjectors": true,
    "ClassCreateInjector": true,
    "ObjectCreateInjector": true,

    "VerifyInstrumentation": true
}
```
In the configuration above, it is specified that methods starting with `System.Threading.Monitor`, `System.Threading.Thread::.ctor(`, `System.Threading.Thread::Start(` and `System.Threading.Thread::Join(` are instrumented always, regarding of the local (specific) configuration. Furthermore, all injectors are by default enabled and after instrumentation, SharpDetect should perform verification of the instrumented assemblies.

**Local configuration**: file `<project-name>.json` contains specific values for a dynamic analysis. Using this configuration file, users may specify the target assembly (the assembly that contains entrypoint for the dynamic analysis), method and field patterns for instrumentation and it may also override certain settings from the global configuration, such as enabling/disabling various injectors. The local configuration that we are going to use in this example is the following:

```json
{
    "TargetAssembly" : "Sample.dll",
    "FieldPatterns" : [ "Sample" ],
    "MethodPatterns" : [ "Sample" ],
}
```

In the configuration above, it is specified that the target assembly has a relative path `.\Sample.dll` to the configuration file. Furthermore, all methods and field, whose namespace starts with `Sample` should be instrumented.

## Performing the Instrumentation

Once we have prepared the configuration, we can proceed to the instrumentation. In order to instrument the subject program, we need to pass the path to the local configuration. This can be achieved using the following command:

```text
dotnet SharpDetect.Console.dll instrument ..\Examples\Sample.json --level Information
```

## Running the Dynamic Analysis

Before executing the dynamic analysis, we can pass additional parameters to SharpDetect. Most importantly, we can now specify which plugins should be registered for the analysis. This can be achieved using the **--config** parameter that takes names of required plugins delimited using the **'|'** pipe character. 

Ensure that the environment variable **SHARPDETECT_PLUGINS** points to the directory with analysis plugins for SharpDetect. SharpDetect will search the directory, as well as all of its subdirectories for available plugins. All specified plugins need to be available in order for the dynamic analysis to proceed. Dynamic analysis can be started using the following command.

```text
dotnet SharpDetect.Console.dll run ..\Examples\_SharpDetect\Sample.dll --config “EchoPlugin|FastTrack” --level Information
```

## Evaluating Results

After successful execution of the dynamic analysis, we should see an output similar to the following. This suggests that the FastTrack plugin correctly recognized data-races on multiple array elements.

```text
16:33:41 [ERR] [FastTrack] detected data-race on array element System.Int32[][1]
16:33:41 [ERR] [FastTrack] detected data-race on array element System.Int32[][2]
16:33:41 [ERR] [FastTrack] detected data-race on array element System.Int32[][3]
16:33:41 [ERR] [FastTrack] detected data-race on array element System.Int32[][7]
16:33:41 [ERR] [FastTrack] detected data-race on array element System.Int32[][4]
16:33:41 [ERR] [FastTrack] detected data-race on array element System.Int32[][5]
16:33:41 [ERR] [FastTrack] detected data-race on array element System.Int32[][6]
16:33:41 [ERR] [FastTrack] detected data-race on array element System.Int32[][8]
```
