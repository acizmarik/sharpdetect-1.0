/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using dnlib.DotNet;
using SharpDetect.Common.Events;
using SharpDetect.Common.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpDetect.Common
{
    public static partial class Definitions
    {
        public static readonly string FrameworkNamespace = "SharpDetect";
        public static readonly string EventDispatcherTypeName = "EventDispatcherBackend";

        public static ITypeDefOrRef EventDispatcherType { get; private set; }
        public static ITypeDefOrRef ParameterType { get; private set; }
        public static IMethod ParameterCtor { get; private set; }
        public static List<AnalysisEvent> RegisteredEvents { get; private set; }
        private static Dictionary<AnalysisEventType, List<AnalysisEvent>> eventTranslations;

        static Definitions()
        {
            RegisteredEvents = new List<AnalysisEvent>();
            eventTranslations = new Dictionary<AnalysisEventType, List<AnalysisEvent>>();
            InitializeEvents();
        }

        public static bool HasEvent(AnalysisEventType type)
            => eventTranslations.ContainsKey(type);

        public static IEnumerable<AnalysisEvent> GetEvent(AnalysisEventType type)
        {
            foreach (var @event in eventTranslations[type])
                yield return @event;
        }

        public static void RegisterEvent(AnalysisEventType type, AnalysisEvent @event)
        {
            RegisteredEvents.Add(@event);

            if (!eventTranslations.ContainsKey(type))
                eventTranslations.Add(type, new List<AnalysisEvent>());

            eventTranslations[type].Add(@event);
        }

        private static void InitializeEvents()
        {
            // Get registered events
            foreach (var property in typeof(Definitions).GetProperties().Where(p => p.PropertyType == typeof(AnalysisEvent)))
            {
                // Skip unregistered events
                if (!Attribute.IsDefined(property, typeof(RegisteredEventAttribute)))
                    continue;

                var registrationAttribute = (RegisteredEventAttribute)Attribute.GetCustomAttribute(property, typeof(RegisteredEventAttribute));
                var extensionAttribute = (ExtensionAttribute)Attribute.GetCustomAttribute(property, typeof(ExtensionAttribute));
                // Prepare event
                var @event = new AnalysisEvent()
                {
                    Name = $"On{property.Name}",
                    RaiserMethodName = $"Raise{property.Name}",
                    RawParameters = registrationAttribute.Parameters,
                    Type = registrationAttribute.Type,
                    ExtensionTo = extensionAttribute?.FromEvent
                };
                // Store event
                property.SetValue(null, @event);
                RegisteredEvents.Add(@event);

                if (!eventTranslations.ContainsKey(@event.Type))
                    eventTranslations.Add(@event.Type, new List<AnalysisEvent>());

                eventTranslations[@event.Type].Add(@event);
            }
        }

        public static void SetupDescriptors(AssemblyDef corlib)
        {
            var valueTuple2 = corlib.ManifestModule.Types.Single(t => t.Namespace == "System" && t.Name == "ValueTuple`2");
            var valueTupleSig = new ValueTypeSig(valueTuple2);
            var valueTuple2Ctor = valueTuple2.Methods.Single(m => m.Name == ".ctor" && m.Parameters.Count == 3);
            ParameterType = valueTupleSig.MakeGenericInstanceType(corlib.ManifestModule.CorLibTypes.Int32, corlib.ManifestModule.CorLibTypes.Object);
            ParameterCtor = valueTuple2Ctor.MakeHostInstanceGeneric(valueTupleSig, corlib.ManifestModule.CorLibTypes.Int32, corlib.ManifestModule.CorLibTypes.Object);

            foreach (var @event in RegisteredEvents)
                @event.Prepare(corlib);
        }

        public static void SetupFromInstrumented(AssemblyDef corlib)
        { 
            MethodDef GetMethodRaiser(ITypeDefOrRef type, string methodName)
                => type.ResolveTypeDef().Methods.Single(m => m.Name == methodName);

            // Load event dispatcher
            EventDispatcherType = corlib.ManifestModule.Types.Single(
                t => t.Namespace == FrameworkNamespace && t.Name == EventDispatcherTypeName);

            // Load raiser methods for events
            foreach (var @event in RegisteredEvents)
                @event.RaiserMethod = GetMethodRaiser(EventDispatcherType, @event.RaiserMethodName);
        }
    }
}
