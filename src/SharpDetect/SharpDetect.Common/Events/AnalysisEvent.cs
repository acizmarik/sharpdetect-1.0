/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using dnlib.DotNet;
using dnlib.DotNet.Emit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpDetect.Common.Events
{
    public class AnalysisEvent
    {
        public string Name { get; internal set; }
        public string RaiserMethodName { get; internal set; }
        public AnalysisEventType Type { get; internal set; }
        public AnalysisEventType? ExtensionTo { get; internal set; }

        public ITypeDefOrRef ArgsType { get; internal set; }
        public IMethod ArgsCtor { get; internal set; }
        public IMethod RaiserMethod { get; internal set; }
        
        public TypeSig[] DnlibParameters { get; internal set; }
        public TypeSig[] ReflectionParameters { get; internal set; }
        internal string[] RawParameters { get; set; }
        internal AssemblyDef CoreLib { get; set; }

        private void ParseParameters()
        {
            if (RawParameters == null || RawParameters.Length == 0)
                return;

            var index = 0;
            this.DnlibParameters = new TypeSig[RawParameters.Length];
            foreach (var param in RawParameters)
            {
                try
                {
                    // Special case so that user does not need to specify arrays
                    if (param == "#PARAMS#")
                    {
                        DnlibParameters[index++] = Definitions.ParameterType.ToTypeSig().MakeArrayType();
                        continue;
                    }
                    var typeSeparator = param.LastIndexOf('.');
                    var @namespace = param.Substring(0, typeSeparator);
                    var @class = param.Substring(typeSeparator + 1);
                    DnlibParameters[index++] = CoreLib.ManifestModule.Types.Single(
                        t => t.Namespace == @namespace && t.Name == @class).ToTypeSig();
                }
                catch (Exception)
                {
                    throw new Exception($"Unable to find {param} type in {CoreLib}");
                }
            }
        }

        public void Prepare(AssemblyDef coreLibrary)
        {
            this.CoreLib = coreLibrary;
            ParseParameters();

            // Non-parametric event
            if (DnlibParameters == null || DnlibParameters.Length == 0)
                return;
            // Single parameter - no need to pack anything
            else if (DnlibParameters.Length == 1)
            {
                this.ArgsType = DnlibParameters[0].ToTypeDefOrRef();
                this.ArgsCtor = null;
            }
            // Multiple parameters - pack to valuetuple
            else if (DnlibParameters.Length > 1 && DnlibParameters.Length < 8)
            {
                var valueTuple = CoreLib.ManifestModule.Types.Single(
                    t => t.Namespace == "System" && t.Name == $"ValueTuple`{DnlibParameters.Length}");
                var valueTupleSig = new ValueTypeSig(valueTuple);
                var valueTupleCtor = valueTuple.FindConstructors().Single(ctor => ctor.Parameters.Count == DnlibParameters.Length + 1);

                this.ArgsType = valueTupleSig.MakeGenericInstanceType(DnlibParameters);
                this.ArgsCtor = valueTupleCtor.MakeHostInstanceGeneric(valueTupleSig, DnlibParameters);
            }
            else
                throw new ArgumentException("Events can not have more than 8 parameters!");
        }
    }
}
