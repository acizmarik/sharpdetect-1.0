/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpDetect.Common.Extensions
{
    public static class LockExtensions
    {
        public const string Monitor_Enter_Block1 = "System.Void System.Threading.Monitor::Enter(System.Object)";
        public const string Monitor_Enter_Block2 = "System.Void System.Threading.Monitor::Enter(System.Object,System.Boolean&)";
        public const string Monitor_Enter_Try1 = "System.Boolean System.Threading.Monitor::TryEnter(System.Object)";
        public const string Monitor_Enter_Try2 = "System.Void System.Threading.Monitor::TryEnter(System.Object,System.Boolean&)";
        public const string Monitor_Enter_Try3 = "System.Boolean System.Threading.Monitor::TryEnter(System.Object,System.Int32)";
        public const string Monitor_Enter_Try4 = "System.Boolean System.Threading.Monitor::TryEnter(System.Object,System.TimeSpan)";
        public const string Monitor_Enter_Try5 = "System.Void System.Threading.Monitor::TryEnter(System.Object,System.Int32,System.Boolean&)";
        public const string Monitor_Enter_Try6 = "System.Void System.Threading.Monitor::TryEnter(System.Object,System.TimeSpan,System.Boolean&)";
        public const string Monitor_Exit = "System.Void System.Threading.Monitor::Exit(System.Object)";

        private static Dictionary<string, Func<(int, object)[], object>> lockObjectGetters;
        private static Dictionary<string, Func<(int, object)[], bool, object, bool>> lockResultGetters;

        /// <summary>
        /// Gets locking object for lock call
        /// </summary>
        /// <param name="methodFullname">Locking method</param>
        /// <param name="parameters">Parameters for the locking call</param>
        /// <returns>Locking object</returns>
        public static object GetLock(string methodFullname, (int, object)[] parameters)
            => lockObjectGetters[methodFullname](parameters);

        /// <summary>
        /// Gets result of locking call. This can be invoked only AFTER lock call returned
        /// </summary>
        /// <param name="methodFullname">Locking method</param>
        /// <param name="parameters">Parameters for the locking call</param>
        /// <param name="hasReturnValue">Determines whether return value is available</param>
        /// <param name="returnValue">Boxed return value or null if not available</param>
        /// <returns>True if lock succeeded, false otherwise</returns>
        public static bool GetResult(string methodFullname, (int, object)[] parameters, bool hasReturnValue, object returnValue)
            => lockResultGetters[methodFullname](parameters, hasReturnValue, returnValue);

        static LockExtensions()
        {
            lockObjectGetters = new Dictionary<string, Func<(int, object)[], object>>();
            lockResultGetters = new Dictionary<string, Func<(int, object)[], bool, object, bool>>();

            // Individual lock descriptions:
            InitMonitorEnterExitMethods();

            // TODO: add other lock methods
        }

        #region MONITOR_ENTER_EXIT
        static void InitMonitorEnterExitMethods()
        {
            lockObjectGetters.Add(Monitor_Enter_Block1, (parameters) => parameters.First().Item2);
            lockObjectGetters.Add(Monitor_Enter_Block2, (parameters) => parameters.First().Item2);
            lockObjectGetters.Add(Monitor_Enter_Try1, (parameters) => parameters.First().Item2);
            lockObjectGetters.Add(Monitor_Enter_Try2, (parameters) => parameters.First().Item2);
            lockObjectGetters.Add(Monitor_Enter_Try3, (parameters) => parameters.First().Item2);
            lockObjectGetters.Add(Monitor_Enter_Try4, (parameters) => parameters.First().Item2);
            lockObjectGetters.Add(Monitor_Enter_Try5, (parameters) => parameters.First().Item2);
            lockObjectGetters.Add(Monitor_Enter_Try6, (parameters) => parameters.First().Item2);

            lockResultGetters.Add(Monitor_Enter_Block1, (_1, _2, _3) => true);
            lockResultGetters.Add(Monitor_Enter_Block2, (parameters, _1, _2) => (bool)parameters.Last().Item2);
            lockResultGetters.Add(Monitor_Enter_Try1, (_1, _2, result) => (bool)result);
            lockResultGetters.Add(Monitor_Enter_Try2, (parameters, _1, _2) => (bool)parameters.Last().Item2);
            lockResultGetters.Add(Monitor_Enter_Try3, (_1, _2, result) => (bool)result);
            lockResultGetters.Add(Monitor_Enter_Try4, (_1, _2, result) => (bool)result);
            lockResultGetters.Add(Monitor_Enter_Try5, (parameters, _1, _2) => (bool)parameters.Last().Item2);
            lockResultGetters.Add(Monitor_Enter_Try6, (parameters, _1, _2) => (bool)parameters.Last().Item2);

            MethodExtensions.RegisterExtensions(Monitor_Enter_Block1, AnalysisEventType.MethodCall, AnalysisEventType.LockAcquireAttempt);
            MethodExtensions.RegisterExtensions(Monitor_Enter_Block2, AnalysisEventType.MethodCall, AnalysisEventType.LockAcquireAttempt);
            MethodExtensions.RegisterExtensions(Monitor_Enter_Try1, AnalysisEventType.MethodCall, AnalysisEventType.LockAcquireAttempt);
            MethodExtensions.RegisterExtensions(Monitor_Enter_Try2, AnalysisEventType.MethodCall, AnalysisEventType.LockAcquireAttempt);
            MethodExtensions.RegisterExtensions(Monitor_Enter_Try3, AnalysisEventType.MethodCall, AnalysisEventType.LockAcquireAttempt);
            MethodExtensions.RegisterExtensions(Monitor_Enter_Try4, AnalysisEventType.MethodCall, AnalysisEventType.LockAcquireAttempt);
            MethodExtensions.RegisterExtensions(Monitor_Enter_Try5, AnalysisEventType.MethodCall, AnalysisEventType.LockAcquireAttempt);
            MethodExtensions.RegisterExtensions(Monitor_Enter_Try6, AnalysisEventType.MethodCall, AnalysisEventType.LockAcquireAttempt);

            MethodExtensions.RegisterExtensions(Monitor_Enter_Block1, AnalysisEventType.MethodReturn, AnalysisEventType.LockAcquireReturn);
            MethodExtensions.RegisterExtensions(Monitor_Enter_Block2, AnalysisEventType.MethodReturn, AnalysisEventType.LockAcquireReturn);
            MethodExtensions.RegisterExtensions(Monitor_Enter_Try1, AnalysisEventType.MethodReturn, AnalysisEventType.LockAcquireReturn);
            MethodExtensions.RegisterExtensions(Monitor_Enter_Try2, AnalysisEventType.MethodReturn, AnalysisEventType.LockAcquireReturn);
            MethodExtensions.RegisterExtensions(Monitor_Enter_Try3, AnalysisEventType.MethodReturn, AnalysisEventType.LockAcquireReturn);
            MethodExtensions.RegisterExtensions(Monitor_Enter_Try4, AnalysisEventType.MethodReturn, AnalysisEventType.LockAcquireReturn);
            MethodExtensions.RegisterExtensions(Monitor_Enter_Try5, AnalysisEventType.MethodReturn, AnalysisEventType.LockAcquireReturn);
            MethodExtensions.RegisterExtensions(Monitor_Enter_Try6, AnalysisEventType.MethodReturn, AnalysisEventType.LockAcquireReturn);

            MethodExtensions.RegisterExtensions(Monitor_Exit, AnalysisEventType.MethodReturn, AnalysisEventType.LockRelease);
        }
        #endregion
    }
}
