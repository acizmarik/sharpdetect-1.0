/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Common.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SharpDetect.Common.Extensions
{
    public static class MethodExtensions
    {
        private static Dictionary<AnalysisEventType, List<AnalysisEventType>> extensions;
        private static Dictionary<string, Dictionary<AnalysisEventType, HashSet<AnalysisEventType>>> registeredExtensions;
        
        static MethodExtensions()
        {
            extensions = new Dictionary<AnalysisEventType, List<AnalysisEventType>>();
            registeredExtensions = new Dictionary<string, Dictionary<AnalysisEventType, HashSet<AnalysisEventType>>>();

            // Warm-up individual extension managers
            System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(typeof(LockExtensions).TypeHandle);
            System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(typeof(SignalExtensions).TypeHandle);
            System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(typeof(ThreadExtensions).TypeHandle);
                      
            foreach (var eventType in (AnalysisEventType[])Enum.GetValues(typeof(AnalysisEventType)))
            {
                if (!Definitions.HasEvent(eventType))
                    continue;

                var events = Definitions.GetEvent(eventType);
                if (events.FirstOrDefault() != null)
                    extensions.Add(eventType, events.Select(e => e.Type).ToList());
            }
        }

        public static IEnumerable<AnalysisEventType> GetExtensions(AnalysisEventType type, string method)
        {
            if (!registeredExtensions.ContainsKey(method) || !registeredExtensions[method].ContainsKey(type))
                return Enumerable.Empty<AnalysisEventType>();

            return registeredExtensions[method][type];
        }

        public static IEnumerable<AnalysisEvent> GetEventsFromType(AnalysisEventType type)
            => Definitions.GetEvent(type);

        internal static void RegisterExtensions(string method, AnalysisEventType type, AnalysisEventType extension)
        {
            if (!registeredExtensions.ContainsKey(method))
                registeredExtensions.Add(method, new Dictionary<AnalysisEventType, HashSet<AnalysisEventType>>());
            if (!registeredExtensions[method].ContainsKey(type))
                registeredExtensions[method].Add(type, new HashSet<AnalysisEventType>());

            var collection = registeredExtensions[method][type].Add(extension);
        }
    }
}
