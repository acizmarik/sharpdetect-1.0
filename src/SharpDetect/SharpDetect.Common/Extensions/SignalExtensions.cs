/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpDetect.Common.Extensions
{
    public static class SignalExtensions
    {
        public const string Monitor_Pulse_One = "System.Void System.Threading.Monitor::Pulse(System.Object)";
        public const string Monitor_Pulse_All = "System.Void System.Threading.Monitor::PulseAll(System.Object)";
        public const string Monitor_Wait = "System.Boolean System.Threading.Monitor::Wait(System.Object)";
        public const string Monitor_Wait_TimeSpan = "System.Boolean System.Threading.Monitor::Wait(System.Object,System.TimeSpan)";
        public const string Monitor_Wait_Int32 = "System.Boolean System.Threading.Monitor::Wait(System.Object,System.Int32)";
        public const string Monitor_Wait_TimeSpan_Boolean = "System.Boolean System.Threading.Monitor::Wait(System.Object,System.TimeSpan,System.Boolean)";
        public const string Monitor_Wait_Int32_Boolean = "System.Boolean System.Threading.Monitor::Wait(System.Object,System.Int32,System.Boolean)";

        private static Dictionary<string, Func<(int, object)[], object>> waitObjectGetters;
        private static Dictionary<string, Func<(int, object)[], object>> pulseObjectGetters;
        private static Dictionary<string, Func<(int, object)[], bool, object, bool>> waitResultGetters;

        /// <summary>
        /// Gets pulsed object for pulse call
        /// </summary>
        /// <param name="methodFullname">Pulse method</param>
        /// <param name="parameters">Parameters for the pulse call</param>
        /// <returns>Pulsed object</returns>
        public static object GetPulseObject(string methodFullname, (int, object)[] parameters)
            => pulseObjectGetters[methodFullname](parameters);

        /// <summary>
        /// Gets waited object for wait call
        /// </summary>
        /// <param name="methodFullname">Wait method</param>
        /// <param name="parameters">Parameters for the wait call</param>
        /// <returns>Waited object</returns>
        public static object GetWaitObject(string methodFullname, (int, object)[] parameters)
           => waitObjectGetters[methodFullname](parameters);

        /// <summary>
        /// Gets result of wait call. This can be invoked only AFTER wait call returned
        /// </summary>
        /// <param name="methodFullname">Wait method</param>
        /// <param name="parameters">Parameters for the wait call</param>
        /// <param name="hasReturnValue">Determines whether return value is available</param>
        /// <param name="returnValue">Boxed return value or null if not available</param>
        /// <returns>True if wait succeeded, false otherwise</returns>
        public static bool GetWaitResult(string methodFullname, (int, object)[] parameters, bool hasReturnValue, object returnValue)
            => waitResultGetters[methodFullname](parameters, hasReturnValue, returnValue);

        static SignalExtensions()
        {
            pulseObjectGetters = new Dictionary<string, Func<(int, object)[], object>>();
            waitObjectGetters = new Dictionary<string, Func<(int, object)[], object>>();
            waitResultGetters = new Dictionary<string, Func<(int, object)[], bool, object, bool>>();

            // Individual pulse wait descriptions:
            InitMonitorWaitPulseMethods();
        }

        #region MONITOR_WAIT_PULSE
        static void InitMonitorWaitPulseMethods()
        {
            waitObjectGetters.Add(Monitor_Wait, (parameters) => parameters.First().Item2);
            waitObjectGetters.Add(Monitor_Wait_Int32, (parameters) => parameters.First().Item2);
            waitObjectGetters.Add(Monitor_Wait_Int32_Boolean, (parameters) => parameters.First().Item2);
            waitObjectGetters.Add(Monitor_Wait_TimeSpan, (parameters) => parameters.First().Item2);
            waitObjectGetters.Add(Monitor_Wait_TimeSpan_Boolean, (parameters) => parameters.First().Item2);

            pulseObjectGetters.Add(Monitor_Pulse_One, (parameters) => parameters.First().Item2);
            pulseObjectGetters.Add(Monitor_Pulse_All, (parameters) => parameters.First().Item2);

            waitResultGetters.Add(Monitor_Wait, (_1, _2, _3) => true);
            waitResultGetters.Add(Monitor_Wait_Int32, (_1, _2, result) => (bool)result);
            waitResultGetters.Add(Monitor_Wait_Int32_Boolean, (_1, _2, result) => (bool)result);
            waitResultGetters.Add(Monitor_Wait_TimeSpan, (_1, _2, result) => (bool)result);
            waitResultGetters.Add(Monitor_Wait_TimeSpan_Boolean, (_1, _2, result) => (bool)result);

            MethodExtensions.RegisterExtensions(Monitor_Wait, AnalysisEventType.MethodCall, AnalysisEventType.ObjectWaitAttempt);
            MethodExtensions.RegisterExtensions(Monitor_Wait_Int32, AnalysisEventType.MethodCall, AnalysisEventType.ObjectWaitAttempt);
            MethodExtensions.RegisterExtensions(Monitor_Wait_Int32_Boolean, AnalysisEventType.MethodCall, AnalysisEventType.ObjectWaitAttempt);
            MethodExtensions.RegisterExtensions(Monitor_Wait_TimeSpan, AnalysisEventType.MethodCall, AnalysisEventType.ObjectWaitAttempt);
            MethodExtensions.RegisterExtensions(Monitor_Wait_TimeSpan_Boolean, AnalysisEventType.MethodCall, AnalysisEventType.ObjectWaitAttempt);
            MethodExtensions.RegisterExtensions(Monitor_Wait, AnalysisEventType.MethodReturn, AnalysisEventType.ObjectWaitReturn);
            MethodExtensions.RegisterExtensions(Monitor_Wait_Int32, AnalysisEventType.MethodReturn, AnalysisEventType.ObjectWaitReturn);
            MethodExtensions.RegisterExtensions(Monitor_Wait_Int32_Boolean, AnalysisEventType.MethodReturn, AnalysisEventType.ObjectWaitReturn);
            MethodExtensions.RegisterExtensions(Monitor_Wait_TimeSpan, AnalysisEventType.MethodReturn, AnalysisEventType.ObjectWaitReturn);
            MethodExtensions.RegisterExtensions(Monitor_Wait_TimeSpan_Boolean, AnalysisEventType.MethodReturn, AnalysisEventType.ObjectWaitReturn);

            MethodExtensions.RegisterExtensions(Monitor_Pulse_One, AnalysisEventType.MethodReturn, AnalysisEventType.ObjectPulse);
            MethodExtensions.RegisterExtensions(Monitor_Pulse_All, AnalysisEventType.MethodReturn, AnalysisEventType.ObjectPulseAll);
        }
        #endregion
    }
}
