/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.Common.Extensions
{
    class ThreadExtensions
    {
        public const string ThreadCtor = "System.Void System.Threading.Thread::.ctor(System.Threading.ThreadStart)";
        public const string ThreadCtor_Stack = "System.Void System.Threading.Thread::.ctor(System.Threading.ThreadStart,System.Int32)";
        public const string ThreadCtor_Parametrized = "System.Void System.Threading.Thread::.ctor(System.Threading.ParametrizedThreadStart)";
        public const string ThreadCtor_Parametrized_Stack = "System.Void System.Threading.Thread::.ctor(System.Threading.ParametrizedThreadStart,System.Int32)";
        public const string Thread_Start = "System.Void System.Threading.Thread::Start()";
        public const string Thread_Start_Parameter = "System.Void System.Threading.Thread.Start(System.Object)";
        public const string Thread_Join = "System.Void System.Threading.Thread::Join()";
        public const string Thread_Join_Int32 = "System.Boolean System.Threading.Thread::Join(System.Int32)";
        public const string Thread_Join_TimeSpan = "System.Boolean System.Threading.Thread::Join(System.TimeSpan)";

        static ThreadExtensions()
        {
            InitThreadCtorStartJoinMethods();
        }

        public static bool GetThreadJoinResult(string methodFullname, bool hasReturnValue, object returnValue)
            => !hasReturnValue || (hasReturnValue && (bool)returnValue == true);

        #region THREAD_CTOR_START_JOIN
        static void InitThreadCtorStartJoinMethods()
        {
            MethodExtensions.RegisterExtensions(ThreadCtor, AnalysisEventType.ObjectCreate, AnalysisEventType.UserThreadCreate);
            MethodExtensions.RegisterExtensions(ThreadCtor_Stack, AnalysisEventType.ObjectCreate, AnalysisEventType.UserThreadCreate);
            MethodExtensions.RegisterExtensions(ThreadCtor_Parametrized, AnalysisEventType.ObjectCreate, AnalysisEventType.UserThreadCreate);
            MethodExtensions.RegisterExtensions(ThreadCtor_Parametrized_Stack, AnalysisEventType.ObjectCreate, AnalysisEventType.UserThreadCreate);

            MethodExtensions.RegisterExtensions(Thread_Start, AnalysisEventType.MethodReturn, AnalysisEventType.UserThreadStart);
            MethodExtensions.RegisterExtensions(Thread_Start_Parameter, AnalysisEventType.MethodReturn, AnalysisEventType.UserThreadStart);

            MethodExtensions.RegisterExtensions(Thread_Join, AnalysisEventType.MethodReturn, AnalysisEventType.UserThreadJoin);
            MethodExtensions.RegisterExtensions(Thread_Join_Int32, AnalysisEventType.MethodReturn, AnalysisEventType.UserThreadJoin);
            MethodExtensions.RegisterExtensions(Thread_Join_TimeSpan, AnalysisEventType.MethodReturn, AnalysisEventType.UserThreadJoin);
        }
        #endregion
    }
}
