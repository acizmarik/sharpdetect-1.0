/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Injector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpDetect.Common
{
    public static class DnlibExtensions
    {
        /// <summary>
        /// Instantiates generic type with provided arguments
        /// </summary>
        /// <param name="self">Generic type</param>
        /// <param name="arguments">Generic variables</param>
        /// <returns>Instantiated type</returns>
        public static ITypeDefOrRef MakeGenericInstanceType(this ClassOrValueTypeSig self, params TypeSig[] arguments)
        {
            // Create generic type signature
            var genericInstanceSig = (GenericInstSig)null;
            if (self.IsValueType)
                genericInstanceSig = new GenericInstSig(new ValueTypeSig(self.ToTypeDefOrRef()), arguments);
            else
                genericInstanceSig = new GenericInstSig(new ClassSig(self.ToTypeDefOrRef()), arguments);

            // Instantiate type using created generic type signature
            var typeSpec = new TypeSpecUser(genericInstanceSig);
            return typeSpec;
        }

        public static TypeSig MakeArrayType(this TypeSig self)
        {
            var reference = new SZArraySig(self);
            return reference;
        }

        public static TypeSig MakeArrayType(this ITypeDefOrRef self)
            => self.ToTypeSig().MakeArrayType();

        public static IMethod MakeHostInstanceGeneric(this IMethodDefOrRef self, ClassOrValueTypeSig declaringType, params TypeSig[] arguments)
        {
            var typeSpec = MakeGenericInstanceType(declaringType, arguments);
            return new MemberRefUser(self.Module, self.Name, self.MethodSig, typeSpec);
        }

        public static TypeSig GetReturnTypeSignature(MethodDef called, Instruction instruction, TypeSig returnType, InjectionContext context)
        {
            return GetNormalParameterSignature(called, instruction, returnType);
        }

        public static TypeSig GetParameterSignature(MethodDef called, Instruction instruction, Parameter parameter, InjectionContext context)
        {
            // Regular user-defined parameters
            if (parameter.IsNormalMethodParameter || parameter.IsReturnTypeParameter)
                return GetNormalParameterSignature(called, instruction, parameter.Type);
            // Hidden "this" parameter
            else if (parameter.IsHiddenThisParameter)
            {
                var declaringType = ((IMethod)instruction.Operand).DeclaringType.ToTypeSig();
                if (!declaringType.IsGenericInstanceType)
                    // Instantiation is not needed
                    return parameter.Type;

                var parameterSig = GetNormalParameterSignature(called, instruction, declaringType);
                if (parameter.Type.IsByRef)
                    parameterSig = new ByRefSig(parameterSig);
                return parameterSig;
            }
            else
                throw new ArgumentException($"Unknown parameter type {parameter.Type}");
        }

        private static TypeSig GetNormalParameterSignature(MethodDef called, Instruction instruction, TypeSig parameterType)
        {
            var method = (IMethod)instruction.Operand;
            var type = method.DeclaringType;

            // For ByRef params first solve its Next
            if (parameterType.IsByRef)
                return new ByRefSig(GetNormalParameterSignature(called, instruction, parameterType.Next));

            // Regular parameter or already instanced parameter can be just imported
            else if (!parameterType.IsGenericParameter && (parameterType.ToTypeDefOrRef().NumberOfGenericParameters == 0))
                return parameterType;
            // Generic param (defined by method)
            else if (parameterType is GenericMVar)
            {
                // Method is instantiated in source
                if (instruction.Operand is MethodSpec)
                {
                    var genericMethod = (MethodSpec)instruction.Operand;
                    return genericMethod.GenericInstMethodSig.GenericArguments[(int)((GenericMVar)parameterType).Number];
                }
                // Method is not instantiated in source
                else
                {
                    var genericMethodVariable = (GenericMVar)parameterType;
                    var genericMethod = genericMethodVariable.OwnerMethod;
                    return null;
                }
            }
            // Generic param (defined by declaring type)
            else if (parameterType is GenericVar)
            {
                var genericType = (TypeSpec)((IMethod)instruction.Operand).DeclaringType;
                return ((GenericInstSig)genericType.TypeSig).GenericArguments[(int)((GenericVar)parameterType).Number];
            }
            // Parameter with own generic params
            else
            {
                var genericArgs = new List<TypeSig>();
                foreach (var parameter in parameterType.ToGenericInstSig().GenericArguments)
                    genericArgs.Add(GetNormalParameterSignature(called, instruction, parameter));
                return parameterType.ToGenericInstSig().GenericType.MakeGenericInstanceType(genericArgs.ToArray()).ToTypeSig();
            }
        }       

        public static IMethod MakeGenericMethod(this IMethodDefOrRef self, params TypeSig[] arguments)
            => new MethodSpecUser(self, new GenericInstMethodSig(arguments));

        public static Instruction MakeIndirectLoadOfType(this ByRefSig type)
        {
            if (type.Next.IsValueType)
            {
                // Check for enum type
                /* TODO */
                // Check for other value types
                switch (type.Next.FullName)
                {
                    case "System.Boolean":
                    case "System.Char":
                    case "System.Byte":
                        return Instruction.Create(OpCodes.Ldind_U1);
                    case "System.UInt16":
                        return Instruction.Create(OpCodes.Ldind_U2);
                    case "System.UInt32":
                        return Instruction.Create(OpCodes.Ldind_U4);
                    // This is not a typo - Ldind_U8 is only alias for Ldind_I8
                    // see: https://en.wikipedia.org/wiki/List_of_CIL_instructions
                    case "System.UInt64":
                        return Instruction.Create(OpCodes.Ldind_I8);

                    case "System.SByte":
                        return Instruction.Create(OpCodes.Ldind_I1);
                    case "System.Int16":
                        return Instruction.Create(OpCodes.Ldind_I2);
                    case "System.Int32":
                        return Instruction.Create(OpCodes.Ldind_I4);
                    case "System.Int64":
                        return Instruction.Create(OpCodes.Ldind_I8);

                    case "System.Single":
                        return Instruction.Create(OpCodes.Ldind_R4);
                    case "System.Double":
                        return Instruction.Create(OpCodes.Ldind_R8);

                    // Default ldobj for structs
                    default:
                        return Instruction.Create(OpCodes.Ldobj, type.Next.ToTypeDefOrRef());
                }
            }
            else
                return Instruction.Create(OpCodes.Ldind_Ref);
        }

    }
}
