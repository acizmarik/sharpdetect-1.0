/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using dnlib.DotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpDetect.Injector
{
    public class InjectionContext
    {
        /// <summary>
        /// Core library
        /// </summary>
        public AssemblyDef CoreLib { get; set; }
        /// <summary>
        /// Shared assembly resolver for injector
        /// </summary>
        public AssemblyResolver AssemblyResolver { get; private set; }
        /// <summary>
        /// Shared member resolver for injector
        /// </summary>
        public Resolver MemberResolver { get; private set; }
        /// <summary>
        /// Newly added types
        /// </summary>
        public Dictionary<(string, string), TypeDef> InjectedTypes { get; private set; }
        private ModuleContext context { get; set; }

        public InjectionContext()
        {
            context = new ModuleContext();
            InjectedTypes = new Dictionary<(string, string), TypeDef>();
            AssemblyResolver = new AssemblyResolver() { UseGAC = false, EnableTypeDefCache = false, DefaultModuleContext = context };
            MemberResolver = new Resolver(AssemblyResolver);

            context.AssemblyResolver = AssemblyResolver;
            context.Resolver = MemberResolver;
        }

        public TypeRef MakeCoreLibTypeReference(string name, string @namespace)
            => new TypeRefUser(CoreLib.ManifestModule, @namespace, name);

        public MemberRef MakeCoreLibMethodReference(string name, MethodSig methodSig, TypeRef type)
            => new MemberRefUser(CoreLib.ManifestModule, name, methodSig, type);

        public MemberRef MakeCoreLibMethodReference(string name, MethodSig methodSig, TypeSpec type)
            => new MemberRefUser(CoreLib.ManifestModule, name, methodSig, type);

        public TypeSig MakeArray(TypeSig arrayType)
            => new SZArraySig(arrayType);

        public void RegisterNewType(TypeDef type)
            => InjectedTypes.Add((type.Name, type.Namespace), type);

        public TypeDef ResolveType(IType type)
        {
            if (type == null)
                throw new InvalidOperationException($"Attempt to resolve null as type");

            if ((type as TypeDef) != null)
                return (TypeDef)type;

            if ((type as TypeRef) != null)
                return MemberResolver.Resolve((TypeRef)type);

            if ((type as TypeSpec) != null)
                return ResolveType(((TypeSpec)type).ScopeType);

            throw new NotSupportedException($"Can not resolve type: {type}");
        }

        public MethodDef ResolveMethod(IMethod method)
        {
            if (method == null)
                throw new InvalidOperationException($"Attempt to resolve null as method");

            if ((method as MethodDef) != null)
                return (MethodDef)method;

            if ((method as MemberRef) != null)
            {
                var declaringTypeDef = ResolveType(method.DeclaringType);
                var resolvedMethodDef = context.Resolver.Resolve((MemberRef)method);
                if (resolvedMethodDef != null)
                    return (MethodDef)resolvedMethodDef;

                foreach (var methodDef in declaringTypeDef.Methods.Where(m => m.Name == method.Name))
                {
                    if (methodDef.ParamDefs.Count != method.MethodSig.Params.Count)
                        continue;

                    for (var paramIndex = 0; paramIndex < methodDef.ParamDefs.Count; ++paramIndex)
                    {
                        if (methodDef.MethodSig.Params[paramIndex].TypeName != method.MethodSig.Params[paramIndex].TypeName)
                            break;
                    }

                    return methodDef;
                }
            }

            // Instantiated generic method
            if ((method as MethodSpec) != null)
                // Resolve based on not instantiated method
                return ResolveMethod(((MethodSpec)method).Method);

            throw new NotSupportedException($"Can not resolve method: {method}");
        }

        public FieldDef ResolveField(IField field)
        {
            if (field == null)
                throw new InvalidOperationException($"Attempt to resolve null as field");

            if ((field as FieldDef) != null)
                return (FieldDef)field;

            if ((field as MemberRef) != null)
                return MemberResolver.ResolveField((MemberRef)field);

            throw new NotSupportedException($"Can not resolve field: {field}");
        }
    }
}
