/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Common.Events;
using SharpDetect.Common.Metadata;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.Common
{
    public static partial class Definitions
    {
        [RegisteredEvent(Type = AnalysisEventType.AnalysisStart, Parameters = new string[] { "System.String" })]
        public static AnalysisEvent AnalysisBegin { get; set; }

        [RegisteredEvent(Type = AnalysisEventType.AnalysisEnd, Parameters = new string[] { "System.String" })]
        public static AnalysisEvent AnalysisEnd { get; set; }

        [RegisteredEvent(Type = AnalysisEventType.ArrayRead, Parameters = new string[] { ("System.Array"), ("System.Int32"), ("System.String") })]
        public static AnalysisEvent ArrayRead { get; set; }

        [RegisteredEvent(Type = AnalysisEventType.ArrayWrite, Parameters = new string[] { ("System.Array"), ("System.Int32"), ("System.Object"), ("System.String") })]
        public static AnalysisEvent ArrayWrite { get; set; }

        [RegisteredEvent(Type = AnalysisEventType.ArrayCreate, Parameters = new string[] { ("System.Array"), ("System.String") })]
        public static AnalysisEvent ArrayCreate { get; set; }

        [RegisteredEvent(Type = AnalysisEventType.ClassCreate, Parameters = new string[] { ("System.String") })]
        public static AnalysisEvent ClassConstruct { get; set; }

        [RegisteredEvent(Type = AnalysisEventType.FieldRead, Parameters = new string[] { ("System.Object"), ("System.String"), ("System.Int32") })]
        public static AnalysisEvent FieldRead { get; set; }

        [RegisteredEvent(Type = AnalysisEventType.FieldWrite, Parameters = new string[] { ("System.Object"), ("System.Object"), ("System.String"), ("System.Int32") })]
        public static AnalysisEvent FieldWritten { get; set; }

        [Extension(FromEvent = AnalysisEventType.MethodCall)]
        [RegisteredEvent(Type = AnalysisEventType.LockAcquireAttempt, Parameters = new string[] { ("#PARAMS#"), ("System.String") })]        
        public static AnalysisEvent LockAcquireCall { get; set; }

        [Extension(FromEvent = AnalysisEventType.MethodReturn)]
        [RegisteredEvent(Type = AnalysisEventType.LockAcquireReturn, Parameters = new string[] { ("System.Object"), ("System.Boolean"), ("#PARAMS#"), ("System.String") })]
        public static AnalysisEvent LockAcquireReturn { get; set; }

        [Extension(FromEvent = AnalysisEventType.MethodReturn)]
        [RegisteredEvent(Type = AnalysisEventType.LockRelease, Parameters = new string[] { ("System.Object"), ("System.Boolean"), ("#PARAMS#"), ("System.String") })]
        public static AnalysisEvent LockRelease { get; set; }

        [Extension(FromEvent = AnalysisEventType.MethodCall)]
        [RegisteredEvent(Type = AnalysisEventType.ObjectWaitAttempt, Parameters = new string[] { ("#PARAMS#"), ("System.String") })]
        public static AnalysisEvent ObjectWaitCall { get; set; }

        [Extension(FromEvent = AnalysisEventType.MethodReturn)]
        [RegisteredEvent(Type = AnalysisEventType.ObjectWaitReturn, Parameters = new string[] { ("System.Object"), ("System.Boolean"), ("#PARAMS#"), ("System.String") })]
        public static AnalysisEvent ObjectWaitReturn { get; set; }

        [Extension(FromEvent = AnalysisEventType.MethodReturn)]
        [RegisteredEvent(Type = AnalysisEventType.ObjectPulse, Parameters = new string[] { ("System.Object"), ("System.Boolean"), ("#PARAMS#"), ("System.String") })]
        public static AnalysisEvent ObjectPulse { get; set; }

        [Extension(FromEvent = AnalysisEventType.MethodReturn)]
        [RegisteredEvent(Type = AnalysisEventType.ObjectPulseAll, Parameters = new string[] { ("System.Object"), ("System.Boolean"), ("#PARAMS#"), ("System.String") })]
        public static AnalysisEvent ObjectPulseAll { get; set; }

        [RegisteredEvent(Type = AnalysisEventType.MethodCall, Parameters = new string[] { ("#PARAMS#"), ("System.String") })]
        public static AnalysisEvent MethodCall { get; set; }

        [RegisteredEvent(Type = AnalysisEventType.MethodReturn, Parameters = new string[] { ("System.Object"), ("System.Boolean"), ("#PARAMS#"), ("System.String") })]
        public static AnalysisEvent MethodReturn { get; set; }

        [RegisteredEvent(Type = AnalysisEventType.ObjectCreate, Parameters = new string[] { ("System.Object") })]
        public static AnalysisEvent ObjectCreate { get; set; }

        [RegisteredEvent(Type = AnalysisEventType.UserThreadCreate, Parameters = new string[] { ("System.Object") })]
        public static AnalysisEvent UserThreadCreate { get; set; }

        [RegisteredEvent(Type = AnalysisEventType.UserThreadStart, Parameters = new string[] { ("System.Object"), ("System.Boolean"), ("#PARAMS#"), ("System.String") })]
        public static AnalysisEvent UserThreadStart { get; set; }

        [RegisteredEvent(Type = AnalysisEventType.UserThreadJoin, Parameters = new string[] { ("System.Object"), ("System.Boolean"), ("#PARAMS#"), ("System.String") })]
        public static AnalysisEvent UserThreadJoin { get; set; }
    }
}
