/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using CliWrap;
using SharpDetect.Core.Configuration;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using SharpDetect.Core;

namespace SharpDetect.Console
{
    partial class Program
    {
        static void Build(PublishOptions options)
        {
            var success = true;
            try
            {
                var buildDirectory = Path.Combine(Path.GetDirectoryName(options.Csproj), options.Output);
                var publishArguments = $"publish {options.Csproj} --runtime {options.RID} --output {buildDirectory}";
                Log.Debug("Running dotnet with following arguments: {publishArguments}", publishArguments);
                Log.Debug("Building to directory: {buildDirectory}", buildDirectory);

                if (!File.Exists(options.Csproj))
                    throw new ArgumentException($"The provided C# project file {options.Csproj} was not found.");

                var result = Cli.Wrap("dotnet")
                    .SetArguments(publishArguments)
                    .SetStandardErrorCallback(error =>
                    {
                        success = false;
                        Log.Error(error);
                    })
                    .EnableStandardErrorValidation(false)
                    .EnableExitCodeValidation(false)
                    .Execute();

                if (result.ExitCode != 0)
                    throw new ArgumentException($"The command dotnet exited with code {result.ExitCode}");

                var projectName = Path.GetFileNameWithoutExtension(options.Csproj);

                // Remove runtime config and dependencies
                var dependenciesConfig = Path.Combine(buildDirectory, $"{projectName}.deps.json");
                var runtimeConfig = Path.Combine(buildDirectory, $"{projectName}.runtimeconfig.json");
                if (File.Exists(dependenciesConfig))
                    File.Delete(dependenciesConfig);
                if (File.Exists(runtimeConfig))
                    File.Delete(runtimeConfig);

                // Copy analysis config
                var configPath = Path.Combine(Path.GetDirectoryName(options.Csproj), $"{projectName}.json");
                if (!File.Exists(configPath))
                    throw new ArgumentException($"Could not find the local configuration file. Expected it at: {configPath}.");
                File.Copy(configPath, Path.Combine(buildDirectory, Path.GetFileName(configPath)), true);
            }
            catch (Exception e)
            {
                Log.Error(e, "The following error occurred while trying to build the project: ");
                success = false;
            }
            finally
            {
                if (!success)
                {
                    var message = string.Concat(
                        "Could not build the project. ",
                        "Make sure the provided path and configuration is correct. ",
                        "Also ensure that the project does not produce any compilation errors.");
                    Log.Fatal(message);
                }
            }
        }
    }
}
