/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Core.Configuration;
using SharpDetect.Injector;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using SharpDetect.Core;
using SharpDetect.Console.Validators;

namespace SharpDetect.Console
{
    partial class Program
    {
        const string InstrumentedAssembliesPath = "_Instrumented";
        const string CleanAssemblyPostfix = "_clean";

        static void TryRestoreAssemblies(string folder)
        {
            Log.Debug("Restoring clean assemblies...");
            foreach (var file in Directory.GetFiles(folder).Where(f => f.EndsWith(CleanAssemblyPostfix)))
            {
                var newName = file.Substring(0, file.Length - CleanAssemblyPostfix.Length);
                File.Delete(newName);
                File.Move(file, newName);
            }
            Log.Debug("Clean assemblies were restored");
        }

        static void TryApplyInstrumentedAssemblies(string folder)
        {
            Log.Debug("Applying instrumented assemblies...");
            var instrumentedPath = Path.Combine(folder, InstrumentedAssembliesPath);
            foreach (var file in Directory.GetFiles(instrumentedPath))
            {
                var originalFilename = Path.GetFileName(file);
                var newCleanName = $"{Path.GetFileName(file)}{CleanAssemblyPostfix}";

                File.Move(Path.Combine(folder, originalFilename), Path.Combine(folder, newCleanName));
                File.Copy(Path.Combine(instrumentedPath, originalFilename), Path.Combine(folder, originalFilename), true);
            }
            Log.Debug("Instrumented assemlies were applied");
        }

        static void CopyDependencies(string folder)
        {
            Log.Debug("Copying additional dependencies...");
            const string mainRuntimeLibrary = "SharpDetect.Core.dll";
            const string injectionDefinitionsLibrary = "SharpDetect.Common.dll";
            const string dnlib = "dnlib.dll";
            const string serilog = "Serilog.dll";
            const string serilogConsole = "Serilog.Sinks.Console.dll";
            const string commandlineTool = "CommandLine.dll";

            var executingAssemblyPath = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory);
            File.Copy(Path.Combine(executingAssemblyPath, dnlib), Path.Combine(folder, dnlib), true);
            File.Copy(Path.Combine(executingAssemblyPath, serilog), Path.Combine(folder, serilog), true);
            File.Copy(Path.Combine(executingAssemblyPath, serilogConsole), Path.Combine(folder, serilogConsole), true);
            File.Copy(Path.Combine(executingAssemblyPath, commandlineTool), Path.Combine(folder, commandlineTool), true);
            File.Copy(Path.Combine(executingAssemblyPath, mainRuntimeLibrary), Path.Combine(folder, mainRuntimeLibrary), true);
            File.Copy(Path.Combine(executingAssemblyPath, injectionDefinitionsLibrary), Path.Combine(folder, injectionDefinitionsLibrary), true);
            Log.Debug("Dependencies were copied");
        }

        static void Instrument(InstrumentOptions options)
        {
            try
            {
                var parametersParser = new ParametersParser();
                var parameters = parametersParser.Parse(new string[] { options.AnalysisConfiguration });
                Log.Debug("Parameters: {@Params}", parameters);

                // If parameters were invalid there should be an error message for user
                if (parameters == null || !parametersParser.TryValidate(parameters))
                    throw new DynamicAnalysisException("Unable to continue due to preceeding errors in parameters. Exiting...");

                // If assemblies where previously instrumented => revert this action
                var outputFolder = Path.GetDirectoryName(parameters.TargetAssembly);
                TryRestoreAssemblies(outputFolder);
                var injector = new CodeInjector(parameters);

                // Prepare output assembly name
                var inputAssemblyFile = Path.GetFileName(parameters.TargetAssembly);
                var outputAssemblyName = Path.GetFileNameWithoutExtension(parameters.TargetAssembly);

                // Check analysis directory exists
                var inputProgramDirectory = Path.GetDirectoryName(parameters.TargetAssembly);
                var dynamicAnalysisBinariesDirectory = Path.Combine(inputProgramDirectory, InstrumentedAssembliesPath);
                if (!Directory.Exists(dynamicAnalysisBinariesDirectory))
                    Directory.CreateDirectory(dynamicAnalysisBinariesDirectory);

                // Prepare assembly file name
                var outputFileName = Path.Combine(dynamicAnalysisBinariesDirectory, inputAssemblyFile);

                // Create injected assembly based on supplied parameters
                injector.Inject();
                var modifiedAssemblies = injector.Store(Path.GetDirectoryName(outputFileName));

                // Apply new instrumented assemblies
                TryApplyInstrumentedAssemblies(outputFolder);
                // Copy additional dependencies
                CopyDependencies(outputFolder);

                // Check if we need to verify
                if (parameters.VerifyInstrumentation)
                {
                    foreach (var assembly in modifiedAssemblies)
                    {
                        // Core library throws many unrelated errors
                        if (assembly.EndsWith("System.Private.CoreLib.dll"))
                            continue;

                        var assemblyName = Path.GetFileName(assembly);
                        var directory = Path.Combine(Path.GetDirectoryName(assembly), "..");
                        var assemblyToVerify = Path.Combine(directory, assemblyName);

                        ILVerifier.Verify(assemblyToVerify);
                    }
                }
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Failed due to the following problem: ");
                throw;
            }
        }
    }
}
