/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Core.Configuration;
using SharpDetect.Plugins;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace SharpDetect.Console
{
    partial class Program
    {
        static void Run(RunOptions options)
        {
            try
            {
                var assemblyDirectory = Path.GetDirectoryName(options.Assembly);
                if (options.PluginsDirectory == null)
                    options.PluginsDirectory = Environment.GetEnvironmentVariable("SHARPDETECT_PLUGINS");

                // Check that directory with plugins is setup
                if (options.PluginsDirectory == null)
                {
                    var message = string.Concat(
                        "The folder with analysis plugins was not specified. ",
                        "Use the environment variable \"SHARPDETECT_PLUGINS\" to specify a valid directory that contains analysis plugins for SharpDetect.");
                    throw new ArgumentException(message);
                }
                // Check that the provided plugins directory exists
                else if (!Directory.Exists(options.PluginsDirectory))
                {
                    var message = string.Concat(
                        $"The provided analysis plugins directory \"{options.PluginsDirectory}\" does not exist. ",
                        "Use the environment variable \"SHARPDETECT_PLUGINS\" to specify a valid directory that contains analysis plugins for SharpDetect.");
                    throw new ArgumentException(message);
                }

                // Serialize options for analyzed assembly
                var formatter = new BinaryFormatter()
                {
                    AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple,
                };
                var filename = Path.Combine(assemblyDirectory, PluginsManager.ParametersResource);
                Log.Debug("Serializing analysis configuration to {filename}", filename);
                SerDes.Serialize(options, Path.Combine(assemblyDirectory, PluginsManager.ParametersResource));

                // Run analysis
                using (var process = new Process())
                {
                    process.StartInfo.FileName = "dotnet";
                    process.StartInfo.Arguments = $"{options.Assembly} {@""}";
                    // Disable Ready2Run - native images
                    process.StartInfo.EnvironmentVariables["COMPlus_ReadyToRun"] = "0";
                    // Disable advanced JIT optimizations
                    if (!options.EnableJitOptimizations)
                        process.StartInfo.EnvironmentVariables["COMPlus_JITMinOpts"] = "1";

                    Log.Information("Starting process with analyzed assembly...");
                    process.Start();
                    process.WaitForExit();
                    Log.Information("Process with {PID} terminated", process.Id);
                }
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Failed due to the following exception: ");
            }
        }
    }
}
