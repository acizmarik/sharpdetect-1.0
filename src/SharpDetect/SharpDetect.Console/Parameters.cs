/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Injector;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpDetect.Console
{
    class ParametersProvider : IParametersInjector
    {
        public string TargetAssembly { get; set; }

        public List<string> AlwaysIncludeMethodPatterns { get; set; }
        public List<string> AlwaysIncludeFieldPatterns { get; set; }
        public List<string> FieldPatterns { get; internal set; }
        public List<string> MethodPatterns { get; internal set; }

        public bool ArrayInjectors { get; internal set; }
        public bool FieldInjectors { get; internal set; }
        public bool MethodInjectors { get; internal set; }
        public bool ObjectCreateInjector { get; internal set; }
        public bool ClassCreateInjector { get; internal set; }

        public bool VerifyInstrumentation { get; internal set; }
        public bool ForceSingleThreadedAnalysis { get; internal set; }

        public ParametersProvider(ParametersGlobal globals, ParametersLocal locals)
        {
            TargetAssembly = locals.TargetAssembly;

            FieldPatterns = locals.FieldPatterns ?? globals.FieldPatterns;
            MethodPatterns = locals.MethodPatterns ?? globals.MethodPatterns;

            bool EvaluateConfig(bool? global, bool? local, bool? defaultValue = null)
            {
                if (local.HasValue)
                    return local.Value;

                else if (global.HasValue)
                    return global.Value;

                else
                {
                    // By default switches are turned on
                    if (defaultValue == null)
                        return true;

                    return defaultValue.Value;
                }
            }

            ArrayInjectors = EvaluateConfig(globals.ArrayInjectors, locals.ArrayInjectors);
            FieldInjectors = EvaluateConfig(globals.FieldInjectors, locals.FieldInjectors);
            MethodInjectors = EvaluateConfig(globals.MethodInjectors, locals.MethodInjectors);
            ObjectCreateInjector = EvaluateConfig(globals.ObjectCreateInjector, locals.ObjectCreateInjector);
            ClassCreateInjector = EvaluateConfig(globals.ClassCreateInjector, locals.ClassCreateInjector);
            
            AlwaysIncludeFieldPatterns = locals.AlwaysIncludeFieldPatterns ?? globals.AlwaysIncludeFieldPatterns;
            AlwaysIncludeMethodPatterns = locals.AlwaysIncludeMethodPatterns ?? globals.AlwaysIncludeMethodPatterns;

            VerifyInstrumentation = EvaluateConfig(globals.VerifyInstrumentation, locals.VerifyInstrumentation, false);
            ForceSingleThreadedAnalysis = EvaluateConfig(globals.ForceSingleThreadedAnalysis, locals.ForceSingleThreadedAnalysis, false);
        }
    }

    class ParametersGlobal
    {
        public List<string> AlwaysIncludeMethodPatterns { get; set; }
        public List<string> AlwaysIncludeFieldPatterns { get; set; }
        public List<string> FieldPatterns { get; set; }
        public List<string> MethodPatterns { get; set; }

        public string PluginsPath { get; set; }        

        public bool? ArrayInjectors { get; set; }
        public bool? FieldInjectors { get; set; }
        public bool? MethodInjectors { get; set; }

        public bool? ObjectCreateInjector { get; set; }
        public bool? ClassCreateInjector { get; set; }

        public bool? ForceSingleThreadedAnalysis { get; set; }
        public bool? VerifyInstrumentation { get; set; }
    }

    class ParametersLocal : ParametersGlobal
    {
        public string TargetAssembly { get; set; }
    }
}
