/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using Serilog;

namespace SharpDetect.Console
{
    partial class ParametersParser
    {
        private readonly string GlobalConfigFilename = "global-config.json";

        public ParametersProvider Parse(string[] args)
        {
            var executingDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var globalConfigPath = $"{executingDirectory}{Path.DirectorySeparatorChar}{GlobalConfigFilename}";

            if (!File.Exists(globalConfigPath))
            {
                Log.Error($"Global config: \"{GlobalConfigFilename}\" not found.");
                return null;
            }
            var dataGlobal = File.ReadAllText(globalConfigPath);
            var parametersGlobal = JsonConvert.DeserializeObject<ParametersGlobal>(dataGlobal);

            // Load specific config
            if (args.Length != 1)
            {
                Log.Error("Incorrect usage: expecting only one argument with analysis config.");
                return null;
            }
            var analysisConfigFilename = args.First();
            if (!File.Exists(analysisConfigFilename))
            {
                Log.Error($"Analysis config: \"{analysisConfigFilename}\" not found.");
                return null;
            }
            var dataLocal = File.ReadAllText(analysisConfigFilename);
            var parametersLocal = JsonConvert.DeserializeObject<ParametersLocal>(dataLocal);

            // Check input assembly
            if (!File.Exists(parametersLocal.TargetAssembly))
            {
                // Try to check relative to the config
                var directoryPath = Path.GetDirectoryName(analysisConfigFilename);
                if (!File.Exists(Path.Combine(directoryPath, parametersLocal.TargetAssembly)))
                {
                    Log.Error($"Input assembly {parametersLocal.TargetAssembly} not found.");
                    return null;
                }

                parametersLocal.TargetAssembly = Path.Combine(directoryPath, parametersLocal.TargetAssembly);
            }

            // Check environment variables
            var environmentPlugins = Environment.GetEnvironmentVariable("SHARPDETECT_PLUGINS");
            if (environmentPlugins != null)
                parametersLocal.PluginsPath = environmentPlugins;

            // Merge parameters
            var parameters = new ParametersProvider(parametersGlobal, parametersLocal);
            return parameters;
        }
    }
}
