/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SharpDetect.Console
{
    partial class ParametersParser
    {
        public bool TryValidate(ParametersProvider parameters)
        {
            var issues = false;

            try
            {
                if (parameters.TargetAssembly == null)
                {
                    Log.Error("You must specify TargetAssembly in injection config");
                    issues = true;
                }
                if (!File.Exists(parameters.TargetAssembly))
                {
                    Log.Error("Specified target assembly {Assembly} could not be found", parameters.TargetAssembly);
                    issues = true;
                }
            }
            catch (Exception e)
            {
                Log.Error(e, "Following error occurred when tried to validate parameters:");
                return true;
            }

            return !issues;
        }
    }
}
