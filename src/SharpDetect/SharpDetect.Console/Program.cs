/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using CommandLine;
using SharpDetect.Core.Configuration;
using Serilog;
using Serilog.Core;


namespace SharpDetect.Console
{
    partial class Program
    {
        static void Main(string[] args)
        {
            const string eventFormat = "{Timestamp:HH:mm:ss} [{Level:u3}] {Message:lj}{NewLine}{Exception}";
            var minimumLevelConfig = (LoggingLevelSwitch)null;

            // Get common settings
            var parsedArgs = Parser.Default.ParseArguments<BaseOptions, InstrumentOptions, PublishOptions, RunOptions>(args);
            parsedArgs.WithParsed<BaseOptions>((parameters) =>
            {
                minimumLevelConfig = new LoggingLevelSwitch(parameters.Level);

                // Create default logger          
                Log.Logger = new LoggerConfiguration()
                    .WriteTo.Console(outputTemplate: eventFormat)
                    .Enrich.FromLogContext().MinimumLevel.ControlledBy(minimumLevelConfig)
                    .CreateLogger();
                Log.Debug("Start.");
                Log.Debug("Arguments: {@Args}", args);
            });

            // Resolve other settings
            parsedArgs
                .WithParsed<PublishOptions>(options => Build(options))
                .WithParsed<InstrumentOptions>(options => Instrument(options))
                .WithParsed<RunOptions>(options => Run(options));

            Log.Debug("Done.");
            Log.CloseAndFlush();
        }
    }
}
