/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using ILVerify;
using Internal.TypeSystem.Ecma;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;

namespace SharpDetect.Console.Validators
{
    static class ILVerifier
    {
        public static void Verify(string assemblyPath)
        {          
            var resolver = new Resolver(Path.GetDirectoryName(assemblyPath));
            var verifier = new Verifier(resolver);
            verifier.SetSystemModuleName(new System.Reflection.AssemblyName("System.Private.CoreLib"));

            var error = false;
            try
            {
                var assemblyToVerify = resolver.Resolve(Path.GetFileName(assemblyPath));
                Log.Information("ILVerify starting verification of {assembly}", assemblyPath);
                var results = verifier.Verify(assemblyToVerify);

                // We are calling internal method in order to obtain the loaded module
                var moduleGetter = verifier.GetType().GetMethod("GetModule", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                var module = (EcmaModule)moduleGetter.Invoke(verifier, new object[] { assemblyToVerify });
                var metadataReader = module.MetadataReader;

                foreach (var result in results)
                {
                    if (result.Code != VerifierError.None)
                    {
                        error = true;
                        var type = metadataReader.GetTypeDefinition(result.Type);
                        var method = metadataReader.GetMethodDefinition(result.Method);
                        var typeNamespace = (!result.Type.IsNil && !type.Namespace.IsNil) ? metadataReader.GetString(type.Namespace) : "[ERROR-READING-TYPE-NAMESPACE]";
                        var typeName = !result.Type.IsNil && (!type.Name.IsNil) ? metadataReader.GetString(type.Name) : "[ERROR-READING-TYPE-NAME]";
                        var methodName = (!result.Method.IsNil && !method.Name.IsNil) ? metadataReader.GetString(method.Name) : "[ERROR-READING-METHOD-NAME]";
                        var args = (result.ErrorArguments.Length > 0) ? string.Join(";", result.ErrorArguments.Select(a => $"[{a.Name}, {a.Value}]")) : string.Empty;
                        Log.Warning("[ILVerify]: {message}; {type}::{method}; args={args}", result.Message, $"{typeNamespace}.{typeName}", methodName, args);
                    }
                    else
                        Log.Debug("[ILVerify]: {message}", result.Message);
                }
            }
            catch (Exception)
            {
                // ILVerify is still under development and sometimes throws when CIL is broken
                // Therefore we consider any exception thrown also as failed verification
                Log.Warning("ILVerify crashed during verification");
                return;
            }

            if (!error)
                Log.Information("Successfully verified all types and methods of the assembly");
            else
                Log.Warning("Could not verify assembly due to preceeding issues");
        }
    }

    class Resolver : ResolverBase
    {
        public string Directory { get; private set; }

        public Resolver(string directory)
        {
            Directory = directory;
        }

        protected override PEReader ResolveCore(string simpleName)
        {
            var filename = Path.Combine(Directory, simpleName);
            var filePath = (filename.EndsWith(".dll")) ? filename : $"{filename}.dll";

            if (!File.Exists(filePath))
                return null;

            var assemblyStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            return new PEReader(assemblyStream);
        }
    }
}
