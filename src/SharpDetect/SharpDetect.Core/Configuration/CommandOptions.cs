/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using CommandLine;
using CommandLine.Text;
using Serilog;
using Serilog.Configuration;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.Core.Configuration
{
    [Verb("build", HelpText = "Prepare project for analysis")]
    public class PublishOptions : BaseOptions
    {
        [Value(0, HelpText = "Path to csproj which describes executable assembly", Required = true)]
        public string Csproj { get; set; }

        [Option(HelpText = "Output directory", Required = false)]
        public string Output { get; set; } = "_SharpDetect";

        [Option(HelpText = "Runtime Identifier (RID) to target", Required = true)]
        public string RID { get; set; }

        [Usage(ApplicationAlias = "SharpDetect")]
        public static IEnumerable<Example> Examples
        {
            get
            {
                yield return new Example("Build project for windows", 
                    new PublishOptions() { Csproj = "MyProj.csproj", RID = "win10-x64" });

                yield return new Example("Build project for linux and set logger level to Debug", 
                    new PublishOptions() { Csproj = "MyProj.csproj", RID = "ubuntu.18.04-x64", Level = LogEventLevel.Debug });
            }
        }
    }

    [Verb("instrument", HelpText = "Instrument executable assembly")]
    public class InstrumentOptions : BaseOptions
    {
        [Value(0, HelpText = "Path to analysis configuration", Required = true)]
        public string AnalysisConfiguration { get; set; }
    }
    
    [Serializable]
    [Verb("run", HelpText = "Run dynamic analysis on given executable assembly")]
    public class RunOptions : BaseOptions
    {
        [Value(0, HelpText = "Instrumented assembly to execute")]
        public string Assembly { get; set; }

        [Option("config", HelpText = "Plugins configuration", Required = true, Min = 1, Separator = '|')]
        public IEnumerable<string> Plugins { get; set; }

        [Option("plugins", HelpText = "Plugins directory", Required = false)]
        public string PluginsDirectory { get; set; }

        [Option("enableJitOptimizations", HelpText = "Enable JIT compiler runtime optimizations", Required = false)]
        public bool EnableJitOptimizations { get; set; }
    }
    
    [Serializable]
    public class BaseOptions
    {
        [Option(HelpText = "Logging verbosity", Required = false)]
        public LogEventLevel Level { get; set; } = LogEventLevel.Warning;
    }
}
