/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace SharpDetect.Core.Configuration
{
    public static class SerDes
    {
        public static void Serialize(RunOptions options, string file)
        {
            var formatter = new BinaryFormatter();
            using (var fstream = new FileStream(file, FileMode.Create, FileAccess.Write))
                formatter.Serialize(fstream, options);
        }

        public static RunOptions Deserialize(string file)
        {
            var formatter = new BinaryFormatter();
            using (var fstream = new FileStream(file, FileMode.Open, FileAccess.Read))
                return (RunOptions)formatter.Deserialize(fstream);
        }
    }
}
