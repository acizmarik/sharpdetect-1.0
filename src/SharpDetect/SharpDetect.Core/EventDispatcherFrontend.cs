/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Common;
using SharpDetect.Plugins;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using SharpDetect.Core.Utilities;
using System.Reflection;
using Serilog;
using SharpDetect.Common.Events;

namespace SharpDetect.Core
{
    public class EventDispatcherFrontend
    {
        /// <summary>
        /// Flag indicating whether events should be forwarded to user.
        /// (use this flag so that user is not informed about events raised by monitoring framework)
        /// </summary>
        [ThreadStatic]
        public static bool EventForwardingSuppressed = true;
        public BasePlugin AnalysisPlugin { get; private set; }
        public static ILogger LoggerConfig { get { return Log.Logger; } }
        private SignatureResolver signatureResolver;
#if MEASUREMENTS
        private static long maxMemory = 0;
#endif

        internal void Setup(BasePlugin analysisPlugin)
        {
            this.AnalysisPlugin = analysisPlugin;
            this.signatureResolver = new SignatureResolver();
        }

        private void AddHandler<T>(EventInfo @event, Action<object, T> eventProcessor, object instance)
        {
            var handler = new Action<object, T>((sender, data) =>
            {
#if MEASUREMENTS
                var currentMemory = GC.GetTotalMemory(false);
                while (true)
                {
                    var currentMaxMemory = Interlocked.Read(ref maxMemory);
                    if (currentMemory < currentMaxMemory)
                        break;

                    if (Interlocked.CompareExchange(ref maxMemory, currentMemory, currentMaxMemory) == currentMaxMemory)
                        break;
                }                   
#endif
                if (!EventForwardingSuppressed)
                    eventProcessor(sender, data);
            });

            @event.AddEventHandler(instance, Delegate.CreateDelegate(@event.EventHandlerType, handler.Target, handler.Method));
        }

        private void RegisterEventHandlers()
        {
            var eventDispatcherBackend = Type.GetType($"{Definitions.FrameworkNamespace}.{Definitions.EventDispatcherTypeName}, System.Private.CoreLib");
            var eventDispatcherBackendInstanceGetter = eventDispatcherBackend.GetMethod("get_Instance");
            var eventDispatcherBackendInstance = eventDispatcherBackendInstanceGetter.Invoke(null, null);

            // Register analysis begin
            var analysisBeginEvent = eventDispatcherBackend.GetEvent(Definitions.AnalysisBegin.Name);
            AddHandler<string>(analysisBeginEvent, OnAnalysisBegin, eventDispatcherBackendInstance);
            // Register analysis end
            var analysisEndEvent = eventDispatcherBackend.GetEvent(Definitions.AnalysisEnd.Name);
            AddHandler<string>(analysisEndEvent, OnAnalysisEnd, eventDispatcherBackendInstance);

            // Register array created
            var arrayCreatedEvent = eventDispatcherBackend.GetEvent(Definitions.ArrayCreate.Name);
            AddHandler<(Array, string)>(arrayCreatedEvent, OnArrayCreated, eventDispatcherBackendInstance);
            // Register array read
            var arrayReadEvent = eventDispatcherBackend.GetEvent(Definitions.ArrayRead.Name);
            AddHandler<(Array, int, string)>(arrayReadEvent, OnArrayElementRead, eventDispatcherBackendInstance);
            // Register array write
            var arrayWriteEvent = eventDispatcherBackend.GetEvent(Definitions.ArrayWrite.Name);
            AddHandler<(Array, int, object, string)>(arrayWriteEvent, OnArrayElementWritten, eventDispatcherBackendInstance);

            // Register class constructed
            var classConstructedEvent = eventDispatcherBackend.GetEvent(Definitions.ClassConstruct.Name);
            AddHandler<string>(classConstructedEvent, OnClassConstructed, eventDispatcherBackendInstance);

            // Object created
            var objectCreatedEvent = eventDispatcherBackend.GetEvent(Definitions.ObjectCreate.Name);
            objectCreatedEvent.AddEventHandler(eventDispatcherBackendInstance,
                new EventHandler<object>((sender, data) =>
                {
                    if (!EventForwardingSuppressed)
                        OnObjectCreated(sender, data);
                }));

            // Thread create
            var threadCreatedEvent = eventDispatcherBackend.GetEvent(Definitions.UserThreadCreate.Name);
            threadCreatedEvent.AddEventHandler(eventDispatcherBackendInstance,
                new EventHandler<object>((sender, data) =>
                {
                    if (!EventForwardingSuppressed)
                        OnThreadCreated(sender, (Thread)data);
                }));
            // Thread started
            var threadStartedEvent = eventDispatcherBackend.GetEvent(Definitions.UserThreadStart.Name);
            AddHandler<(object, bool, (int, object)[], string)>(threadStartedEvent, OnThreadStarted, eventDispatcherBackendInstance);
            // Thread joined
            var threadJoinedEvent = eventDispatcherBackend.GetEvent(Definitions.UserThreadJoin.Name);
            AddHandler<(object, bool, (int, object)[], string)>(threadJoinedEvent, OnThreadJoined, eventDispatcherBackendInstance);

            // Field read
            var fieldReadTypeEvent = eventDispatcherBackend.GetEvent(Definitions.FieldRead.Name);
            AddHandler<(object, string, int)>(fieldReadTypeEvent, OnFieldReferenceTypeRead, eventDispatcherBackendInstance);
            // Field write
            var fieldWrittenTypeEvent = eventDispatcherBackend.GetEvent(Definitions.FieldWritten.Name);
            AddHandler<(object, object, string, int)>(fieldWrittenTypeEvent, OnFieldReferenceTypeWrite, eventDispatcherBackendInstance);

            // Lock acquire attempt
            var lockAcquireAttemptEvent = eventDispatcherBackend.GetEvent(Definitions.LockAcquireCall.Name);
            AddHandler<((int, object)[], string)>(lockAcquireAttemptEvent, OnLockAcquireAttempted, eventDispatcherBackendInstance);
            // Lock acquire return
            var lockAcquireReturnEvent = eventDispatcherBackend.GetEvent(Definitions.LockAcquireReturn.Name);
            AddHandler<(object, bool, (int, object)[], string)>(lockAcquireReturnEvent, OnLockAcquireReturned, eventDispatcherBackendInstance);
            // Lock release
            var lockReleaseEvent = eventDispatcherBackend.GetEvent(Definitions.LockRelease.Name);
            AddHandler<(object, bool, (int, object)[], string)>(lockReleaseEvent, OnLockRelease, eventDispatcherBackendInstance);

            // Object wait attempt
            var objectWaitAttemptEvent = eventDispatcherBackend.GetEvent(Definitions.ObjectWaitCall.Name);
            AddHandler<((int, object)[], string)>(objectWaitAttemptEvent, OnObjectWaitAttempted, eventDispatcherBackendInstance);

            // Object wait return
            var objectWaitReturnEvent = eventDispatcherBackend.GetEvent(Definitions.ObjectWaitReturn.Name);
            AddHandler<(object, bool, (int, object)[], string)>(objectWaitReturnEvent, OnObjectWaitReturned, eventDispatcherBackendInstance);

            // Object pulse
            var objectPulseEvent = eventDispatcherBackend.GetEvent(Definitions.ObjectPulse.Name);
            AddHandler<(object, bool, (int, object)[], string)>(objectPulseEvent, OnObjectPulsed, eventDispatcherBackendInstance);

            // Object pulse all
            var objectPulseAllEvent = eventDispatcherBackend.GetEvent(Definitions.ObjectPulseAll.Name);
            AddHandler<(object, bool, (int, object)[], string)>(objectPulseAllEvent, OnObjectPulsedAll, eventDispatcherBackendInstance);

            // Method call
            var methodCallEvent = eventDispatcherBackend.GetEvent(Definitions.MethodCall.Name);
            AddHandler<((int, object)[], string)>(methodCallEvent, OnMethodCalled, eventDispatcherBackendInstance);
            // Method returned
            var methodReturnEvent = eventDispatcherBackend.GetEvent(Definitions.MethodReturn.Name);
            AddHandler<(object, bool, (int, object)[], string)>(methodReturnEvent, OnMethodReturned, eventDispatcherBackendInstance);
        }

        private void OnAnalysisBegin(object sender, string descriptor)
        {
            using (new InternalSection())
                AnalysisPlugin.AnalysisStart(signatureResolver.ResolveMethod(descriptor));
        }

        private void OnAnalysisEnd(object sender, string descriptor)
        {
            using (new InternalSection())
                AnalysisPlugin.AnalysisEnd(signatureResolver.ResolveMethod(descriptor));

#if MEASUREMENTS
            Log.Warning("{memory}", maxMemory);
#endif
        }

        private int GetManagedThreadId()
            => Thread.CurrentThread.ManagedThreadId;

        private void OnArrayCreated(object sender, (Array, string) descriptor)
        {
            using (new InternalSection())
                AnalysisPlugin.ArrayCreated(GetManagedThreadId(), signatureResolver.ResolveMethod(descriptor.Item2), descriptor.Item1);
        }

        private void OnArrayElementRead(object sender, (Array, int, string) descriptor)
        {
            using (new InternalSection())
            {
                var method = signatureResolver.ResolveMethod(descriptor.Item3);
                AnalysisPlugin.ArrayElementRead(GetManagedThreadId(), method, descriptor.Item1, descriptor.Item2);
            }
        }

        private void OnArrayElementWritten(object sender, (Array, int, object, string) descriptor)
        {
            using (new InternalSection())
            {
                var method = signatureResolver.ResolveMethod(descriptor.Item4);
                AnalysisPlugin.ArrayElementWritten(GetManagedThreadId(), method, descriptor.Item1, descriptor.Item2, descriptor.Item3);
            }
        }

        private void OnClassConstructed(object sender, string className)
        { 
            using (new InternalSection())
                AnalysisPlugin.ClassConstructed(GetManagedThreadId(), signatureResolver.ResolveType(className));
        }

        private void OnFieldReferenceTypeRead(object sender, (object, string, int) descriptor)
        {
            using (new InternalSection())
            {
                var resolvedField = signatureResolver.ResolveField(descriptor.Item2, (FieldFlags)descriptor.Item3);
                AnalysisPlugin.FieldRead(GetManagedThreadId(), descriptor.Item1, resolvedField);
            }
        }

        private void OnFieldReferenceTypeWrite(object sender, (object, object, string, int) descriptor)
        {
            using (new InternalSection())
            {
                var resolvedField = signatureResolver.ResolveField(descriptor.Item3, (FieldFlags)descriptor.Item4);
                AnalysisPlugin.FieldWritten(GetManagedThreadId(), descriptor.Item1, resolvedField, descriptor.Item2);
            }
        }       

        private void OnLockAcquireAttempted(object sender, ((int, object)[], string) descriptor)
        {
            using (new InternalSection())
            {
                var lockObj = Common.Extensions.LockExtensions.GetLock(descriptor.Item2, descriptor.Item1);
                AnalysisPlugin.LockAcquireAttempted(GetManagedThreadId(), signatureResolver.ResolveMethod(descriptor.Item2), lockObj, descriptor.Item1);
            }
        }

        private void OnLockAcquireReturned(object sender, (object, bool, (int, object)[], string) descriptor)
        {
            using (new InternalSection())
            {
                var lockObj = Common.Extensions.LockExtensions.GetLock(descriptor.Item4, descriptor.Item3);
                var result = Common.Extensions.LockExtensions.GetResult(descriptor.Item4, descriptor.Item3, descriptor.Item2, descriptor.Item1);
                AnalysisPlugin.LockAcquireReturned(GetManagedThreadId(), signatureResolver.ResolveMethod(descriptor.Item4), lockObj, result, descriptor.Item3);
            }
        }

        private void OnLockRelease(object sender, (object, bool, (int, object)[], string) descriptor)
        {
            using (new InternalSection())
            {
                // Object which is being locked is always the first parameter for (Try)Enter methods on monitor
                var lockObj = descriptor.Item3[0].Item2;
                AnalysisPlugin.LockReleased(GetManagedThreadId(), signatureResolver.ResolveMethod(descriptor.Item4), lockObj);
            }
        }

        private void OnObjectCreated(object sender, object obj)
        {
            using (new InternalSection())
                AnalysisPlugin.ObjectCreated(GetManagedThreadId(), obj);
        }

        private void OnMethodCalled(object sender, ((int, object)[], string) descriptor)
        {
            using (new InternalSection())
                AnalysisPlugin.MethodCalled(GetManagedThreadId(), descriptor.Item1, signatureResolver.ResolveMethod(descriptor.Item2));
        }

        private void OnMethodReturned(object sender, (object, bool, (int, object)[], string) descriptor)
        {
            using (new InternalSection())
                AnalysisPlugin.MethodReturned(GetManagedThreadId(), descriptor.Item1, descriptor.Item2, descriptor.Item3, signatureResolver.ResolveMethod(descriptor.Item4));
        }

        private void OnThreadCreated(object sender, Thread thread)
        {
            using (new InternalSection())
                AnalysisPlugin.UserThreadCreated(GetManagedThreadId(), thread);
        }

        private void OnThreadStarted(object sender, (object, bool, (int, object)[], string) descriptor)
        {
            using (new InternalSection())
                // Instance reference is stored as first parameter
                AnalysisPlugin.UserThreadStarted(GetManagedThreadId(), (Thread)descriptor.Item3[0].Item2);
        }

        private void OnThreadJoined(object sender, (object, bool, (int, object)[], string) descriptor)
        {
            using (new InternalSection())
                // Instance reference is stored as first parameter
                AnalysisPlugin.UserThreadJoined(GetManagedThreadId(), (Thread)descriptor.Item3[0].Item2);
        }

        private void OnObjectWaitAttempted(object sender, ((int, object)[], string) descriptor)
        {
            using (new InternalSection())
            {
                var waitObj = Common.Extensions.SignalExtensions.GetWaitObject(descriptor.Item2, descriptor.Item1);
                AnalysisPlugin.ObjectWaitAttempted(GetManagedThreadId(), signatureResolver.ResolveMethod(descriptor.Item2), waitObj, descriptor.Item1);
            }
        }

        private void OnObjectWaitReturned(object sender, (object, bool, (int, object)[], string) descriptor)
        {
            using (new InternalSection())
            {
                var waitObj = Common.Extensions.SignalExtensions.GetWaitObject(descriptor.Item4, descriptor.Item3);
                var waitResult = Common.Extensions.SignalExtensions.GetWaitResult(descriptor.Item4, descriptor.Item3, descriptor.Item2, descriptor.Item1);
                AnalysisPlugin.ObjectWaitReturned(GetManagedThreadId(), signatureResolver.ResolveMethod(descriptor.Item4), waitObj, waitResult, descriptor.Item3);
            }
        }

        private void OnObjectPulsed(object sender, (object, bool, (int, object)[], string) descriptor)
        {
            using (new InternalSection())
            {
                var pulseObj = Common.Extensions.SignalExtensions.GetPulseObject(descriptor.Item4, descriptor.Item3);
                AnalysisPlugin.ObjectPulsedOne(GetManagedThreadId(), signatureResolver.ResolveMethod(descriptor.Item4), pulseObj);
            }
        }

        private void OnObjectPulsedAll(object sender, (object, bool, (int, object)[], string) descriptor)
        {
            using (new InternalSection())
            {
                var pulseObj = Common.Extensions.SignalExtensions.GetPulseObject(descriptor.Item4, descriptor.Item3);
                AnalysisPlugin.ObjectPulsedAll(GetManagedThreadId(), signatureResolver.ResolveMethod(descriptor.Item4), pulseObj);
            }
        }

#region SINGLETON_IMPLEMENTATION
        private static EventDispatcherFrontend instance;
        public static EventDispatcherFrontend GetInstance()
        {
            if (instance == null)
            {
                using (new InternalSection())
                {
                    instance = new EventDispatcherFrontend();
                    PluginsManager.Instance.Initialize();
                    Log.Information("Dynamic analysis framework successfully initialized.");
                    Log.Information("Beginning analysis...");
                }
            }
            return instance;
        }

        private EventDispatcherFrontend()
        {
            RegisterEventHandlers();
        }
#endregion
    }
}
