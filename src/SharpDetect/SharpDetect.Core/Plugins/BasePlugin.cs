/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using Serilog;
using SharpDetect.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SharpDetect.Plugins
{
    public abstract class BasePlugin
    {
        public abstract string PluginName { get; }
        public BasePlugin NextPlugin { get; set; }
        public event EventHandler<string> ViolationFound;

        protected const string ViolationPattern = "[{Plugin}] {Message}";
        protected const string InformationPattern = "[{Thread}] {Category}: {Message}";

        protected void RaiseViolationFound(string message)
        {
            Log.Error(ViolationPattern, PluginName, message);
            ViolationFound?.Invoke(this, message);
        }

        public virtual void AnalysisStart(MethodDescriptor method)
            => NextPlugin?.AnalysisStart(method);

        public virtual void AnalysisEnd(MethodDescriptor method)
            => NextPlugin?.AnalysisEnd(method);

        public virtual void ArrayCreated(int threadId, MethodDescriptor method, Array array)
            => NextPlugin?.ArrayCreated(threadId, method, array);

        public virtual void ArrayElementRead(int threadId, MethodDescriptor method, Array array, int index)
            => NextPlugin?.ArrayElementRead(threadId, method, array, index);

        public virtual void ArrayElementWritten(int threadId, MethodDescriptor method, Array array, int index, object value)
            => NextPlugin?.ArrayElementWritten(threadId, method, array, index, value);

        public virtual void ClassConstructed(int threadId, TypeDescriptor type)
            => NextPlugin?.ClassConstructed(threadId, type);

        public virtual void FieldRead(int threadId, object instance, FieldDescriptor field)
            => NextPlugin?.FieldRead(threadId, instance, field);

        public virtual void FieldWritten(int threadId, object instance, FieldDescriptor field, object value)
            => NextPlugin?.FieldWritten(threadId, instance, field, value);

        public virtual void LockAcquireAttempted(int threadId, MethodDescriptor method, object lockObj, (int, object)[] parameters)
            => NextPlugin?.LockAcquireAttempted(threadId, method, lockObj, parameters);

        public virtual void LockAcquireReturned(int threadId, MethodDescriptor method, object lockObj, bool result, (int, object)[] parameters)
            => NextPlugin?.LockAcquireReturned(threadId, method, lockObj, result, parameters);

        public virtual void LockReleased(int threadId, MethodDescriptor method, object lockObj)
            => NextPlugin?.LockReleased(threadId, method, lockObj);

        public virtual void ObjectWaitAttempted(int threadId, MethodDescriptor method, object waitObj, (int, object)[] parameters)
            => NextPlugin?.ObjectWaitAttempted(threadId, method, waitObj, parameters);

        public virtual void ObjectWaitReturned(int threadId, MethodDescriptor method, object waitObj, bool result, (int, object)[] parameters)
            => NextPlugin?.ObjectWaitReturned(threadId, method, waitObj, result, parameters);

        public virtual void ObjectPulsedOne(int threadId, MethodDescriptor method, object pulseObj)
            => NextPlugin?.ObjectPulsedOne(threadId, method, pulseObj);

        public virtual void ObjectPulsedAll(int threadId, MethodDescriptor method, object pulseObj)
            => NextPlugin?.ObjectPulsedAll(threadId, method, pulseObj);

        public virtual void MethodCalled(int threadId, (int, object)[] parameters, MethodDescriptor method)
            => NextPlugin?.MethodCalled(threadId, parameters, method);

        public virtual void MethodReturned(int threadId, object returnValue, bool isValid, (int, object)[] parameters, MethodDescriptor method)
            => NextPlugin?.MethodReturned(threadId, returnValue, isValid, parameters, method);

        public virtual void ObjectCreated(int threadId, object obj)
            => NextPlugin?.ObjectCreated(threadId, obj);

        public virtual void UserThreadCreated(int threadId, Thread newThread)
            => NextPlugin?.UserThreadCreated(threadId, newThread);

        public virtual void UserThreadStarted(int threadId, Thread thread)
            => NextPlugin?.UserThreadStarted(threadId, thread);

        public virtual void UserThreadJoined(int threadId, Thread thread)
            => NextPlugin?.UserThreadJoined(threadId, thread);
    }
}
