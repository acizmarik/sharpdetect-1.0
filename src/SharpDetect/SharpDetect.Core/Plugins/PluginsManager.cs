/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Core.Configuration;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using SharpDetect.Core;
using Serilog.Core;

namespace SharpDetect.Plugins
{
    public sealed class PluginsManager
    {
        public const string ParametersResource = "AnalysisConfiguration.dat";
        public const string ParametersDelimitter = ";";
        internal Dictionary<string, Assembly> LoadedAssemblies { get; private set; }
        internal Dictionary<string, Func<BasePlugin>> LoadedPlugins { get; private set; }
        internal string PluginsRootPath { get; private set; } = Path.Combine(Directory.GetCurrentDirectory(), "Plugins");
        private string[] pluginsToLoad;

        public void Initialize()
        {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var options = SerDes.Deserialize(Path.Combine(path, ParametersResource));

            const string eventFormat = "{Timestamp:HH:mm:ss} [{Level:u3}] {Message:lj}{NewLine}{Exception}";
            var minimumLevelConfig = new LoggingLevelSwitch(options.Level);

            // Create default logger
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext().MinimumLevel.ControlledBy(minimumLevelConfig)
                .Destructure.ToMaximumCollectionCount(5)
                .Destructure.ToMaximumStringLength(30)
                .WriteTo.Console(outputTemplate: eventFormat)
                .CreateLogger();

            // Prepare plugins
            PluginsRootPath = options.PluginsDirectory;
            pluginsToLoad = options.Plugins.ToArray();
            LoadPlugins();
            var pluginChain = SetupPluginChain(pluginsToLoad);

            // Initialize event dispatcher
            EventDispatcherFrontend.GetInstance().Setup(pluginChain);
        }

        private BasePlugin SetupPluginChain(string[] pluginIdentifiers)
        {
            Log.Information("Required plugins for analysis: {Plugins}", pluginIdentifiers);
            List<Func<BasePlugin>> pluginActivators = new List<Func<BasePlugin>>();
            bool pluginsFound = true;

            foreach (var identifier in pluginIdentifiers)
            {
                if (!LoadedPlugins.ContainsKey(identifier))
                {
                    Log.Fatal("Plugin {Plugin} not found", identifier);
                    pluginsFound = false;
                    break;
                }
                pluginActivators.Add(LoadedPlugins[identifier]);
            }

            if (!pluginsFound)
            {
                Log.Fatal($"Plugin chain was not constructed due to previous errors");
                return null;
            }

            var headPlugin = pluginActivators[0].Invoke();
            var currentPlugin = headPlugin;
            for (int i = 1; i < pluginActivators.Count; ++i)
            {
                currentPlugin.NextPlugin = pluginActivators[i].Invoke();
                currentPlugin = currentPlugin.NextPlugin;
            }

            return headPlugin;
        }     

        private void LoadPlugins()
        {
            this.LoadedAssemblies = new Dictionary<string, Assembly>();
            this.LoadedPlugins = new Dictionary<string, Func<BasePlugin>>();

            var directoryStack = new Stack<string>();
            var visitedDirectories = new HashSet<string>();
            directoryStack.Push(PluginsRootPath);
            visitedDirectories.Add(PluginsRootPath);

            while (directoryStack.Count != 0)
            {
                var currentDirectory = directoryStack.Pop();
                Log.Information("Searching for assemblies in {Directory}...", currentDirectory);
                foreach (var assemblyPath in Directory.GetFiles(currentDirectory)
                    .Where((f) => Path.GetExtension(f).Equals(".dll", StringComparison.InvariantCultureIgnoreCase)))
                {
                    var newAssembly = TryLoadAssembly(assemblyPath);
                    if (newAssembly != null)
                        RegisterPlugins(newAssembly);
                }

                foreach (var subdirectory in Directory.GetDirectories(currentDirectory)
                    .Where((d) => !visitedDirectories.Contains(d)))
                {
                    directoryStack.Push(subdirectory);
                    visitedDirectories.Add(subdirectory);
                }
            }
        }

        private Assembly TryLoadAssembly(string path)
        {
            Assembly newAssembly = null;
            try
            {
                newAssembly = Assembly.LoadFrom(path);
                Log.Information("Loaded assembly {Assembly}", newAssembly);
                LoadedAssemblies.Add(path, newAssembly);
            }
            catch (Exception e)
            {
                Log.Error(e, $"Could not load assembly due to");
            }

            return newAssembly;
        }

        private void RegisterPlugins(Assembly assembly)
        {
            foreach (var type in assembly.GetTypes()
                .Where((t) => t.IsSubclassOf(typeof(BasePlugin))))
            {                
                var pluginName = ActivatePlugin(type).PluginName;
                Log.Information("Found plugin {Plugin} in {Assembly}", pluginName, assembly);
                if (LoadedPlugins.ContainsKey(pluginName))
                    Log.Error("Plugin with name {Plugin} already exists - skipping", pluginName);
                else
                    LoadedPlugins.Add(pluginName, () => ActivatePlugin(type));
            }
        }

        private BasePlugin ActivatePlugin(Type type)
            => (BasePlugin)Activator.CreateInstance(type);

        #region SINGLETON_IMPLEMENTATION
        private static PluginsManager instance;
        public static PluginsManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new PluginsManager();
                return instance;
            }
        }
        private PluginsManager()
        {

        }
        #endregion
    }
}
