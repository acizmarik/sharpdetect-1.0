/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpDetect.Core.Utilities
{
    public class MethodDescriptor
    {
        public string Name { get; internal set; }
        public TypeDescriptor DeclaringType { get; internal set; }
        public TypeDescriptor ReturnValue { get; internal set; }
        public bool IsGeneric { get; internal set; }
        
        public ParameterDescriptor[] Parameters { get; internal set; }

        public override string ToString()
        {
            var parametersSb = new StringBuilder();
            if (Parameters != null)
            {
                foreach (var param in Parameters)
                {
                    if (parametersSb.Length != 0)
                        parametersSb.Append(',');
                    parametersSb.Append(param.ToString());
                }
            }
            var generic = (IsGeneric) ? "<?>" : string.Empty;

            return $"{ReturnValue} {DeclaringType}::{Name}{generic}({parametersSb.ToString()})";
        }
    }
}
