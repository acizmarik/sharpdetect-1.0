/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Common.Events;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SharpDetect.Core.Utilities
{
    public class SignatureResolver
    {
        private ConcurrentDictionary<string, MethodDescriptor> resolvedMethods;
        private ConcurrentDictionary<string, TypeDescriptor> resolvedTypes;
        private ConcurrentDictionary<string, ParameterDescriptor> resolvedParameters;
        private ConcurrentDictionary<string, FieldDescriptor> resolvedFields;
        private Type isVolatileType;

        public SignatureResolver()
        {
            this.resolvedMethods = new ConcurrentDictionary<string, MethodDescriptor>();
            this.resolvedParameters = new ConcurrentDictionary<string, ParameterDescriptor>();
            this.resolvedTypes = new ConcurrentDictionary<string, TypeDescriptor>();
            this.resolvedFields = new ConcurrentDictionary<string, FieldDescriptor>();
            this.isVolatileType = typeof(IsVolatile);
        }

        private bool IsMangled(string identifier)
        {
            if (identifier[0] == '_' || char.GetUnicodeCategory(identifier, 0) == System.Globalization.UnicodeCategory.LetterNumber)
                return false;

            return true;
        }

        private void ResolveMangledMethodIdentifier(string method, out string name, out bool isGeneric)
        {
            const char genericParametersStart = '<';
            const char genericParametersEnd = '>';

            // Non-generic mangled method
            if (method[method.Length - 1] != genericParametersEnd)
            {
                isGeneric = false;
                name = method;
                return;
            }

            // Generic mangled method
            isGeneric = true;
            name = method.Substring(0, method.LastIndexOf(genericParametersStart));
        }

        public MethodDescriptor ResolveMethod(string methodFullname)
        {
            // Make sure we check the cache first
            if (resolvedMethods.ContainsKey(methodFullname))
                return resolvedMethods[methodFullname];

            /*  What exactly are we parsing:
             *  - methodFullname = <Namespace>.<ReturnType> <Namespace>.<DeclaringType>::<Method>(<Parameters>)
             *  - <parameters> = <parameter>,<parameter>,...<parameter>
             *  
             *  - <Type> can additionally have generic parameters
             *  - <Method> can additionally have generic parameters
             *  - Also individual method parameters can have generic parameters
             */

            const char methodSignatureSeparator = ' ';
            const char methodIdentifierSeparator = ':';
            const char methodParametersSeparator1 = '(';
            const char methodParametersSeparator2 = ')';
            const char parameterSeparator = ',';
            const char genericParametersSeparator = '<';

            try
            {
                // <Namespace>.<ReturnType>
                var returnType = SubstringUntilCharacter(methodFullname, 0, methodSignatureSeparator);
                // <Namespace>.<Type>::<Method>(<parameters>)
                var methodSignature = methodFullname.Substring(returnType.Length + 1);
                // <Namespace>.<DeclaringType>
                var declaringType = SubstringUntilCharacter(methodSignature, 0, methodIdentifierSeparator);
                // <Method>
                if (methodSignature[declaringType.Length + 1] != methodIdentifierSeparator)
                    throw new DynamicAnalysisException($"Could not resolve {methodFullname} to a method.");
                var method = SubstringUntilCharacter(methodSignature, declaringType.Length + 2, methodParametersSeparator1);
                // <Parameters>
                var parametersStartIndex = methodSignature.IndexOf(methodParametersSeparator1);
                var parameters = SubstringUntilCharacter(methodSignature, parametersStartIndex + 1, methodParametersSeparator2);
                // <Parameter>,<Parameter>,...<Parameter>
                var parameterTypes = parameters.Split(new char[] { parameterSeparator }, StringSplitOptions.RemoveEmptyEntries);

                var returnTypeDescriptor = ResolveType(returnType);
                var declaringTypeDescriptor = ResolveType(declaringType);
                var parametersArray = (parameterTypes.Length != 0) ?
                    new ParameterDescriptor[parameterTypes.Length] : Array.Empty<ParameterDescriptor>();
                var paramIndex = 0;
                foreach (var parameterType in parameterTypes)
                    parametersArray[paramIndex++] = ResolveParameter(parameterType);

                // Parse method generics
                var hasGenerics = false;
                var methodName = string.Empty;

                if (IsMangled(method))
                    ResolveMangledMethodIdentifier(method, out methodName, out hasGenerics);
                else
                {
                    hasGenerics = (method.IndexOf(genericParametersSeparator) > 0);
                    if (hasGenerics)
                        methodName = SubstringUntilCharacter(method, 0, genericParametersSeparator);
                }

                var methodDescriptor = new MethodDescriptor()
                {
                    Name = methodName,
                    DeclaringType = declaringTypeDescriptor,
                    Parameters = parametersArray,
                    ReturnValue = returnTypeDescriptor,
                    IsGeneric = hasGenerics
                };
                resolvedMethods.GetOrAdd(methodFullname, methodDescriptor);
                return methodDescriptor;
            }
            catch (ArgumentException e)
            {
                throw new DynamicAnalysisException($"Could not resolve {methodFullname} to a method due to {e}.");
            }
        }

        public FieldDescriptor ResolveField(string fieldFullname, FieldFlags fieldFlags)
        {
            // Make sure we check cache first
            if (resolvedFields.ContainsKey(fieldFullname))
                return resolvedFields[fieldFullname];

            const char fieldTypeSeparator = ' ';
            const char fieldDeclaringTypeSeparator = ':';
            
            try
            {
                var fieldTokens = fieldFullname.Split(fieldTypeSeparator);
                if (fieldTokens.Length < 2)
                    throw new DynamicAnalysisException($"Could not resolve {fieldFullname} to a field.");

                var fieldType = fieldTokens.First();
                var fieldDeclaringTypeAndName = fieldTokens.Last();

                var fieldDeclaringTypeSeparatorIndex = fieldDeclaringTypeAndName.IndexOf(fieldDeclaringTypeSeparator);
                var fieldDeclaringType = fieldDeclaringTypeAndName.Substring(0, fieldDeclaringTypeSeparatorIndex);
                var fieldName = fieldDeclaringTypeAndName.Substring(fieldDeclaringTypeSeparatorIndex + 2); 

                var fieldTypeDescriptor = ResolveType(fieldType);
                var fieldDeclaringTypeDescriptor = ResolveType(fieldDeclaringType);

                var fieldDescriptor = new FieldDescriptor()
                {
                    Name = fieldName,
                    FieldType = fieldTypeDescriptor,
                    FieldFlags = fieldFlags,
                    DeclaringType = fieldDeclaringTypeDescriptor
                };
                resolvedFields.GetOrAdd(fieldFullname, fieldDescriptor);
                return fieldDescriptor;
            }
            catch (ArgumentException e)
            {
                throw new DynamicAnalysisException($"Could not resolve {fieldFullname} to a field due to {e}.");
            }
        }

        public TypeDescriptor ResolveType(string typeFullname)
        {
            // Make sure we check cache firstfa
            if (resolvedTypes.ContainsKey(typeFullname))
                return resolvedTypes[typeFullname];

            const char namespaceSeparator = '.';
            const char genericsSeparatorBegin = '<';
            const char genericsSeparatorEnd = '>';

            try
            {
                var hasGenerics = typeFullname[typeFullname.Length - 1] == genericsSeparatorEnd;
                var genericsStart = -1;

                if (hasGenerics)
                {
                    // Find generics start                    
                    var nested = -1;
                    for (var i = typeFullname.Length - 2; i > 0; i--)
                    {
                        if (typeFullname[i] == genericsSeparatorEnd)
                            --nested;
                        if (typeFullname[i] == genericsSeparatorBegin)
                            ++nested;

                        // We found it
                        if (nested == 0)
                        {
                            genericsStart = i;
                            break;
                        }
                    }
                }

                var typeIdentifier = (genericsStart != -1) ?
                    typeFullname.Substring(0, genericsStart) : typeFullname;

                var typeSeparatorIndex = typeIdentifier.LastIndexOf(namespaceSeparator);
                typeSeparatorIndex = (typeSeparatorIndex > -1) ? typeSeparatorIndex : 0;
                var typeNamespace = typeIdentifier.Substring(0, typeSeparatorIndex);
                var typeName = (typeSeparatorIndex > 0) ?
                    typeIdentifier.Substring(typeSeparatorIndex + 1) : typeIdentifier;

                var typeDescriptor = new TypeDescriptor()
                {
                    Name = typeName,
                    Namespace = typeNamespace,
                    IsGeneric = hasGenerics
                };
                resolvedTypes.GetOrAdd(typeFullname, typeDescriptor);

                return typeDescriptor;
            }
            catch (ArgumentException e)
            {
                throw new DynamicAnalysisException($"Could not resolve {typeFullname} to a type due to {e}.");
            }
        }

        public ParameterDescriptor ResolveParameter(string parameterFullname)
        {
            // Make sure we check cache first
            if (resolvedParameters.ContainsKey(parameterFullname))
                return resolvedParameters[parameterFullname];

            // Parameter can be basically parsed by type parser
            // However, we need to check passing by reference first

            const char byReferenceSign = '&';

            try
            {
                TypeDescriptor underlyingType = null;
                var parameterDescriptor = new ParameterDescriptor();
                var isByRef = (parameterFullname[parameterFullname.Length - 1] == byReferenceSign);
                if (isByRef)
                {
                    var typeFullname = parameterFullname.Substring(0, parameterFullname.Length - 1);
                    underlyingType = ResolveType(typeFullname);
                    parameterDescriptor.IsByReference = true;
                }
                else
                {
                    underlyingType = ResolveType(parameterFullname);
                    parameterDescriptor.IsByReference = false;
                }
                parameterDescriptor.Name = underlyingType.Name;
                parameterDescriptor.Namespace = underlyingType.Namespace;
                parameterDescriptor.IsGeneric = underlyingType.IsGeneric;

                resolvedParameters.GetOrAdd(parameterFullname, parameterDescriptor);
                return parameterDescriptor;
            }
            catch (ArgumentException e)
            {
                throw new DynamicAnalysisException($"Could not resolve {parameterFullname} to a parameter due to {e}.");
            }
        }

        private string SubstringUntilCharacter(string str, int start, char ch)
        {
            var fromIndex = str.IndexOf(ch);
            if (fromIndex == -1)
                throw new ArgumentException($"Character {ch} is not contained in {str}");

            return str.Substring(start, fromIndex - start);
        }
    }
}
