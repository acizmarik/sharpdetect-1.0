/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using NetMQ;
using NetMQ.Sockets;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CaseStudy
{
    class Program
    {
        static readonly TimeSpan Timeout = TimeSpan.FromSeconds(5);
        static readonly int TestRepetition = 1000;
        static readonly string TerminateString = "Goodbye!";

        public static void RunServer()
        {
            using (var server = new ResponseSocket())
            {
                var random = new Random();
                server.Bind("tcp://*:5555");
                while (true)
                {
                    var recvMessage = (NetMQMessage)null;
                    var resMessage = new NetMQMessage(1);
                    var result = server.TryReceiveMultipartMessage(Timeout, ref recvMessage);
                    if (!result)
                        break;

                    var payload = recvMessage.Pop().ConvertToString();
                    if (payload.StartsWith("Ping"))
                        resMessage.Append("Pong");
                    else if (payload == TerminateString)
                        break;
                    else
                        resMessage.Append($"Unknown command {payload}");

                    Thread.Sleep(random.Next((int)(Timeout.Milliseconds * 0.8)));

                    server.SendMultipartMessage(resMessage);
                }
            }
        }

        public static void RunClient()
        {
            using (var client = new RequestSocket())
            {
                client.Connect("tcp://localhost:5555");
                for (int i = 0; i < TestRepetition; ++i)
                {
                    var sendMessage = new NetMQMessage(1);
                    var resMessage = (NetMQMessage)null;
                    sendMessage.Append(new NetMQFrame($"Ping {i}"));

                    client.SendMultipartMessage(sendMessage);
                    var response = client.TryReceiveMultipartMessage(Timeout, ref resMessage);
                    if (!response)
                        return;
                }

                var terminateMessage = new NetMQMessage(1);
                terminateMessage.Append(new NetMQFrame(TerminateString));
                client.SendMultipartMessage(terminateMessage);
            }
        }

        static void Main(string[] args)
        {
            var serverTask = Task.Run(() => RunServer());
            var clientTask = Task.Run(() => RunClient());
            Task.WaitAll(serverTask, clientTask);
        }
    }
}
