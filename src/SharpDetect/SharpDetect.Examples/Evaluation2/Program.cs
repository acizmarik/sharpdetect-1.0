/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Evaluation2
{
    public class Program
    {
        const int size = 100;
        static Queue<int> queue;
        static object lockObj;

        static void Producer()
        {
            for (var i = 10000; i >= -1; i--)
            {
                lock (lockObj)
                {
                    if (queue.Count == size)
                        Monitor.Wait(lockObj);
                    queue.Enqueue(i);
                    Monitor.Pulse(lockObj);
                }
            }
        }

        static void Consumer()
        {
            var item = 0;
            do
            {
                lock (lockObj)
                {
                    if (queue.Count == 0)
                        Monitor.Wait(lockObj);
                    item = queue.Dequeue();
                    Monitor.Pulse(lockObj);
                }
            } while (item != -1);
        }

        public static void Main(string[] args)
        {
            queue = new Queue<int>(size);
            lockObj = new object();

            var producer = Task.Run(() => Producer());
            var consumer = Task.Run(() => Consumer());
            Task.WaitAll(producer, consumer);
        }
    }
}