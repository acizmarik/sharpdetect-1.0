/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpDetect.FunctionalTests.Checkers
{
    abstract class BaseChecker
    {
        public event EventHandler<string> ErrorFound;
        public event EventHandler<string> RequirementPassed;
        protected SignatureResolver Resolver { get; private set; }
        protected List<Requirements.BaseRequirement> Requirements { get; private set; }

        public BaseChecker(SignatureResolver resolver)
        {
            this.Resolver = resolver;
            this.Requirements = new List<Requirements.BaseRequirement>();
        }

        public void InjectionTerminated()
        {
            foreach (var requirement in Requirements)
            {
                requirement.Finish();
                foreach (var error in requirement.Errors)
                    ErrorFound?.Invoke(this, error);
                foreach (var success in requirement.Successes)
                    RequirementPassed?.Invoke(this, success);
            }
        }

        public virtual void AnalysisBegin()
        {
            foreach (var requirement in Requirements)
                requirement.AnalysisBegin();
        }

        public virtual void AnalysisEnd()
        {
            foreach (var requirement in Requirements)
                requirement.AnalysisEnd();
        }

        public virtual void MethodInjected(string injector, MethodDescriptor injectedMethod, MethodDescriptor callerMethod)
        {
            var injectorEvent = Helper.StringToInjectorEventTranslations[injector];
            foreach (var requirement in Requirements)
                requirement.Update(injectorEvent, injectedMethod, callerMethod);
        }

        public virtual void FieldInjected(string injector, FieldDescriptor injectedField, MethodDescriptor fromMethod)
        {
            var injectorEvent = Helper.StringToInjectorEventTranslations[injector];
            foreach (var requirement in Requirements)
                requirement.Update(injectorEvent, injectedField, fromMethod);
        }

        public virtual void ClassConstructed(string injector, TypeDescriptor injectedType)
        {
            var injectorEvent = Helper.StringToInjectorEventTranslations[injector];
            foreach (var requirement in Requirements)
                requirement.Update(injectorEvent, injectedType);
        }
    }
}
