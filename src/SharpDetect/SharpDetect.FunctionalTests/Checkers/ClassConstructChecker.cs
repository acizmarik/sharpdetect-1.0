/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Common;
using SharpDetect.FunctionalTests.Checkers.Requirements;
using SharpDetect.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.FunctionalTests.Checkers
{
    class ClassConstructChecker : BaseChecker
    {
        public ClassConstructChecker(SignatureResolver resolver)
            : base(resolver)
        {
            var testEntryPoint = $"System.Void {Helper.TestsNamespace}.ClassConstruct::Main(System.String[])";
            var typeA = $"{Helper.TestsNamespace}.A";
            var typeB = $"{Helper.TestsNamespace}.B";
            var typeC = $"{Helper.TestsNamespace}.C";
            var typeD = $"{Helper.TestsNamespace}.D";

            Requirements.Add(new TypeRequirement()
                .AddEvent(AnalysisEventType.ClassCreate)
                .SetInjectedType(resolver.ResolveType(typeA)));
            Requirements.Add(new TypeRequirement()
                .AddEvent(AnalysisEventType.ClassCreate)
                .SetInjectedType(resolver.ResolveType(typeB)));
            Requirements.Add(new TypeRequirement()
                .AddEvent(AnalysisEventType.ClassCreate)
                .SetInjectedType(resolver.ResolveType(typeC)));
            Requirements.Add(new TypeRequirement()
                .AddEvent(AnalysisEventType.ClassCreate, EventOccurrence.Never)
                .SetInjectedType(resolver.ResolveType(typeD)));
        }
    }
}
