/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Common;
using SharpDetect.FunctionalTests.Checkers.Requirements;
using SharpDetect.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using SharpDetect.Common.Events;

namespace SharpDetect.FunctionalTests.Checkers
{
    class FieldReadChecker : BaseChecker
    {
        public FieldReadChecker(SignatureResolver resolver)
            : base(resolver)
        {
            var testEntryPoint = $"System.Void {Helper.TestsNamespace}.FieldRead::Main(System.String[])";
            var isVolatileType = typeof(IsVolatile);

            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldRead)
                .SetInjectedField(Resolver.ResolveField($"System.Object {Helper.TestsNamespace}.FieldRead::StaticRefField", FieldFlags.Static)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldRead)
                .SetInjectedField(Resolver.ResolveField($"System.Tuple`2<?> {Helper.TestsNamespace}.FieldRead::StaticGenRefField", FieldFlags.Static)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldRead)
                .SetInjectedField(Resolver.ResolveField($"System.Int32 {Helper.TestsNamespace}.FieldRead::StaticValField", FieldFlags.Static)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldRead)
                .SetInjectedField(Resolver.ResolveField($"System.Collections.Generic.KeyValuePair`2<?> {Helper.TestsNamespace}.FieldRead::StaticGenValField", FieldFlags.Static)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldRead)
                .SetInjectedField(Resolver.ResolveField($"System.Int32 modreq({isVolatileType.FullName}) {Helper.TestsNamespace}.FieldRead::StaticVolatileValField", FieldFlags.Static | FieldFlags.Volatile)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldRead)
                .SetInjectedField(Resolver.ResolveField($"System.Object modreq({isVolatileType.FullName}) {Helper.TestsNamespace}.FieldRead::StaticVolatileRefField", FieldFlags.Static | FieldFlags.Volatile)));

            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldRead)
                .SetInjectedField(Resolver.ResolveField($"System.Object {Helper.TestsNamespace}.FieldRead::InstanceRefField", FieldFlags.None)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldRead)
                .SetInjectedField(Resolver.ResolveField($"System.Tuple`2<?> {Helper.TestsNamespace}.FieldRead::InstanceGenRefField", FieldFlags.None)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldRead)
                .SetInjectedField(Resolver.ResolveField($"System.Int32 {Helper.TestsNamespace}.FieldRead::InstanceValField", FieldFlags.None)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldRead)
                .SetInjectedField(Resolver.ResolveField($"System.Collections.Generic.KeyValuePair`2<?> {Helper.TestsNamespace}.FieldRead::InstanceGenValField", FieldFlags.None)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldRead)
                .SetInjectedField(Resolver.ResolveField($"System.Int32 modreq({isVolatileType.FullName}) {Helper.TestsNamespace}.FieldRead::InstanceVolatileValField", FieldFlags.Volatile)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldRead)
                .SetInjectedField(Resolver.ResolveField($"System.Object modreq({isVolatileType.FullName}) {Helper.TestsNamespace}.FieldRead::InstanceVolatileRefField", FieldFlags.Volatile)));

            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldRead)
                .SetInjectedField(Resolver.ResolveField($"System.Int32 {Helper.TestsNamespace}.Struct::ValField", FieldFlags.None)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldRead)
                .SetInjectedField(Resolver.ResolveField($"System.Object {Helper.TestsNamespace}.Struct::RefField", FieldFlags.None)));
        }
    }
}
