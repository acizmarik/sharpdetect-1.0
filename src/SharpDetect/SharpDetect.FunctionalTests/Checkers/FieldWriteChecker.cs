/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Common;
using SharpDetect.FunctionalTests.Checkers.Requirements;
using SharpDetect.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using SharpDetect.Common.Events;

namespace SharpDetect.FunctionalTests.Checkers
{
    class FieldWriteChecker : BaseChecker
    {
        public FieldWriteChecker(SignatureResolver resolver)
            : base(resolver)
        {
            var testEntryPoint = $"System.Void {Helper.TestsNamespace}.FieldWrite::Main(System.String[])";
            var isVolatileType = typeof(IsVolatile);

            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldWrite, EventOccurrence.ExactlyNTimes, 2)
                .SetInjectedField(Resolver.ResolveField($"System.Object {Helper.TestsNamespace}.FieldWrite::StaticRefField", FieldFlags.Static)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldWrite, EventOccurrence.ExactlyNTimes, 2)
                .SetInjectedField(Resolver.ResolveField($"System.Tuple`2<?> {Helper.TestsNamespace}.FieldWrite::StaticGenRefField", FieldFlags.Static)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldWrite, EventOccurrence.ExactlyNTimes, 2)
                .SetInjectedField(Resolver.ResolveField($"System.Int32 {Helper.TestsNamespace}.FieldWrite::StaticValField", FieldFlags.Static)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldWrite, EventOccurrence.ExactlyNTimes, 2)
                .SetInjectedField(Resolver.ResolveField($"System.Collections.Generic.KeyValuePair`2<?> {Helper.TestsNamespace}.FieldWrite::StaticGenValField", FieldFlags.Static)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldWrite, EventOccurrence.ExactlyNTimes, 2)
                .SetInjectedField(Resolver.ResolveField($"System.Int32 modreq({isVolatileType.FullName}) {Helper.TestsNamespace}.FieldWrite::StaticVolatileValField", FieldFlags.Static | FieldFlags.Volatile)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldWrite, EventOccurrence.ExactlyNTimes, 2)
                .SetInjectedField(Resolver.ResolveField($"System.Object modreq({isVolatileType.FullName}) {Helper.TestsNamespace}.FieldWrite::StaticVolatileRefField", FieldFlags.Static | FieldFlags.Volatile)));

            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldWrite, EventOccurrence.ExactlyNTimes, 1)
                .SetInjectedField(Resolver.ResolveField($"System.Object {Helper.TestsNamespace}.FieldWrite::InstanceRefField", FieldFlags.None)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldWrite, EventOccurrence.ExactlyNTimes, 1)
                .SetInjectedField(Resolver.ResolveField($"System.Tuple`2<?> {Helper.TestsNamespace}.FieldWrite::InstanceGenRefField", FieldFlags.None)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldWrite, EventOccurrence.ExactlyNTimes, 1)
                .SetInjectedField(Resolver.ResolveField($"System.Int32 {Helper.TestsNamespace}.FieldWrite::InstanceValField", FieldFlags.None)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldWrite, EventOccurrence.ExactlyNTimes, 1)
                .SetInjectedField(Resolver.ResolveField($"System.Collections.Generic.KeyValuePair`2<?> {Helper.TestsNamespace}.FieldWrite::InstanceGenValField", FieldFlags.None)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldWrite, EventOccurrence.ExactlyNTimes, 1)
                .SetInjectedField(Resolver.ResolveField($"System.Int32 modreq({isVolatileType.FullName}) {Helper.TestsNamespace}.FieldWrite::InstanceVolatileValField", FieldFlags.Volatile)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldWrite, EventOccurrence.ExactlyNTimes, 1)
                .SetInjectedField(Resolver.ResolveField($"System.Object modreq({isVolatileType.FullName}) {Helper.TestsNamespace}.FieldWrite::InstanceVolatileRefField", FieldFlags.Volatile)));

            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldWrite, EventOccurrence.ExactlyNTimes, 2)
                .SetInjectedField(Resolver.ResolveField($"System.Int32 {Helper.TestsNamespace}.Struct::ValField", FieldFlags.None)));
            Requirements.Add(new FieldRequirement()
                .AddEvent(AnalysisEventType.FieldWrite, EventOccurrence.ExactlyNTimes, 1)
                .SetInjectedField(Resolver.ResolveField($"System.Object {Helper.TestsNamespace}.Struct::RefField", FieldFlags.None)));
        }
    }
}
