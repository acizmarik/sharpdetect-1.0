/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.FunctionalTests.Checkers.Requirements;
using SharpDetect.Common;
using SharpDetect.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.FunctionalTests.Checkers.Methods
{
    class ReferenceCallsChecker : BaseChecker
    {
        public ReferenceCallsChecker(SignatureResolver resolver)
            : base(resolver)
        {
            var testEntryPoint = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceCalls::Main(System.String[])";

            
            var testStatic1 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::StaticMethod1(System.Int32)";
            var testStatic2 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::StaticMethod2(System.Object)";
            var testStatic3 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::StaticMethod3(System.Int32&)";
            var testStatic4 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::StaticMethod4(System.Int32&)";
            var testGenStatic1 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::StaticGenMethod1(RefType)";
            var testGenStatic2 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::StaticGenMethod2(ValType)";
            var testGenStatic3 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::StaticGenMethod3(ValType&)";
            var testGenStatic4 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::StaticGenMethod4(ValType&)";
            var testGenStatic5 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::StaticGenMethod5(RefType,ValType)";
            var testGenStatic6 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::StaticGenMethod6<?>(SomeType)";
            var testGenStatic7 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::StaticGenMethod7<?>(RefType,SomeType)";

            var testInstance1 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::InstanceMethod1(System.Int32)";
            var testInstance2 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::InstanceMethod2(System.Object)";
            var testInstance3 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::InstanceMethod3(System.Int32&)";
            var testInstance4 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::InstanceMethod4(System.Int32&)";
            var testInstanceGen1 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::InstanceGenMethod1(RefType)";
            var testInstanceGen2 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::InstanceGenMethod2(ValType)";
            var testInstanceGen3 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::InstanceGenMethod3(ValType&)";
            var testInstanceGen4 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::InstanceGenMethod4(ValType&)";
            var testInstanceGen5 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::InstanceGenMethod5(RefType,ValType)";
            var testInstanceGen6 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::InstanceGenMethod6<?>(SomeType)";
            var testInstanceGen7 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::InstanceGenMethod7<?>(RefType,SomeType)";

            var testVirtualInstance1 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::VirtualInstanceMethod1(System.Int32)";
            var testVirtualInstance2 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::VirtualInstanceMethod2(System.Object)";
            var testVirtualInstance3 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::VirtualInstanceMethod3(System.Int32&)";
            var testVirtualInstance4 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::VirtualInstanceMethod4(System.Int32&)";
            var testVirtualInstanceGen1 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::VirtualInstanceGenMethod1(RefType)";
            var testVirtualInstanceGen2 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::VirtualInstanceGenMethod2(ValType)";
            var testVirtualInstanceGen3 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::VirtualInstanceGenMethod3(ValType&)";
            var testVirtualInstanceGen4 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::VirtualInstanceGenMethod4(ValType&)";
            var testVirtualInstanceGen5 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::VirtualInstanceGenMethod5(RefType,ValType)";
            var testVirtualInstanceGen6 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::VirtualInstanceGenMethod6<?>(SomeType)";
            var testVirtualInstanceGen7 = $"System.Void {Helper.TestsNamespace}.Methods.ReferenceType`2<?>::VirtualInstanceGenMethod7<?>(RefType,SomeType)";

            void SetInjectedMethod(string method)
            {
                Requirements.Add(new MethodRequirement()
                    .SetInjectedMethod(Resolver.ResolveMethod(method))
                    .SetCallerMethod(Resolver.ResolveMethod(testEntryPoint))
                    .AddEvent(AnalysisEventType.MethodCall)
                    .AddEvent(AnalysisEventType.MethodReturn));
            }

            SetInjectedMethod(testStatic1);
            SetInjectedMethod(testStatic2);
            SetInjectedMethod(testStatic3);
            SetInjectedMethod(testStatic4);
            SetInjectedMethod(testGenStatic1);
            SetInjectedMethod(testGenStatic2);
            SetInjectedMethod(testGenStatic3);
            SetInjectedMethod(testGenStatic4);
            SetInjectedMethod(testGenStatic5);
            SetInjectedMethod(testGenStatic6);
            SetInjectedMethod(testGenStatic7);

            SetInjectedMethod(testInstance1);
            SetInjectedMethod(testInstance2);
            SetInjectedMethod(testInstance3);
            SetInjectedMethod(testInstance4);
            SetInjectedMethod(testInstanceGen1);
            SetInjectedMethod(testInstanceGen2);
            SetInjectedMethod(testInstanceGen3);
            SetInjectedMethod(testInstanceGen4);
            SetInjectedMethod(testInstanceGen5);
            SetInjectedMethod(testInstanceGen6);
            SetInjectedMethod(testInstanceGen7);

            SetInjectedMethod(testVirtualInstance1);
            SetInjectedMethod(testVirtualInstance2);
            SetInjectedMethod(testVirtualInstance3);
            SetInjectedMethod(testVirtualInstance4);
            SetInjectedMethod(testVirtualInstanceGen1);
            SetInjectedMethod(testVirtualInstanceGen2);
            SetInjectedMethod(testVirtualInstanceGen3);
            SetInjectedMethod(testVirtualInstanceGen4);
            SetInjectedMethod(testVirtualInstanceGen5);
            SetInjectedMethod(testVirtualInstanceGen6);
            SetInjectedMethod(testVirtualInstanceGen7);
        }
    }
}
