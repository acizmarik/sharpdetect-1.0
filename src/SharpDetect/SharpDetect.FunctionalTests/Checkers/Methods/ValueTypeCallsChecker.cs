/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.FunctionalTests.Checkers.Requirements;
using SharpDetect.Common;
using SharpDetect.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.FunctionalTests.Checkers.Methods
{
    class ValueTypeCallsChecker : BaseChecker
    {
        public ValueTypeCallsChecker(SignatureResolver resolver)
            : base(resolver)
        {
            var testEntryPoint = $"System.Void {Helper.TestsNamespace}.Methods.ValueTypeCalls::Main(System.String[])";

            var testStatic1 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::StaticMethod1(System.Int32)";
            var testStatic2 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::StaticMethod2(System.Object)";
            var testStatic3 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::StaticMethod3(System.Int32&)";
            var testStatic4 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::StaticMethod4(System.Int32&)";
            var testGenStatic1 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::StaticGenMethod1(RefType)";
            var testGenStatic2 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::StaticGenMethod2(ValType)";
            var testGenStatic3 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::StaticGenMethod3(ValType&)";
            var testGenStatic4 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::StaticGenMethod4(ValType&)";
            var testGenStatic5 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::StaticGenMethod5(RefType,ValType)";
            var testGenStatic6 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::StaticGenMethod6<?>(SomeType)";
            var testGenStatic7 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::StaticGenMethod7<?>(RefType,SomeType)";

            var testInstance1 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::InstanceMethod1(System.Int32)";
            var testInstance2 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::InstanceMethod2(System.Object)";
            var testInstance3 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::InstanceMethod3(System.Int32&)";
            var testInstance4 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::InstanceMethod4(System.Int32&)";
            var testInstanceGen1 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::InstanceGenMethod1(RefType)";
            var testInstanceGen2 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::InstanceGenMethod2(ValType)";
            var testInstanceGen3 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::InstanceGenMethod3(ValType&)";
            var testInstanceGen4 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::InstanceGenMethod4(ValType&)";
            var testInstanceGen5 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::InstanceGenMethod5(RefType,ValType)";
            var testInstanceGen6 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::InstanceGenMethod6<?>(SomeType)";
            var testInstanceGen7 = $"System.Void {Helper.TestsNamespace}.Methods.GenericValueType`2<?>::InstanceGenMethod7<?>(RefType,SomeType)";

            var testInstance1ns = $"System.Void {Helper.TestsNamespace}.Methods.NonGenericValueType::StaticMethod1(System.Int32)";
            var testInstance2ns = $"System.Void {Helper.TestsNamespace}.Methods.NonGenericValueType::StaticMethod2(System.Object)";
            var testInstance3ns = $"System.Void {Helper.TestsNamespace}.Methods.NonGenericValueType::StaticMethod3(System.Int32&)";
            var testInstance4ns = $"System.Void {Helper.TestsNamespace}.Methods.NonGenericValueType::StaticMethod4(System.Int32&)";
            var testInstanceGenns = $"System.Void {Helper.TestsNamespace}.Methods.NonGenericValueType::StaticGenMethod<?>(SomeType)";

            var testInstance1ni = $"System.Void {Helper.TestsNamespace}.Methods.NonGenericValueType::InstanceMethod1(System.Int32)";
            var testInstance2ni = $"System.Void {Helper.TestsNamespace}.Methods.NonGenericValueType::InstanceMethod2(System.Object)";
            var testInstance3ni = $"System.Void {Helper.TestsNamespace}.Methods.NonGenericValueType::InstanceMethod3(System.Int32&)";
            var testInstance4ni = $"System.Void {Helper.TestsNamespace}.Methods.NonGenericValueType::InstanceMethod4(System.Int32&)";
            var testInstanceGenni = $"System.Void {Helper.TestsNamespace}.Methods.NonGenericValueType::InstanceGenMethod<?>(SomeType)";

            var testInterfaceMethod = $"System.Void {Helper.TestsNamespace}.Methods.ISomeInterface::Method()";

            void SetInjectedMethod(string method)
            {
                Requirements.Add(new MethodRequirement()
                    .SetInjectedMethod(Resolver.ResolveMethod(method))
                    .SetCallerMethod(Resolver.ResolveMethod(testEntryPoint))
                    .AddEvent(AnalysisEventType.MethodCall)
                    .AddEvent(AnalysisEventType.MethodReturn));
            }

            SetInjectedMethod(testStatic1);
            SetInjectedMethod(testStatic2);
            SetInjectedMethod(testStatic3);
            SetInjectedMethod(testStatic4);
            SetInjectedMethod(testGenStatic1);
            SetInjectedMethod(testGenStatic2);
            SetInjectedMethod(testGenStatic3);
            SetInjectedMethod(testGenStatic4);
            SetInjectedMethod(testGenStatic5);
            SetInjectedMethod(testGenStatic6);
            SetInjectedMethod(testGenStatic7);

            SetInjectedMethod(testInstance1);
            SetInjectedMethod(testInstance2);
            SetInjectedMethod(testInstance3);
            SetInjectedMethod(testInstance4);
            SetInjectedMethod(testInstanceGen1);
            SetInjectedMethod(testInstanceGen2);
            SetInjectedMethod(testInstanceGen3);
            SetInjectedMethod(testInstanceGen4);
            SetInjectedMethod(testInstanceGen5);
            SetInjectedMethod(testInstanceGen6);
            SetInjectedMethod(testInstanceGen7);

            SetInjectedMethod(testInstance1ns);
            SetInjectedMethod(testInstance2ns);
            SetInjectedMethod(testInstance3ns);
            SetInjectedMethod(testInstance4ns);
            SetInjectedMethod(testInstanceGenns);
            SetInjectedMethod(testInstance1ni);
            SetInjectedMethod(testInstance2ni);
            SetInjectedMethod(testInstance3ni);
            SetInjectedMethod(testInstance4ni);
            SetInjectedMethod(testInstanceGenni);

            SetInjectedMethod(testInterfaceMethod);
        }
    }
}
