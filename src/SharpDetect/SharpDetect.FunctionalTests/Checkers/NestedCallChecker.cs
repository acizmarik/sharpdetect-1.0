/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Common;
using SharpDetect.FunctionalTests.Checkers.Requirements;
using SharpDetect.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.FunctionalTests.Checkers
{
    class NestedCallChecker : BaseChecker
    {
        public NestedCallChecker(SignatureResolver resolver)
            : base(resolver)
        {
            var testEntryPoint = $"System.Void {Helper.TestsNamespace}.Methods.NestedCall::Main(System.String[])";
            var methodA = $"System.Void {Helper.TestsNamespace}.Methods.NestedCall::MethodA()";
            var methodB = $"System.Void {Helper.TestsNamespace}.Methods.NestedCall::MethodB()";
            var methodC = $"System.Void {Helper.TestsNamespace}.Methods.NestedCall::MethodC()";

            Requirements.Add(new MethodRequirement()
                .SetInjectedMethod(Resolver.ResolveMethod(methodA))
                .SetCallerMethod(Resolver.ResolveMethod(testEntryPoint))
                .AddEvent(AnalysisEventType.MethodCall)
                .AddEvent(AnalysisEventType.MethodReturn));
            Requirements.Add(new MethodRequirement()
                .SetInjectedMethod(Resolver.ResolveMethod(methodB))
                .SetCallerMethod(Resolver.ResolveMethod(methodA))
                .AddEvent(AnalysisEventType.MethodCall)
                .AddEvent(AnalysisEventType.MethodReturn));
            Requirements.Add(new MethodRequirement()
                .SetInjectedMethod(Resolver.ResolveMethod(methodC))
                .SetCallerMethod(Resolver.ResolveMethod(methodB))
                .AddEvent(AnalysisEventType.MethodCall)
                .AddEvent(AnalysisEventType.MethodReturn));


            Requirements.Add(new MethodRequirement()
                .SetInjectedMethod(Resolver.ResolveMethod(Helper.MonitorEnter))
                .SetCallerMethod(Resolver.ResolveMethod(methodC))
                .AddEvent(AnalysisEventType.MethodCall)
                .AddEvent(AnalysisEventType.MethodReturn));

            Requirements.Add(new MethodRequirement()
                .SetInjectedMethod(Resolver.ResolveMethod(Helper.MonitorExit))
                .SetCallerMethod(Resolver.ResolveMethod(methodC))
                .AddEvent(AnalysisEventType.MethodCall)
                .AddEvent(AnalysisEventType.MethodReturn));
        }
    }
}
