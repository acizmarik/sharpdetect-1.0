/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Common;
using SharpDetect.FunctionalTests.Checkers.Requirements;
using SharpDetect.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.FunctionalTests.Checkers
{
    class NestedClassesChecker : BaseChecker
    {
        public NestedClassesChecker(SignatureResolver resolver)
            : base(resolver)
        {
            var testEntryPoint = $"System.Void {Helper.TestsNamespace}.NestedClasses::Main(System.String[])";
            var classAMethod = $"System.Void {Helper.TestsNamespace}.NestedClasses/A::Method()";
            var classBMethod = $"System.Void {Helper.TestsNamespace}.NestedClasses/B::Method()";

            Requirements.Add(new MethodRequirement()
                .SetInjectedMethod(Resolver.ResolveMethod(classAMethod))
                .SetCallerMethod(Resolver.ResolveMethod(testEntryPoint))
                .AddEvent(AnalysisEventType.MethodCall)
                .AddEvent(AnalysisEventType.MethodReturn));

            Requirements.Add(new MethodRequirement()
                .SetInjectedMethod(Resolver.ResolveMethod(classBMethod))
                .SetCallerMethod(Resolver.ResolveMethod(classAMethod))
                .AddEvent(AnalysisEventType.MethodCall)
                .AddEvent(AnalysisEventType.MethodReturn));
        }
    }
}
