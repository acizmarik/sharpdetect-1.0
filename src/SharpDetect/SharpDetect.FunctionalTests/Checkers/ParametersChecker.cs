/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Common;
using SharpDetect.FunctionalTests.Checkers.Requirements;
using SharpDetect.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.FunctionalTests.Checkers
{
    class ParametersChecker : BaseChecker
    {
        public ParametersChecker(SignatureResolver resolver)
            : base(resolver)
        {
            var methodRef = $"System.Void {Helper.TestsNamespace}.Parameters::MethodRefType(System.Object)";
            var methodRefCaller = $"System.Void {Helper.TestsNamespace}.Parameters::MethodRefTypeCaller()";
            var methodEnum = $"System.Void {Helper.TestsNamespace}.Parameters::MethodEnum(System.UriHostNameType)";
            var methodEnumCaller = $"System.Void {Helper.TestsNamespace}.Parameters::MethodEnumCaller()";
            var methodVal = $"System.Void {Helper.TestsNamespace}.Parameters::MethodValueType(System.Int32)";
            var methodValCaller = $"System.Void {Helper.TestsNamespace}.Parameters::MethodValueTypeCaller()";
            var methodValOut = $"System.Void {Helper.TestsNamespace}.Parameters::MethodValueTypeOut(System.Int32&)";
            var methodValOutCaller = $"System.Void {Helper.TestsNamespace}.Parameters::MethodValueTypeOutCaller()";
            var methodValRef = $"System.Void {Helper.TestsNamespace}.Parameters::MethodValueTypeRef(System.Int32&)";
            var methodValRefCaller = $"System.Void {Helper.TestsNamespace}.Parameters::MethodValueTypeRefCaller()";

            Requirements.Add(new MethodRequirement()
                .SetInjectedMethod(Resolver.ResolveMethod(methodRef))
                .SetCallerMethod(Resolver.ResolveMethod(methodRefCaller))
                .AddEvent(AnalysisEventType.MethodCall)
                .AddEvent(AnalysisEventType.MethodReturn));

            Requirements.Add(new MethodRequirement()
                .SetInjectedMethod(Resolver.ResolveMethod(methodEnum))
                .SetCallerMethod(Resolver.ResolveMethod(methodEnumCaller))
                .AddEvent(AnalysisEventType.MethodCall)
                .AddEvent(AnalysisEventType.MethodReturn));

            Requirements.Add(new MethodRequirement()
                .SetInjectedMethod(Resolver.ResolveMethod(methodVal))
                .SetCallerMethod(Resolver.ResolveMethod(methodValCaller))
                .AddEvent(AnalysisEventType.MethodCall)
                .AddEvent(AnalysisEventType.MethodReturn));

            Requirements.Add(new MethodRequirement()
                .SetInjectedMethod(Resolver.ResolveMethod(methodValOut))
                .SetCallerMethod(Resolver.ResolveMethod(methodValOutCaller))
                .AddEvent(AnalysisEventType.MethodCall)
                .AddEvent(AnalysisEventType.MethodReturn));

            Requirements.Add(new MethodRequirement()
                .SetInjectedMethod(Resolver.ResolveMethod(methodValRef))
                .SetCallerMethod(Resolver.ResolveMethod(methodValRefCaller))
                .AddEvent(AnalysisEventType.MethodCall)
                .AddEvent(AnalysisEventType.MethodReturn));
        }
    }
}
