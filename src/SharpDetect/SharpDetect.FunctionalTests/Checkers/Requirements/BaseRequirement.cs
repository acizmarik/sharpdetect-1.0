/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Common;
using SharpDetect.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpDetect.FunctionalTests.Checkers.Requirements
{
    abstract class BaseRequirement
    {        
        public virtual bool Satisfied { get { return Errors.Count == 0; } }
        public List<string> Errors { get; private set; }
        public List<string> Successes { get; private set; }
        protected Dictionary<AnalysisEventType, (EventOccurrence, int, int)> EventsData { get; private set; }
        protected abstract string InjectionTargetDescription { get; }

        public BaseRequirement()
        {
            this.Errors = new List<string>();
            this.Successes = new List<string>();
            this.EventsData = new Dictionary<AnalysisEventType, (EventOccurrence, int, int)>();
        }

        public virtual void AnalysisBegin()
        {
            /* Ignore */
        }

        public virtual void AnalysisEnd()
        {
            /* Ignore */
        }

        public virtual void Update(AnalysisEventType injectorEvent, MethodDescriptor injected, MethodDescriptor caller)
        {
            /* Ignore */
        }

        public virtual void Update(AnalysisEventType injectorEvent, FieldDescriptor injected, MethodDescriptor caller)
        {
            /* Ignore */
        }

        public virtual void Update(AnalysisEventType injectorEvent, TypeDescriptor injected)
        {
            /* Ignore */
        }

        public virtual void Finish()
        {
            foreach (var record in EventsData)
            {
                var eventName = record.Key;
                var (occurrence, required, current) = record.Value;

                switch (occurrence)
                {
                    case EventOccurrence.ExactlyNTimes:
                        if (current != required)
                            Errors.Add($"Event: {eventName} was expected to occur exactly {required}-times. [{InjectionTargetDescription}]");
                        else
                            Successes.Add($"Event: {eventName} occurred exactly {required}-times. [{InjectionTargetDescription}]");
                        break;
                    case EventOccurrence.Never:
                        if (current != 0)
                            Errors.Add($"Event: {eventName} was expected to never occur. [{InjectionTargetDescription}]");
                        else
                            Successes.Add($"Event: {eventName} never occurred. [{InjectionTargetDescription}]");
                        break;
                }
            }
        }
    }
}
