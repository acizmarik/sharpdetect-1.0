/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDetect.Common;
using SharpDetect.Common.Metadata;
using SharpDetect.Core.Utilities;

namespace SharpDetect.FunctionalTests.Checkers.Requirements
{
    class FieldRequirement : BaseRequirement
    {
        public bool InjectedFieldSet { get; protected set; }
        public bool CallerMethodSet { get; protected set; }
        public MethodDescriptor CallerMethod { get; protected set; }
        public FieldDescriptor InjectedField { get; protected set; }
        protected override string InjectionTargetDescription => InjectedField.ToString();

        public FieldRequirement AddEvent(AnalysisEventType injectorEvent, EventOccurrence occurrence = EventOccurrence.ExactlyNTimes, int count = 1)
        {
            EventsData.Add(injectorEvent, (occurrence, count, 0));
            return this;
        }

        public FieldRequirement SetCallerMethod(MethodDescriptor caller)
        {
            if (CallerMethodSet)
                throw new InvalidOperationException("Caller method already set");

            CallerMethodSet = true;
            CallerMethod = caller;
            return this;
        }

        public virtual FieldRequirement SetInjectedField(FieldDescriptor field)
        {
            if (InjectedFieldSet)
                throw new InvalidOperationException("Injected field already set");

            InjectedFieldSet = true;
            InjectedField = field;
            return this;
        }

        public override void Update(AnalysisEventType injectorEvent, FieldDescriptor injected, MethodDescriptor caller)
        {
            void RegisterEvent()
            {
                var (occurrence, required, tracked) = EventsData[injectorEvent];
                EventsData[injectorEvent] = (occurrence, required, ++tracked);
            }

            if (!EventsData.ContainsKey(injectorEvent))
                return;

            if (CallerMethodSet)
            {
                if (caller.DeclaringType.Namespace != CallerMethod.DeclaringType.Namespace)
                    return;
                if (caller.DeclaringType.Name != CallerMethod.DeclaringType.Name)
                    return;
                if (caller.Name != CallerMethod.Name)
                    return;
            }

            if (injected.FieldType.Namespace != InjectedField.FieldType.Namespace)
                return;
            if (injected.FieldType.Name != InjectedField.FieldType.Name)
                return;
            if (injected.DeclaringType.Namespace != InjectedField.DeclaringType.Namespace)
                return;
            if (injected.DeclaringType.Name != InjectedField.DeclaringType.Name)
                return;
            if (injected.Name != InjectedField.Name)
                return;

            RegisterEvent();
        }
    }
}
