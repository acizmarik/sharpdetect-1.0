/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDetect.Common;
using SharpDetect.Common.Metadata;
using SharpDetect.Core.Utilities;

namespace SharpDetect.FunctionalTests.Checkers.Requirements
{
    class MethodRequirement : BaseRequirement
    {
        public bool InjectedMethodSet { get; protected set; }
        public bool CallerMethodSet { get; protected set; }
        public MethodDescriptor CallerMethod { get; protected set; }
        public MethodDescriptor InjectedMethod { get; protected set; }
        protected override string InjectionTargetDescription => InjectedMethod.ToString();

        public MethodRequirement AddEvent(AnalysisEventType injectorEvent, EventOccurrence occurrence = EventOccurrence.ExactlyNTimes, int count = 1)
        {
            EventsData.Add(injectorEvent, (occurrence, count, 0));
            return this;
        }

        public virtual MethodRequirement SetInjectedMethod(MethodDescriptor method)
        {
            if (InjectedMethodSet)
                throw new InvalidOperationException("Injected method already set");

            InjectedMethodSet = true;
            InjectedMethod = method;
            return this;
        }

        public virtual MethodRequirement SetCallerMethod(MethodDescriptor method)
        {
            if (CallerMethodSet)
                throw new InvalidOperationException("Caller method already set");

            CallerMethodSet = true;
            CallerMethod = method;
            return this;
        }

        public override void Update(AnalysisEventType injectorEvent, MethodDescriptor injected, MethodDescriptor caller)
        {
            void RegisterEvent()
            {
                var (occurrence, required, tracked) = EventsData[injectorEvent];
                EventsData[injectorEvent] = (occurrence, required, ++tracked);
            }

            if (!EventsData.ContainsKey(injectorEvent))
                return;

            if (CallerMethodSet)
            {
                if (caller.DeclaringType.Namespace != CallerMethod.DeclaringType.Namespace)
                    return;
                if (caller.DeclaringType.Name != CallerMethod.DeclaringType.Name)
                    return;
                if (caller.Name != CallerMethod.Name)
                    return;
            }

            if (injected.DeclaringType.Namespace != InjectedMethod.DeclaringType.Namespace)
                return;
            if (injected.DeclaringType.Name != InjectedMethod.DeclaringType.Name)
                return;
            if (injected.Name != InjectedMethod.Name)
                return;

            RegisterEvent();
        }
    }
}
