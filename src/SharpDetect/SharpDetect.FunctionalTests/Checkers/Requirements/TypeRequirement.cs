/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Common;
using SharpDetect.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.FunctionalTests.Checkers.Requirements
{
    class TypeRequirement : BaseRequirement
    {
        public bool InjectedTypeSet { get; private set; }
        public TypeDescriptor InjectedType { get; private set; }
        protected override string InjectionTargetDescription => InjectedType.ToString();

        public TypeRequirement AddEvent(AnalysisEventType injectorEvent, EventOccurrence occurrence = EventOccurrence.ExactlyNTimes, int count = 1)
        {
            EventsData.Add(injectorEvent, (occurrence, count, 0));
            return this;
        }

        public TypeRequirement SetInjectedType(TypeDescriptor type)
        {
            if (InjectedTypeSet)
                throw new InvalidOperationException("Injected type already set");

            InjectedTypeSet = true;
            InjectedType = type;
            return this;
        }

        public override void Update(AnalysisEventType injectorEvent, TypeDescriptor injected)
        {
            void RegisterEvent()
            {
                var (occurrence, required, tracked) = EventsData[injectorEvent];
                EventsData[injectorEvent] = (occurrence, required, ++tracked);
            }

            if (!EventsData.ContainsKey(injectorEvent))
                return;

            if (injected.Namespace != InjectedType.Namespace)
                return;
            if (injected.Name != InjectedType.Name)
                return;

            RegisterEvent();
        }
    }
}
