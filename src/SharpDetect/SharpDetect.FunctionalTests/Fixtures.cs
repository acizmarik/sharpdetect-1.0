/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using CliWrap;
using SharpDetect.FunctionalTests.Runners;
using NUnit;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SharpDetect.FunctionalTests
{
    [SetUpFixture]
    class Fixtures
    {
        internal static List<string> Dependencies;

        static Fixtures()
        {
            var path = Path.Combine("..", "..", "..", "..", 
                "SharpDetect.Console", "bin", "Debug", "netcoreapp2.1");

            Dependencies = new List<string>()
            {
                Path.Combine(path, "SharpDetect.Console.dll"),
                Path.Combine(path, "SharpDetect.Common.dll"),
                Path.Combine(path, "SharpDetect.Core.dll"),
                Path.Combine(path, "CommandLine.dll"),
                Path.Combine(path, "dnlib.dll"),
                Path.Combine(path, "Serilog.dll"),
                Path.Combine(path, "Serilog.Sinks.Console.dll"),
                Path.Combine(path, "Serilog.Sinks.File.dll")
            };
        }

        [OneTimeSetUp]
        public void Setup()
        {
            if (Directory.Exists(TestRunner.TestDir))
                return;
            else
            {
                Directory.CreateDirectory(TestRunner.TestDir);
                Directory.CreateDirectory(TestRunner.BuildDir);
                Directory.CreateDirectory(TestRunner.PublishDir);

                // Copy dependencies
                foreach (var lib in Dependencies)
                    File.Copy(lib, Path.Combine(TestRunner.PublishDir, Path.GetFileName(lib)));
            }

            // Setup empty project
            Directory.SetCurrentDirectory(TestRunner.TestDir);
            Cli.Wrap(TestRunner.Terminal)
                .SetStandardErrorCallback((args) => Assert.Fail(args))
                .SetArguments($"{TestRunner.CommandPrefix} \"dotnet new console --force\"")
                .Execute();
            
            // Publish its dependencies
            Cli.Wrap(TestRunner.Terminal)
                .SetStandardErrorCallback((args) => Assert.Fail(args))
                .SetArguments($"{TestRunner.CommandPrefix} " +
                    $"\"dotnet publish _Test.csproj --runtime {TestRunner.RID} --output {TestRunner.PublishDirName}\"")
                .Execute();
            Directory.SetCurrentDirectory("..");
         }

        [OneTimeTearDown]
        public void TearDown()
        {
            // TODO
        }
    }
}
