/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Common;
using SharpDetect.FunctionalTests.Checkers.Requirements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpDetect.FunctionalTests
{
    static class Helper
    {
        public static Dictionary<AnalysisEventType, string> InjectorEventToStringTranslations;
        public static Dictionary<string, AnalysisEventType> StringToInjectorEventTranslations;

        public const string TestsNamespace = @"SharpDetect.FunctionalTests.Tests";

        public const string MonitorEnter = @"System.Void System.Threading.Monitor::Enter(System.Object, System.Boolean&)";
        public const string MonitorExit = @"System.Void System.Threading.Monitor::Exit(System.Object)";
        public const string MonitorPulse = @"System.Void System.Threading.Monitor::Pulse()";
        public const string MonitorPulseAll = @"System.Void System.Threading.Monitor::PulseAll()";
        public const string MonitorWait = @"System.Void System.Threading.Monitor::Wait()";

        public const string ThreadCreate = @"System.Void System.Threading.Thread::.ctor()";
        public const string ThreadStart = @"System.Void System.Threading.Thread::Start()";

        static Helper()
        {
            InjectorEventToStringTranslations = new Dictionary<AnalysisEventType, string>();
            foreach (var @event in Enum.GetValues(typeof(AnalysisEventType)))
            {
                var value = Enum.GetName(typeof(AnalysisEventType), @event);
                InjectorEventToStringTranslations.Add((AnalysisEventType)@event, value);
            }

            StringToInjectorEventTranslations = new Dictionary<string, AnalysisEventType>();
            foreach (var (key, value) in InjectorEventToStringTranslations)
                StringToInjectorEventTranslations.Add(value, key);
        }      
    }
}
