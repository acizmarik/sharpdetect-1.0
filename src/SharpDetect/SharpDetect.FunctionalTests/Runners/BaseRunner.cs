/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using CliWrap;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace SharpDetect.FunctionalTests.Runners
{
    abstract class BaseRunner
    {
        public static string TestAssembly = "_Test.dll";
        public static string TestDirName = "_Test";
        public static string PublishDirName = "_publish";
        public static string BuildDirName = "_build";
        public static string TestDir = TestDirName;
        public static string PublishDir = Path.Combine(TestDir, PublishDirName);
        public static string BuildDir = Path.Combine(TestDir, BuildDirName);

        public static string Terminal { get; private set; }
        public static string CommandPrefix { get; private set; }
        public static string RID { get; private set; }

#if DEBUG
        protected string BuildConfiguration { get; private set; } = "Debug";
#else
        protected string BuildConfiguration { get; private set; } = "Release";
#endif

        private static bool IsLinux()
            => RuntimeInformation.IsOSPlatform(OSPlatform.Linux);
        private static bool IsWindows()
            => RuntimeInformation.IsOSPlatform(OSPlatform.Windows);

        static BaseRunner()
        {
            // Determine OS platform
            if (IsLinux())
            {
                Terminal = "/bin/bash";
                CommandPrefix = "-c ";
            }
            else if (IsWindows())
            {
                Terminal = "powershell";
                CommandPrefix = "/c";
            }
            else
                throw new NotSupportedException("Unknown OS");

            // Get RID
            var dotnetInfo = new List<string>();
            Cli.Wrap(Terminal)
                .SetArguments($"{CommandPrefix} \"dotnet --info\"")
                .SetStandardErrorCallback((args) => Assert.Fail($"Unable to get information about runtime: {args}"))
                .SetStandardOutputCallback((args) => dotnetInfo.Add(args))
                .Execute();

            foreach (var line in dotnetInfo)
            {
                if (!line.Trim().StartsWith("RID:"))
                    continue;

                RID = line.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries).Last().Trim();
            }

            if (RID == null)
                throw new NotSupportedException("Unknown OS");
        }

    }
}
