/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using CliWrap;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SharpDetect.FunctionalTests.Runners.Jobs
{
    class Executor
    {
        public virtual void PerformAnalysisTest(string sharpDetectConsole, string sharpDetectPlugins)
        {
            var eventOffset = "23:59:99 [INF] ".Length;
            var success = false;
            var error = string.Empty;

            Directory.SetCurrentDirectory(BaseRunner.PublishDir);
            try
            {
                var errorLines = new List<string>();
                File.Delete("_Test.deps.json");
                File.Delete("_Test.runtimeconfig.json");

                var commandBuilder = new StringBuilder();
                commandBuilder.Append($"{BaseRunner.CommandPrefix} \"dotnet ");
                commandBuilder.Append($"{Path.Combine("..", sharpDetectConsole)} run ");
                commandBuilder.Append($"--level Information --config EchoPlugin --plugins {sharpDetectPlugins} ");
                commandBuilder.Append($"_Test.dll\"");

                Cli.Wrap(BaseRunner.Terminal)
                    .SetArguments(commandBuilder.ToString())
                    .SetStandardErrorCallback(args => errorLines.Add(args))
                    .SetStandardOutputCallback(arg =>
                    {
                        if (arg.Length > eventOffset && arg[eventOffset] == '#' && arg.Contains("Analysis ends"))
                            success = true;
                    })
                    .EnableExitCodeValidation(false)
                    .Execute();

                var sb = new StringBuilder();
                foreach (var errorLine in errorLines)
                    sb.AppendLine(errorLine);
                error = sb.ToString();
            }
            finally
            {
                Directory.SetCurrentDirectory($"..{Path.DirectorySeparatorChar}..");
            }

            if (error.Length > 0)
                Assert.Fail(error);
            Assert.IsTrue(success, "Analysis ended too soon (this may be caused by an invalid program)");
        }
    }
}
