/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using CliWrap;
using SharpDetect.FunctionalTests.Checkers;
using SharpDetect.Common;
using SharpDetect.Core.Utilities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SharpDetect.FunctionalTests.Runners.Jobs
{
    class Instrumentor
    {
        protected readonly int EventInfoLogOffset = "23:59:99 [INF] ".Length;
        protected string LastInjectionLogLine { get; private set; }

        public virtual void Instrument(BaseChecker checker, SignatureResolver resolver, string sharpDetectConsole)
        {
            ApplyNewCompiledAssemblies();

            var errorLines = new List<string>();
            var error = string.Empty;

            if (checker != null)
                checker.ErrorFound += (sender, args) => errorLines.Add(args);

            var publishDirAbsolute = Path.GetFullPath(BaseRunner.PublishDir);
            var commandBuilder = new StringBuilder();
            commandBuilder.Append($"{BaseRunner.CommandPrefix} \"dotnet ");
            commandBuilder.Append($"{sharpDetectConsole} instrument --level Information ");
            commandBuilder.Append($"{Path.Combine(publishDirAbsolute, "_Test.json")}\"");

            // Make sure the injector did its job correctly
            Directory.SetCurrentDirectory(BaseRunner.TestDir);
            try
            {

                Cli.Wrap(BaseRunner.Terminal)
                    .SetArguments(commandBuilder.ToString())
                    .SetStandardErrorCallback((args) => errorLines.Add(args))
                    .SetStandardOutputCallback((args) =>
                    {
                        if (checker == null)
                            return;

                        ProcessInjectionLogLine(args, checker, resolver);
                    })
                    .EnableExitCodeValidation(false)
                    .Execute();

                if (checker != null)
                    checker.InjectionTerminated();
            }
            finally
            {
                Directory.SetCurrentDirectory("..");
            }

            var sb = new StringBuilder();
            foreach (var errorLine in errorLines)
                sb.AppendLine(errorLine);

            error = sb.ToString();
            if (error.Length > 0)
                Assert.Fail(error);
        }

        protected void ProcessInjectionLogLine(string line, BaseChecker checker, SignatureResolver resolver)
        {
            var threadId = System.Threading.Thread.CurrentThread.ManagedThreadId;
            if (line[EventInfoLogOffset] == '[')
                LastInjectionLogLine = line.Substring(15);
            else if (line.StartsWith("\t\tat "))
            {
                var tokens = LastInjectionLogLine.Split(' ');
                var injectorName = tokens[0].Substring(1, tokens[0].Length - 2);
                var injectorEvent = Helper.StringToInjectorEventTranslations[injectorName];
                /* Ignore token[1] - CIL address */
                /* Ignore token[2] - CIL OpCode */
                var callerMethod = line.Substring("\t\tat ".Length);
                var callerMethodDescriptor = resolver.ResolveMethod(callerMethod);
                switch (injectorEvent)
                {
                    case AnalysisEventType.ArrayCreate:
                    case AnalysisEventType.ArrayRead:
                    case AnalysisEventType.ArrayWrite:
                        /* TODO */
                        break;
                    case AnalysisEventType.ClassCreate:
                        var cctor = line.Split(" ").Last();
                        var type = resolver.ResolveType(cctor.Substring(0, cctor.LastIndexOf(':') - 1));
                        checker.ClassConstructed(injectorName, type);
                        break;
                    case AnalysisEventType.FieldRead:
                    case AnalysisEventType.FieldWrite:
                        // Concat field type with its namespace and name
                        var injectedField = string.Join(" ", tokens.Skip(3));
                        var injectedFieldDescriptor = resolver.ResolveField(injectedField, Common.Events.FieldFlags.None);
                        checker.FieldInjected(injectorName, injectedFieldDescriptor, callerMethodDescriptor);
                        break;
                    case AnalysisEventType.MethodCall:
                    case AnalysisEventType.MethodReturn:
                    case AnalysisEventType.ObjectCreate:
                        // Concat return value with the rest of method signature
                        var injectedMethod = string.Concat(tokens[3], " ", tokens[4]);
                        var injectedMethodDescriptor = resolver.ResolveMethod(injectedMethod);
                        checker.MethodInjected(injectorName, injectedMethodDescriptor, callerMethodDescriptor);
                        break;
                }
            }
        }

        private void ApplyNewCompiledAssemblies()
        {
            foreach (var file in Directory.GetFiles(BaseRunner.BuildDir))
            {
                if (Path.GetFileName(file).EndsWith(".dll"))
                {
                    var oldFilename = Path.GetFileName(file);
                    var newFilename = Path.Combine(BaseRunner.PublishDir, oldFilename);
                    // Apply newly built
                    File.Copy(file, newFilename, true);
                    File.Delete(Path.Combine(BaseRunner.PublishDir, $"{oldFilename}_clean"));
                }
            }
        }
    }
}
