/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using CliWrap;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SharpDetect.FunctionalTests.Runners
{
    class Publisher
    {
        public virtual void Publish(string sourcePath, string configPath)
        {
            var commandBuilder = new StringBuilder();
            File.Copy(configPath, Path.Combine(BaseRunner.PublishDir, "_Test.json"), true);
            File.Copy(sourcePath, Path.Combine(BaseRunner.TestDir, "Program.cs"), true);

            // Clean previous build
            var fullName = Path.GetFullPath(BaseRunner.BuildDir);
            Directory.SetCurrentDirectory(BaseRunner.TestDir);
            if (Directory.Exists("bin"))
                Directory.Delete("bin", true);
            if (Directory.Exists("obj"))
                Directory.Delete("obj", true);
            // Try to compile
            try
            {
                var newCompilationTask = Cli.Wrap(BaseRunner.Terminal)
                    .SetArguments($"{BaseRunner.CommandPrefix} \"dotnet build --output {fullName}\"")
                    .SetStandardErrorCallback((args) => Assert.Fail(args))
                    .EnableExitCodeValidation(false)
                    .Execute();
            }
            finally
            {
                Directory.SetCurrentDirectory("..");
            }
        }
    }
}
