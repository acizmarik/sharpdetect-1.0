/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using CliWrap;
using SharpDetect.FunctionalTests.Checkers;
using SharpDetect.FunctionalTests.Runners.Jobs;
using SharpDetect.Common;
using SharpDetect.Core.Utilities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace SharpDetect.FunctionalTests.Runners
{
    partial class TestRunner : BaseRunner
    {
        protected string SharpDetectConsole { get; private set; }
        protected string SharpDetectPlugins { get; private set; }
        protected Executor Executor { get; private set; } = new Executor();
        protected Instrumentor Instrumentor { get; private set; } = new Instrumentor();
        protected Publisher Publisher { get; private set; } = new Publisher();

        public TestRunner()
        {
            // Set path to console
            SharpDetectConsole = Path.Combine("..", "..", "..", "..", "..",
                "SharpDetect.Console", "bin", BuildConfiguration, "netcoreapp2.1",
                "SharpDetect.Console.dll");
            // Set path to plugins
            SharpDetectPlugins = Path.Combine("..", "..", "..", "..", "..", "..",
                "SharpDetect.Plugins", "bin", BuildConfiguration, "netstandard2.0");
        }

        public void BuildTest(string sourcePath, string configPath)
            => Publisher.Publish(sourcePath, configPath);

        public void InstrumentTest(BaseChecker checker, SignatureResolver resolver)
            => Instrumentor.Instrument(checker, resolver, SharpDetectConsole);

        public void ExecuteTest()
            => Executor.PerformAnalysisTest(SharpDetectConsole, SharpDetectPlugins);
    }
}
