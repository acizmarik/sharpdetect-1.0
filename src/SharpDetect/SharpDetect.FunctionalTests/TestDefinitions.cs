/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.FunctionalTests.Checkers;
using SharpDetect.FunctionalTests.Runners;
using SharpDetect.Core.Utilities;
using NUnit;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SharpDetect.FunctionalTests
{
    class TestDefinitions
    {
        public const string UniversalConfig = "./UniversalTest.json";

        public SignatureResolver Resolver { get; private set; }
        public TestRunner Runner { get; private set; }

        [SetUp]
        public void Setup()
        {
            Resolver = new SignatureResolver();
            Runner = new TestRunner();
        }

        private void ExecuteTest(string testSrc, string testConfig)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var checkerName = $"{Path.GetFileNameWithoutExtension(testSrc)}Checker";
            var checker = (assembly.DefinedTypes.FirstOrDefault(t => t.IsSubclassOf(typeof(BaseChecker)) && t.Name == checkerName));
            var checkerInstance = (BaseChecker)null;
            if (checker == null)
                System.Diagnostics.Debug.WriteLine($"Could not find checker for {testSrc}");
            else
                checkerInstance = (BaseChecker)Activator.CreateInstance(checker, new object[] { Resolver });

            Assert.DoesNotThrow(() => Runner.BuildTest(testSrc, testConfig));
            Assert.DoesNotThrow(() => Runner.InstrumentTest(checkerInstance, Resolver));
            Assert.DoesNotThrow(() => Runner.ExecuteTest());
        }

        [TestCase("Tests/Blocks/HandlerBlock.cs", UniversalConfig, TestName = "HandlerBlock",
            Description = "Testing injection around try/catch/finally does not break anything")]

        [TestCase("Tests/Blocks/IfBlock.cs", UniversalConfig, TestName = "IfBlock",
            Description = "Testing injection around if/else block does not break anything")]

        [TestCase("Tests/Methods/ReferenceCalls.cs", UniversalConfig, TestName = "ReferenceCalls",
            Description = "Testing various CALL instructions on reference types")]

        [TestCase("Tests/Methods/ValueTypeCalls.cs", UniversalConfig, TestName = "ValueTypeCalls",
            Description = "Testing various CALL instructions on value types")]

        [TestCase("Tests/Methods/ContrainedCalls.cs", UniversalConfig, TestName = "ConstrainedCalls",
            Description = "Testing CALLVIRT constrained to a specific types")]

        [TestCase("Tests/Fields/FieldRead.cs", UniversalConfig, TestName = "FieldRead",
            Description = "Testing LDFLD/LDSFLD/.Volatile instructions for various field types")]

        [TestCase("Tests/Fields/FieldWrite.cs", UniversalConfig, TestName = "FieldWrite",
            Description = "Testing STFLD/STSFLD/.Volatile instructions for various field types")]

        [TestCase("Tests/Arrays/Arrays.cs", UniversalConfig, TestName = "Arrays",
            Description = "Testing NEWARR/LDELEM/STELEM instructions")]
        
        [TestCase("Tests/Arrays/ReadonlyAddressOfElement.cs", UniversalConfig, TestName = "ReadonlyAddressOfElement",
            Description = "Testing getting readonly managed pointer from array element")]

        [TestCase("Tests/Threading/LockKeyword.cs", UniversalConfig, TestName = "LockKeyword",
            Description = "Testing lock keyword injection as lock events")]

        [TestCase("Tests/Threading/MonitorEnterExit.cs", UniversalConfig, TestName = "LockExplicit",
            Description = "Testing explicit locking using Monitor::Enter, Monitor::Exit injection")]

        [TestCase("Tests/Threading/MonitorWaitPulse.cs", UniversalConfig, TestName = "WaitPulse",
            Description = "Testing injection of Monitor::Wait, Monitor::Pulse")]

        [TestCase("Tests/Methods/NestedCall.cs", UniversalConfig, TestName = "NestedCalls",
            Description = "Testing injector correctly follows call graph")]

        [TestCase("Tests/NestedClasses.cs", UniversalConfig, TestName = "NestedClasses",
            Description = "Testing injector correctly follows call graph")]

        [TestCase("Tests/Threading/TaskParallelLibrary.cs", UniversalConfig, TestName = "TaskParallelLibrary",
            Description = "Testing TPL does not crash")]

        [TestCase("Tests/Threading/Tasks1.cs", UniversalConfig, TestName = "Tasks",
            Description = "Testing tasks are correctly injected")]

        [TestCase("Tests/Threading/Threads1.cs", UniversalConfig, TestName = "Thread1",
            Description = "Testing threads are correctly injected")]

        [TestCase("Tests/Threading/Threads2.cs", UniversalConfig, TestName = "Thread2",
            Description = "Testing threads are correctly injected")]

        public void GenericTest(string testSrc, string testConfig)
            => ExecuteTest(testSrc, testConfig);
    }
}
