/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SharpDetect.FunctionalTests.Tests.Arrays
{
    class ArrayRead
    {
        public static void Main(string[] args)
        {
            var refArray = new object[1];
            var refGenArray = new Tuple<object, object>[1];
            var valArray = new int[1];
            var valGenArray = new (int, object)[1];
            
            // Write test
            refArray[0] = "Hello world";
            refGenArray[0] = new Tuple<object, object>("Hello", " world");
            valArray[0] = 11;
            valGenArray[0] = (11, "Hello world");

            // Read test
            var refArrayRead = refArray[0];
            var refGenArrayRead = refGenArray[0];
            var valArrayRead = valArray[0];
            var valGenArrayRead = valGenArray[0];
        }
    }
}
