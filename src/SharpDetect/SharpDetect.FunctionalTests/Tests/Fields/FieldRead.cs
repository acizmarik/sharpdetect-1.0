/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpDetect.FunctionalTests.Tests
{
    struct Struct
    {
        public int ValField;
        public object RefField;

        public Struct(int val, object obj)
        {
            ValField = val;
            RefField = obj;
        }
    }

    class GenericClass<T>
    {
        public Tuple<T, T> Tuple;
        public KeyValuePair<T, T> KeyValuePair;

        public GenericClass(T val1, T val2)
        {
            Tuple = new Tuple<T, T>(val1, val2);
            KeyValuePair = new KeyValuePair<T, T>(val1, val2);
        }
    }

    class FieldRead
    {
        internal static object StaticRefField = new object();
        internal static Tuple<object, object> StaticGenRefField = new Tuple<object, object>(11, "Hello");
        internal static int StaticValField = 11;
        internal static KeyValuePair<int, int> StaticGenValField = new KeyValuePair<int, int>(11, 11);
        internal static volatile int StaticVolatileValField = 11;
        internal static volatile object StaticVolatileRefField = new object();

        internal object InstanceRefField = new object();
        internal Tuple<object, object> InstanceGenRefField = new Tuple<object, object>(11, "Hello");
        internal int InstanceValField = 11;
        internal KeyValuePair<int, int> InstanceGenValField = new KeyValuePair<int, int>(11, 11);
        internal volatile int InstanceVolatileValField = 11;
        internal volatile object InstanceVolatileRefField = new object();

        public static void Main(string[] args)
        {
            var readRefStatic = StaticRefField;
            var readGenRefStatic = StaticGenRefField;
            var readValStatic = StaticValField;
            var readGenValStatic = StaticGenValField;
            var readVolatileValStatic = StaticVolatileValField;
            var readVolatileRefStatic = StaticVolatileRefField;

            var refInstance = new FieldRead();
            var readRefInstance = refInstance.InstanceRefField;
            var readGenRefInstance = refInstance.InstanceGenRefField;
            var readValInstance = refInstance.InstanceValField;
            var readGenValInstance = refInstance.InstanceGenValField;
            var readVolatileValStaticInstance = refInstance.InstanceVolatileValField;
            var readVolatileRefStaticInstance = refInstance.InstanceVolatileRefField;

            var valInstance = new Struct();
            var readValValInstance = valInstance.ValField;
            var readRefValInstance = valInstance.RefField;

            var genericRefTest1 = new GenericClass<int>(1, 11);
            var genericRefTest2 = new GenericClass<object>(new object(), "Hello world");
        }
    }
}
