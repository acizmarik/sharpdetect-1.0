/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpDetect.FunctionalTests.Tests
{
    struct Struct
    {
        public int ValField;
        public object RefField;

        public Struct(int val, object obj)
        {
            ValField = val;
            RefField = obj;
        }
    }

    class Class
    {
        public Struct Struct;
    }

    class FieldWrite
    {
        internal static object StaticRefField = new object();
        internal static Tuple<object, object> StaticGenRefField = new Tuple<object, object>(11, "Hello");
        internal static int StaticValField = 11;
        internal static KeyValuePair<int, int> StaticGenValField = new KeyValuePair<int, int>(11, 11);
        internal static volatile int StaticVolatileValField = 11;
        internal static volatile object StaticVolatileRefField = new object();

        internal object InstanceRefField = new object();
        internal Tuple<object, object> InstanceGenRefField = new Tuple<object, object>(11, "Hello");
        internal int InstanceValField = 11;
        internal KeyValuePair<int, int> InstanceGenValField = new KeyValuePair<int, int>(11, 11);
        internal volatile int InstanceVolatileValField = 11;
        internal volatile object InstanceVolatileRefField = new object();

        public static void Main(string[] args)
        {
            StaticRefField = "Hello";
            StaticGenRefField = new Tuple<object, object>(11, "Hello world");
            StaticValField = 11;
            StaticGenValField = new KeyValuePair<int, int>(11, 11);
            StaticVolatileValField = 11;
            StaticVolatileRefField = new object();

            var refInstance1 = new FieldWrite();
            refInstance1.InstanceRefField = "World";
            refInstance1.InstanceGenRefField = new Tuple<object, object>(11, "Hello world");
            refInstance1.InstanceValField = 11;
            refInstance1.InstanceGenValField = new KeyValuePair<int, int>(11, 11);
            refInstance1.InstanceVolatileValField = 11;
            refInstance1.InstanceVolatileRefField = new object();

            var valInstance = new Struct();
            valInstance.ValField = 11;
            valInstance.RefField = "Hello world";

            var refInstance2 = new Class();
            refInstance2.Struct.ValField = 11;
        }
    }
}
