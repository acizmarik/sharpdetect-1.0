/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.FunctionalTests.Tests.Methods
{
    class NestedCall
    {
        internal static object lockObj = new object();
        internal static bool flag = false;

        public static void MethodA()
            => MethodB();

        public static void MethodB()
            => MethodC();

        public static void MethodC()
        {
            lock (lockObj)
                flag = true;
        }

        public static void Main(string[] args)
        {
            MethodA();
        }
    }
}
