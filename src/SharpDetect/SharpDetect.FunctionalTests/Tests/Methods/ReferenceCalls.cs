/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.FunctionalTests.Tests.Methods
{
    class BetterReferenceType : ReferenceType<object, int>
    {
        public override void VirtualInstanceMethod1(int val) { }
        public override void VirtualInstanceMethod2(object val) { }
        public override void VirtualInstanceMethod3(out int val) { val = 11; }
        public override void VirtualInstanceMethod4(ref int val) { }
        public override void VirtualInstanceGenMethod1(object val) { }
        public override void VirtualInstanceGenMethod2(int val) { }
        public override void VirtualInstanceGenMethod3(out int val) { val = 11; }
        public override void VirtualInstanceGenMethod4(ref int val) { }
        public override void VirtualInstanceGenMethod5(object val1, int val2) { }
        public override void VirtualInstanceGenMethod6<SomeType>(SomeType val) { }
        public override void VirtualInstanceGenMethod7<SomeType>(object val1, SomeType val2) { }
    }

    class ReferenceType<RefType, ValType> 
        where RefType : class
        where ValType : struct
    {
        public static void StaticMethod1(int val) { }
        public static void StaticMethod2(object val) { }
        public static void StaticMethod3(out int val) { val = 1; }
        public static void StaticMethod4(ref int val) { }
        public static void StaticGenMethod1(RefType val) { }
        public static void StaticGenMethod2(ValType val) { }
        public static void StaticGenMethod3(out ValType val) { val = default(ValType); }
        public static void StaticGenMethod4(ref ValType val) { }
        public static void StaticGenMethod5(RefType val1, ValType val2) { }
        public static void StaticGenMethod6<SomeType>(SomeType val) { }
        public static void StaticGenMethod7<SomeType>(RefType val1, SomeType val2) { }

        public void InstanceMethod1(int val) { }
        public void InstanceMethod2(object val) { }
        public void InstanceMethod3(out int val) { val = 1; }
        public void InstanceMethod4(ref int val) { }
        public void InstanceGenMethod1(RefType val) { }
        public void InstanceGenMethod2(ValType val) { }
        public void InstanceGenMethod3(out ValType val) { val = default(ValType); }
        public void InstanceGenMethod4(ref ValType val) { }
        public void InstanceGenMethod5(RefType val1, ValType val2) { }
        public void InstanceGenMethod6<SomeType>(SomeType val) { }
        public void InstanceGenMethod7<SomeType>(RefType val1, SomeType val2) { }

        public virtual void VirtualInstanceMethod1(int val) { }
        public virtual void VirtualInstanceMethod2(object val) { }
        public virtual void VirtualInstanceMethod3(out int val) { val = 1; }
        public virtual void VirtualInstanceMethod4(ref int val) { }
        public virtual void VirtualInstanceGenMethod1(RefType val) { }
        public virtual void VirtualInstanceGenMethod2(ValType val) { }
        public virtual void VirtualInstanceGenMethod3(out ValType val) { val = default(ValType); }
        public virtual void VirtualInstanceGenMethod4(ref ValType val) { }
        public virtual void VirtualInstanceGenMethod5(RefType val1, ValType val2) { }
        public virtual void VirtualInstanceGenMethod6<SomeType>(SomeType val) { }
        public virtual void VirtualInstanceGenMethod7<SomeType>(RefType val1, SomeType val2) { }
    }


    class ReferenceCalls
    {
        public static void Main(string[] args)
        {
            ReferenceType<object, int>.StaticMethod1(11);
            ReferenceType<object, int>.StaticMethod2(new object());
            ReferenceType<object, int>.StaticMethod3(out int val1);
            ReferenceType<object, int>.StaticMethod4(ref val1);
            ReferenceType<object, int>.StaticGenMethod1(new object());
            ReferenceType<object, int>.StaticGenMethod2(11);
            ReferenceType<object, int>.StaticGenMethod3(out int val2);
            ReferenceType<object, int>.StaticGenMethod4(ref val2);
            ReferenceType<object, int>.StaticGenMethod5("Hello", val2);
            ReferenceType<object, int>.StaticGenMethod6<object>("Hello");
            ReferenceType<object, int>.StaticGenMethod7<float>(new object(), 1.5f);

            var instance = new ReferenceType<object, int>();
            instance.InstanceMethod1(11);
            instance.InstanceMethod2(new object());
            instance.InstanceMethod3(out int val3);
            instance.InstanceMethod4(ref val3);
            instance.InstanceGenMethod1(new object());
            instance.InstanceGenMethod2(val3);
            instance.InstanceGenMethod3(out int val4);
            instance.InstanceGenMethod4(ref val4);
            instance.InstanceGenMethod5("Hello", val4);
            instance.InstanceGenMethod6<object>("Hello");
            instance.InstanceGenMethod7<float>(new object(), 1.5f);

            instance = new BetterReferenceType();
            instance.VirtualInstanceMethod1(11);
            instance.VirtualInstanceMethod2(new object());
            instance.VirtualInstanceMethod3(out int val5);
            instance.VirtualInstanceMethod4(ref val5);
            instance.VirtualInstanceGenMethod1(new object());
            instance.VirtualInstanceGenMethod2(val5);
            instance.VirtualInstanceGenMethod3(out int val6);
            instance.VirtualInstanceGenMethod4(ref val6);
            instance.VirtualInstanceGenMethod5("Hello", val6);
            instance.VirtualInstanceGenMethod6<object>("Hello");
            instance.VirtualInstanceGenMethod7<float>(new object(), 1.5f);
        }
    }
}
