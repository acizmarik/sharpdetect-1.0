/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.FunctionalTests.Tests.Methods
{
    struct NonGenericValueType
    {
        public static void StaticMethod1(int val) { }
        public static void StaticMethod2(object val) { }
        public static void StaticMethod3(out int val) { val = 1; }
        public static void StaticMethod4(ref int val) { }
        public static void StaticGenMethod<SomeType>(SomeType val) { }

        public void InstanceMethod1(int val) { }
        public void InstanceMethod2(object val) { }
        public void InstanceMethod3(out int val) { val = 1; }
        public void InstanceMethod4(ref int val) { }
        public void InstanceGenMethod<SomeType>(SomeType val) { }
    }

    struct GenericValueType<RefType, ValType>
        where RefType : class
        where ValType : struct
    {
        public static void StaticMethod1(int val) { }
        public static void StaticMethod2(object val) { }
        public static void StaticMethod3(out int val) { val = 1; }
        public static void StaticMethod4(ref int val) { }
        public static void StaticGenMethod1(RefType val) { }
        public static void StaticGenMethod2(ValType val) { }
        public static void StaticGenMethod3(out ValType val) { val = default(ValType); }
        public static void StaticGenMethod4(ref ValType val) { }
        public static void StaticGenMethod5(RefType val1, ValType val2) { }
        public static void StaticGenMethod6<SomeType>(SomeType val) { }
        public static void StaticGenMethod7<SomeType>(RefType val1, SomeType val2) { }

        public void InstanceMethod1(int val) { }
        public void InstanceMethod2(object val) { }
        public void InstanceMethod3(out int val) { val = 1; }
        public void InstanceMethod4(ref int val) { }
        public void InstanceGenMethod1(RefType val) { }
        public void InstanceGenMethod2(ValType val) { }
        public void InstanceGenMethod3(out ValType val) { val = default(ValType); }
        public void InstanceGenMethod4(ref ValType val) { }
        public void InstanceGenMethod5(RefType val1, ValType val2) { }
        public void InstanceGenMethod6<SomeType>(SomeType val) { }
        public void InstanceGenMethod7<SomeType>(RefType val1, SomeType val2) { }
    }

    interface ISomeInterface
    {
        void Method();
    }

    struct InterfacedValueType : ISomeInterface
    {
        public void Method() { }
    }

    class ValueTypeCalls
    {
        public static void Main(string[] args)
        {
            GenericValueType<object, int>.StaticMethod1(11);
            GenericValueType<object, int>.StaticMethod2(new object());
            GenericValueType<object, int>.StaticMethod3(out int val1);
            GenericValueType<object, int>.StaticMethod4(ref val1);
            GenericValueType<object, int>.StaticGenMethod1(new object());
            GenericValueType<object, int>.StaticGenMethod2(11);
            GenericValueType<object, int>.StaticGenMethod3(out int val2);
            GenericValueType<object, int>.StaticGenMethod4(ref val2);
            GenericValueType<object, int>.StaticGenMethod5("Hello", val2);
            GenericValueType<object, int>.StaticGenMethod6<object>("Hello");
            GenericValueType<object, int>.StaticGenMethod7<float>(new object(), 1.5f);

            var genericInstance = new GenericValueType<object, int>();
            genericInstance.InstanceMethod1(11);
            genericInstance.InstanceMethod2(new object());
            genericInstance.InstanceMethod3(out int val3);
            genericInstance.InstanceMethod4(ref val3);
            genericInstance.InstanceGenMethod1(new object());
            genericInstance.InstanceGenMethod2(val3);
            genericInstance.InstanceGenMethod3(out int val4);
            genericInstance.InstanceGenMethod4(ref val4);
            genericInstance.InstanceGenMethod5("Hello", val4);
            genericInstance.InstanceGenMethod6<object>("Hello");
            genericInstance.InstanceGenMethod7<float>(new object(), 1.5f);

            NonGenericValueType.StaticMethod1(11);
            NonGenericValueType.StaticMethod2(new object());
            NonGenericValueType.StaticMethod3(out int val5);
            NonGenericValueType.StaticMethod4(ref val5);
            NonGenericValueType.StaticGenMethod<float>(1.5f);

            var nongenericInstance = new NonGenericValueType();
            nongenericInstance.InstanceMethod1(11);
            nongenericInstance.InstanceMethod2(new object());
            nongenericInstance.InstanceMethod3(out int val6);
            nongenericInstance.InstanceMethod4(ref val6);
            nongenericInstance.InstanceGenMethod<float>(1.5f);

            var interfacedValueType = new InterfacedValueType();
            interfacedValueType.Method();
            ((ISomeInterface)interfacedValueType).Method();

            var list = new List<String>();
            var enumerator = list.GetEnumerator();
            while (enumerator.MoveNext())
            {

            }
        }
    }
}
