/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SharpDetect.FunctionalTests.Tests.Threading
{
    class WaitPulse
    {
        static Random rnd = new Random();
        static Queue<int> items = new Queue<int>();
        static bool empty = true;

        public static void Produce()
        {
            Task.Delay(rnd.Next(10)).Wait();
            var newItem = rnd.Next(10);
            lock (items)
            {
                items.Enqueue(newItem);
                if (empty)
                {
                    empty = false;
                    Monitor.Pulse(items);
                }
            }
        }

        public static void Consume()
        {
            int toConsume = 0;
            lock (items)
            {
                while (empty)
                    Monitor.Wait(items);
                toConsume = items.Dequeue();
                if (items.Count == 0)
                    empty = true;
            }
            Task.Delay(rnd.Next(100)).Wait();
        }

        public static void Main(string[] args)
        {
            const int itemsCount = 10;
            var thread1 = new Thread(() =>
            {
                for (var i = 0; i < itemsCount; ++i)
                    Produce();
            });
            var thread2 = new Thread(() =>
            {
                for (var i = 0; i < itemsCount; ++i)
                    Consume();
            });
            thread1.Start();
            thread2.Start();

            thread1.Join();
            thread2.Join();
        }
    }
}
