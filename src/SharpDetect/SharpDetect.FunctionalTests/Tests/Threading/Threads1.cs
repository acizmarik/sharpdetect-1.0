/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SharpDetect.FunctionalTests.Tests.Threading
{
    class Threads1
    {
        static object lockObj = new object();
        static int counter = 0;

        public static void Method()
        {
            for (var i = 0; i < 100; ++i)
                lock (lockObj)
                    ++counter;
        }

        public static void Main(string[] args)
        {
            // Method is not directly called
            var thread1 = new Thread(Method);
            var thread2 = new Thread(Method);
            thread1.Start();
            thread2.Start();

            thread1.Join();
            thread2.Join();
        }
    }
}
