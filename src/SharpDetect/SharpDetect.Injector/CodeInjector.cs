/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Common;
using SharpDetect.Injector.Injectors;
using SharpDetect.Injector.PatternRecognizers;
using SharpDetect.Injector.PatternRecognizers.Patterns;
using SharpDetect.Injector.Runtime;
using SharpDetect.Injector.Utilities;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using SharpDetect.Core;

namespace SharpDetect.Injector
{
    public class CodeInjector
    {
        internal static readonly string InjectionLogMessageTemplate = String.Concat(
            "[{Event}] {Instruction}",
            Environment.NewLine,
            "\t\tat {Method}");
        internal static readonly string InjectionExtensionLogMessageTemplate =
            "[{Event}] applied as extension to the last event";

        protected InjectionContext Context;
        protected HashSet<MethodDef> ProcessedMethods;
        protected HashSet<AssemblyDef> DirtyAssemblies;
        private Dictionary<AnalysisEventType, BaseInjector> injectors;
        private BaseRecognizer[] codePatternRecognizers;
        private CodeInspector codeInspector;
        private InjectedRuntime runtime;
        private PackedParametersCache parametersCache;  


        public CodeInjector(IParametersInjector parameters)
        {
            Context = new InjectionContext();
            DirtyAssemblies = new HashSet<AssemblyDef>();
            ProcessedMethods = new HashSet<MethodDef>();
            parametersCache = new PackedParametersCache();
            codeInspector = new CodeInspector(parameters, Context);

            // Prepare injected runtime
            runtime = new InjectedRuntime(codeInspector);
            DirtyAssemblies.Add(codeInspector.CoreLibrary);
           
            // Initialize injectors
            injectors = new Dictionary<AnalysisEventType, BaseInjector>()
            {
                { AnalysisEventType.AnalysisStart, new AnalysisStartInjector(Context) },
                { AnalysisEventType.AnalysisEnd, new AnalysisEndInjector(Context) },

                { AnalysisEventType.ArrayCreate, new ArrayCreatedInjector(Context) },
                { AnalysisEventType.ArrayRead, new ArrayReadInjector(Context) },
                { AnalysisEventType.ArrayWrite, new ArrayWriteInjector(Context) },

                { AnalysisEventType.ClassCreate, new ClassConstructedInjector(Context) },

                { AnalysisEventType.FieldRead, new FieldReadInjector(Context) },
                { AnalysisEventType.FieldWrite, new FieldWriteInjector(Context) },

                { AnalysisEventType.MethodCall, new MethodCallInjector(Context, parametersCache) },
                { AnalysisEventType.MethodReturn, new MethodReturnInjector(Context, parametersCache) },

                { AnalysisEventType.ObjectCreate, new ObjectCreateInjector(Context) }
            };

            // Initialize code pattern recognizers
            codePatternRecognizers = new BaseRecognizer[]
            {
                new ArrayRecognizer(Context, 
                    injectors[AnalysisEventType.ArrayCreate],
                    injectors[AnalysisEventType.ArrayRead],
                    injectors[AnalysisEventType.ArrayWrite]),
                new ClassConstructRecognizer(Context,
                    injectors[AnalysisEventType.ClassCreate]),
                new FieldRecognizer(Context, 
                    injectors[AnalysisEventType.FieldRead], 
                    injectors[AnalysisEventType.FieldWrite]),
                new CallsRecognizer(Context,
                    injectors[AnalysisEventType.MethodCall],
                    injectors[AnalysisEventType.MethodReturn]),
                new NewobjRecognizer(Context,
                    injectors[AnalysisEventType.ObjectCreate]),
            };
        }

        public void Inject()
        {
            var entryPoint = codeInspector.MainAssembly.ManifestModule.EntryPoint;
            if (entryPoint == null)
            {
                Log.Error("Could not find entrypoint in assembly {assembly}", codeInspector.MainAssembly);
                throw new DynamicAnalysisException("Could not find entrypoint in target assembly");
            }

            foreach (var (called, caller) in codeInspector.CallGraph)
            {
                if (!codeInspector.IsMatch(called))
                {
                    Log.Debug("Searching method {called} [NOT MATCHED], called from method {caller}", called, caller);
                    Inject(called, matched: false);
                }
                else
                {
                    Log.Debug("Searching method {called} [MATCH], called from method {caller}", called, caller);
                    Inject(called, matched: true);
                }
            }

            injectors[AnalysisEventType.AnalysisStart].Inject(entryPoint, entryPoint.Body.Instructions.First(), null);
            injectors[AnalysisEventType.AnalysisEnd].Inject(entryPoint, entryPoint.Body.Instructions.Last(), null);
            if (ProcessedMethods.Contains(entryPoint))
            {
                // If entrypoint was not injected make sure to increase stack based on analysis begin and end
                entryPoint.Body.MaxStack += Math.Max(injectors[AnalysisEventType.AnalysisStart].IncreasesMaxStackBy,
                    injectors[AnalysisEventType.AnalysisEnd].IncreasesMaxStackBy);
            }

            DirtyAssemblies.Add(codeInspector.MainAssembly);
        }

        internal void Inject(MethodDef method, bool matched = true)
        {
            // Make sure we do not inject the same method twice
            if (ProcessedMethods.Contains(method) || method.Module.Name.StartsWith("System"))
                return;

            // First store information about injections
            // Actual code injection will be performed after method scan is complete
            var toInject = new List<(BasePattern, IEnumerable<BaseInjector>, Instruction, int)>();

            // Scan entire method
            for (var instructionIndex = 0; instructionIndex < method.Body.Instructions.Count; ++instructionIndex)
            {
                var instruction = method.Body.Instructions[instructionIndex];

                // Check each instruction against all code recognizers
                foreach (var recognizer in codePatternRecognizers)
                {
                    var result = (matched) ? 
                        recognizer.CanRecognize(method, instruction, codeInspector.InjectingParameters) : 
                        recognizer.IsMatch(method, instruction, codeInspector);
                    if (!result)
                        continue;

                    var (pattern, injectors) = recognizer.Recognize(method, instruction, instructionIndex);
                    toInject.Add((pattern, injectors, instruction, instructionIndex));
                }
            }

            if (toInject.Count != 0)
            {
                // Mark assembly as dirty if changed
                if (!DirtyAssemblies.Contains(method.Module.Assembly))
                    DirtyAssemblies.Add(method.Module.Assembly);

                // Inject found events and change maximum stack depth
                var maxStackChange = (ushort)0;
                var addsVariables = false;
                foreach (var (pattern, injectors, instruction, instructionIndex) in toInject)
                {
                    foreach (var injector in injectors)
                    {
                        maxStackChange = Math.Max(maxStackChange, injector.IncreasesMaxStackBy);
                        addsVariables |= injector.AddsVariables;
                        Log.Information(InjectionLogMessageTemplate, injector.Name, instruction, method);
                        injector.Inject(method, instruction, pattern);
                    }
                }
                method.Body.MaxStack += maxStackChange;
                method.Body.InitLocals = true;
            }

            // Add to processed methods
            ProcessedMethods.Add(method);
        }

        public List<string> Store(string path)
        {
            var modified = new List<string>();
            foreach (var assembly in DirtyAssemblies)
            {
                Log.Information("Writing assembly {assembly}", assembly);
                var assemblyPath = Path.Combine(path, Path.GetFileName(assembly.ManifestModule.FullName));
                assembly.WriteAssembly(assemblyPath);
                modified.Add(assemblyPath);
            }

            return modified;
        }
    }
}
