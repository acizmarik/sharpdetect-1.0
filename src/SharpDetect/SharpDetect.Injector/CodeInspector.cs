/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using dnlib.DotNet;
using dnlib.DotNet.Emit;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using Serilog;

namespace SharpDetect.Injector
{
    internal class CodeInspector
    {
        public IParametersInjector InjectingParameters { get; private set; }
        public InjectionContext Context { get; private set; }
        public AssemblyDef MainAssembly { get; private set; }
        public AssemblyDef CoreLibrary { get; private set; }
        protected Dictionary<TypeDef, HashSet<MethodDef>> VirtualCalls { get; set; }

        public CodeInspector(IParametersInjector parameters, InjectionContext context)
        {           
            try
            {
                this.MainAssembly = AssemblyDef.Load(parameters.TargetAssembly);
            }
            catch (BadImageFormatException e)
            {
                Log.Fatal("Could not load assembly due to and exception {@Exception}", e);
                throw;
            }

            // Make sure the main assembly contains entrypoint
            if (MainAssembly.ManifestModule.EntryPoint == null)
            {
                var message = String.Concat(
                    "Manifest module of input assembly {MainAssembly} does not contain entrypoint.",
                    "Expected an executable assembly with properly defined entrypoint method.");
                Log.Fatal(message, MainAssembly);
                throw new ArgumentException("Provided assembly is not executable");
            }

            this.InjectingParameters = parameters;
            this.Context = context;
            this.CoreLibrary = GetCoreLibrary();
            this.Context.CoreLib = CoreLibrary;

            // Make sure the framework is supported
            if (MainAssembly.TryGetOriginalTargetFrameworkAttribute(out string framework, out Version version, out string _))
            {
                Log.Information("Assembly {Assembly} targets framework {Framework}, version {Version}", MainAssembly, framework, version);
                if (framework.ToUpperInvariant() != ".NETCOREAPP")
                {
                    Log.Fatal("Framework {Framework} is not supported", framework);
                    throw new ArgumentException("Target framework is not supported");
                }
            }

            this.VirtualCalls = new Dictionary<TypeDef, HashSet<MethodDef>>();
        }

        private IEnumerable<TypeDef> GetDescendants(TypeDef type)
        {
            // Sealed types can not have any descendants
            if (!type.Attributes.HasFlag(TypeAttributes.Sealed))
            {
                // Check current assembly
                foreach (var descendant in type.Module.Types.Where(t => t.BaseType == type))
                    yield return descendant;

            }

            yield break;
        }

        public IEnumerable<(MethodDef, MethodDef)> CallGraph
        {
            get
            {
                var visitedAssemblies = new HashSet<AssemblyDef>();
                var processedFieldTypes = new HashSet<TypeDef>();
                var processedMethodDefs = new HashSet<MethodDef>();
                var toSearch = new Queue<(MethodDef, Instruction, MethodDef)>();

                void SearchMethodsOfType(TypeDef type)
                {
                    if (processedFieldTypes.Contains(type) || type == null)
                        return;

                    if (type.HasMethods)
                    {
                        foreach (var method in type.Methods.Where((m) => !processedMethodDefs.Contains(m)))
                        {
                            toSearch.Enqueue((method, null, null));
                            processedMethodDefs.Add(method);
                        }
                    }
                    processedFieldTypes.Add(type);
                }

                var entryPoint = MainAssembly.ManifestModule.EntryPoint;

                visitedAssemblies.Add(MainAssembly);
                Log.Debug("Checking assembly {assembly}", MainAssembly);
                toSearch.Enqueue((entryPoint, null, null));

                // Traverse whole call graph
                while (toSearch.Count > 0)
                {
                    var (currentMethod, callInstruction, callerMethod) = toSearch.Dequeue();

                    // Check if we already encountered this assembly
                    if (!visitedAssemblies.Contains(currentMethod.Module.Assembly))
                    {
                        visitedAssemblies.Add(currentMethod.Module.Assembly);
                        Log.Debug("Checking assembly {assembly}", currentMethod.Module.Assembly);
                    }

                    // Skip external methods
                    if (currentMethod.Body == null)
                        continue;
                    // Skip CLR's internal methods
                    if (currentMethod.IsInternalCall)
                        continue;

                    // Find called / otherwise referenced types and methods
                    foreach (var instruction in currentMethod.Body.Instructions)
                    {
                        if (instruction.OpCode.FlowControl == FlowControl.Call || instruction.OpCode == OpCodes.Ldftn)
                        {
                            var calledMethod = Context.ResolveMethod((IMethod)instruction.Operand);
                            if (calledMethod.IsConstructor)
                            {
                                var constructedType = calledMethod.DeclaringType;
                                SearchMethodsOfType(constructedType);
                            }
                                   
                            // Check if we already processed this method
                            if (processedMethodDefs.Contains(calledMethod))
                                continue;
                            processedMethodDefs.Add(calledMethod);

                            // Push new call record for searching
                            toSearch.Enqueue((calledMethod, instruction, currentMethod));
                        }
                        else if (instruction.OpCode == OpCodes.Ldloca_S || instruction.OpCode == OpCodes.Ldloca)
                        {
                            var variableTypeSig = ((Local)instruction.Operand).Type;
                            // If it is generic type then we skip
                            if (variableTypeSig.ContainsGenericParameter)
                                continue;

                            var variableType = Context.ResolveType(variableTypeSig.ToTypeDefOrRef());
                            
                            SearchMethodsOfType(variableType);
                        }
                    }
                    yield return (currentMethod, callerMethod);

                    // Check if declaring type of the method has cctor
                    var declaringType = currentMethod.DeclaringType;
                    var cctor = declaringType.Methods.FirstOrDefault(m => m.IsStaticConstructor);
                    // Class constructors are called by CLR
                    if (cctor != null && !processedMethodDefs.Contains(cctor))
                    {
                        toSearch.Enqueue((cctor, null, null));
                        processedMethodDefs.Add(cctor);
                    }
                }
            }
        }

        private AssemblyDef GetCoreLibrary()
        {
            try
            {
                var systemRuntime = MainAssembly.ManifestModule.GetAssemblyRefs().Single(r => r.Name == "System.Runtime");
                var loadedSystemRuntime = Context.AssemblyResolver.Resolve(systemRuntime, MainAssembly.ManifestModule);

                var corelib = loadedSystemRuntime.ManifestModule.GetAssemblyRefs().Single(r => r.Name == "System.Private.CoreLib");
                var loadedCorelib = Context.AssemblyResolver.Resolve(corelib, loadedSystemRuntime.ManifestModule);

                return loadedCorelib;
            }
            catch (InvalidOperationException)
            {
                var message = string.Concat(
                    "Could not find BCL between {Assembly} references.",
                    Environment.NewLine,
                    "Make sure this is a valid .NETCoreApp");
                Log.Fatal(message);
                throw new InvalidOperationException("Could not load BCL");
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool IsMatch(IMethod method)
        {
            var path = $"{method.FullName}".Split(' ').ElementAtOrDefault(1);
            return CheckMatch(path, InjectingParameters.AlwaysIncludeMethodPatterns, InjectingParameters.MethodPatterns);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool IsMatch(IField field)
        {
            var path = $"{field.FullName}".Split(' ').LastOrDefault();
            return CheckMatch(path, InjectingParameters.AlwaysIncludeFieldPatterns, InjectingParameters.FieldPatterns);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool IsMatch(ITypeDefOrRef type)
        {
            var path = type.FullName;
            return CheckMatch(path, InjectingParameters.AlwaysIncludeMethodPatterns, InjectingParameters.MethodPatterns);
        }

        private bool CheckMatch(string path, IEnumerable<string> alwaysPositive, IEnumerable<string> positivePatterns)
        {
            if (alwaysPositive != null)
            {
                foreach (var pattern in alwaysPositive)
                {
                    if (path.StartsWith(pattern))
                        return true;
                }
            }

            if (positivePatterns != null)
            {
                foreach (var pattern in positivePatterns)
                {
                    if (path.StartsWith(pattern))
                        return true;
                }
            }

            return false;
        }
    }
}
