/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpDetect.Injector
{
    public interface IParametersInjector
    {
        string TargetAssembly { get; }

        List<string> AlwaysIncludeMethodPatterns { get; }
        List<string> AlwaysIncludeFieldPatterns { get; }
        List<string> FieldPatterns { get; }
        List<string> MethodPatterns { get; }

        bool ArrayInjectors { get; }
        bool FieldInjectors { get; }
        bool MethodInjectors { get; }
        bool ObjectCreateInjector { get; }
        bool ClassCreateInjector { get; }
    }
}
