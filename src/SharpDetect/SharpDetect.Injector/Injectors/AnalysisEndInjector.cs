/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Common;
using SharpDetect.Injector.PatternRecognizers.Patterns;
using SharpDetect.Injector.Utilities;

namespace SharpDetect.Injector.Injectors
{
    class AnalysisEndInjector : BaseInjector
    {
        public override ushort IncreasesMaxStackBy { get; } = 1;
        public override bool AddsVariables { get; } = false;

        public AnalysisEndInjector(InjectionContext context)
            : base(context, AnalysisEventType.AnalysisEnd)
        {
            EventRaiserMethod = Definitions.AnalysisEnd.RaiserMethod;
        }

        public override void Inject(MethodDef method, Instruction instruction, BasePattern _)
        {
            // Check that we are injecting before RET and not end of cycle for example
            if (instruction.OpCode != OpCodes.Ret)
            {
                // This should not break any handlers or jumps
                instruction = Instruction.Create(OpCodes.Ret);
                method.Body.Instructions.Add(instruction);
            }

            method.InjectBefore(instruction, new List<Instruction>
            {
                // Call event dispatcher
                CreateEventDispatcherLoadInstruction(method),
                Instruction.Create(OpCodes.Ldstr, method.FullName),
                CreateEventRaiserCallInstruction(method)
            });
        }
    }
}
