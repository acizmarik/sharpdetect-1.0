/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Common;
using SharpDetect.Injector.PatternRecognizers.Patterns;
using SharpDetect.Injector.Utilities;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpDetect.Injector.Injectors
{
    class AnalysisStartInjector : BaseInjector
    {
        public override ushort IncreasesMaxStackBy { get; } = 1;
        public override bool AddsVariables { get; } = false;

        public AnalysisStartInjector(InjectionContext context)
            : base(context, AnalysisEventType.AnalysisStart)
        {
            EventRaiserMethod = Definitions.AnalysisBegin.RaiserMethod;
        }

        public override void Inject(MethodDef method, Instruction instruction, BasePattern _)
        {
            method.InjectBefore(instruction, new List<Instruction>
            {
                // Calls singleton constructor which initializes event handlers
                CreateAnalysisRegistrationInstruction(method),
                // Initialized instance is no longer needed at this point on stack
                Instruction.Create(OpCodes.Pop),

                // Call event dispatcher
                CreateEventDispatcherLoadInstruction(method),
                Instruction.Create(OpCodes.Ldstr, method.FullName),
                CreateEventRaiserCallInstruction(method)
            });
        }
    }
}
