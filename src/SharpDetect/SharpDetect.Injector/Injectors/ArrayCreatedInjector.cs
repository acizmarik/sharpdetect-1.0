/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Common;
using SharpDetect.Injector.PatternRecognizers.Patterns;
using SharpDetect.Injector.Utilities;

namespace SharpDetect.Injector.Injectors
{
    class ArrayCreatedInjector : BaseInjector
    {
        public override ushort IncreasesMaxStackBy { get; } = 4;
        public override bool AddsVariables { get; } = true;

        public ArrayCreatedInjector(InjectionContext context)
            : base(context, AnalysisEventType.ArrayCreate)
        {
            EventInfoTypeDefinition = Definitions.ArrayCreate.ArgsType;
            EventInfoTypeSig = Definitions.ArrayCreate.ArgsType.ToTypeSig();
            EventInfoTypeCtor = Definitions.ArrayCreate.ArgsCtor;
            EventRaiserMethod = Definitions.ArrayCreate.RaiserMethod;
        }

        public override void Inject(MethodDef method, Instruction instruction, BasePattern pattern)
        {
            var variablesPool = VariableMethodPool.Instance;
            var importedArraySig = method.Module.Import(((ArrayPattern)pattern).ArrayTypeSig);
            var importedEventType = method.Module.Import(EventInfoTypeDefinition);
            var importedEventInfoSig = method.Module.Import(EventInfoTypeSig);

            var eventInfoVariable = variablesPool.GetVariable(method, importedEventInfoSig);
            var arrayVariable = variablesPool.GetVariable(method, importedArraySig);

            method.InjectAfter(instruction, new List<Instruction>()
            {
                // Store array to variable
                Instruction.Create(OpCodes.Stloc, arrayVariable),
                // Create event info struct
                Instruction.Create(OpCodes.Ldloca, eventInfoVariable),
                Instruction.Create(OpCodes.Ldloc, arrayVariable),
                Instruction.Create(OpCodes.Ldstr, method.FullName),
                CreateEventInfoCtorInstruction(method),
                // Call event dispatcher
                CreateEventDispatcherLoadInstruction(method),
                Instruction.Create(OpCodes.Ldloc, eventInfoVariable),
                CreateEventRaiserCallInstruction(method),
                // Restore original stack
                Instruction.Create(OpCodes.Ldloc, arrayVariable)
            });

            variablesPool.StoreVariable(method, arrayVariable);
            variablesPool.StoreVariable(method, eventInfoVariable);
        }
    }
}
