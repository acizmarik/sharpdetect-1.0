/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Common;
using SharpDetect.Injector.PatternRecognizers.Patterns;
using SharpDetect.Injector.Runtime;
using SharpDetect.Injector.Utilities;

namespace SharpDetect.Injector.Injectors
{
    /// <summary>
    /// Injector responsible for injecting analysis call around LDELEM_* instructions
    /// </summary>
    class ArrayReadInjector : BaseInjector
    {
        public override ushort IncreasesMaxStackBy { get; } = 4;
        public override bool AddsVariables { get; } = true;

        public ArrayReadInjector(InjectionContext context)
            : base(context, AnalysisEventType.ArrayRead)
        {
            EventInfoTypeDefinition = Definitions.ArrayRead.ArgsType;
            EventInfoTypeSig = Definitions.ArrayRead.ArgsType.ToTypeSig();
            EventInfoTypeCtor = Definitions.ArrayRead.ArgsCtor;
            EventRaiserMethod = Definitions.ArrayRead.RaiserMethod;
        }

        public override void Inject(MethodDef method, Instruction instruction, BasePattern pattern)
        {
            var variablePool = VariableMethodPool.Instance;
            var importedEventInfoSig = method.Module.Import(EventInfoTypeSig);

            var arrayTypeSig = method.Module.Import(((ArrayPattern)pattern).ArrayTypeSig);
            var indexTypeSig = method.Module.Import(((ArrayPattern)pattern).IndexTypeSig);
            var arrayVariable = variablePool.GetVariable(method, arrayTypeSig);
            var indexVariable = variablePool.GetVariable(method, indexTypeSig);
            var eventInfoVariable = variablePool.GetVariable(method, importedEventInfoSig);
          
            method.InjectBefore(instruction, new List<Instruction>
            {
                // Copy stack - array and index
                Instruction.Create(OpCodes.Stloc, indexVariable),
                Instruction.Create(OpCodes.Stloc, arrayVariable),
                // Restore original stack
                Instruction.Create(OpCodes.Ldloc, arrayVariable),
                Instruction.Create(OpCodes.Ldloc, indexVariable)
            });
            method.InjectAfter(instruction, new List<Instruction>
            {
                // Create event info struct
                Instruction.Create(OpCodes.Ldloca, eventInfoVariable),                
                Instruction.Create(OpCodes.Ldloc, arrayVariable),
                Instruction.Create(OpCodes.Ldloc, indexVariable),
                Instruction.Create(OpCodes.Ldstr, method.FullName),
                CreateEventInfoCtorInstruction(method),
                // Call event dispatcher
                CreateEventDispatcherLoadInstruction(method),
                Instruction.Create(OpCodes.Ldloc, eventInfoVariable),
                CreateEventRaiserCallInstruction(method),            
            });
            variablePool.StoreVariable(method, arrayVariable);
            variablePool.StoreVariable(method, indexVariable);
            variablePool.StoreVariable(method, eventInfoVariable);
        }
    }
}
