/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Common;
using SharpDetect.Common.Events;
using SharpDetect.Injector.PatternRecognizers.Patterns;
using SharpDetect.Injector.Utilities;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDetect.Core;

namespace SharpDetect.Injector.Injectors
{
    /// <summary>
    /// Common base class for all injectors
    /// </summary>
    abstract class BaseInjector
    {
        public string Name { get; private set; }
        public AnalysisEventType Type { get; }
        public abstract bool AddsVariables { get; }
        public abstract ushort IncreasesMaxStackBy { get; }
        public abstract void Inject(MethodDef method, Instruction instruction, BasePattern pattern);

        protected TypeSig EventInfoTypeSig { get; set; }
        protected IType EventInfoTypeDefinition { get; set; }
        protected IMethod EventInfoTypeCtor { get; set; }
        protected IMethod EventRaiserMethod { get; set; }      
        protected InjectionContext Context { get; set; }

        protected static TypeSig ParameterTypeSig { get; private set; }
        protected static IMethod EventDispatcherInstanceGetterMethod { get; private set; }
        protected static IMethod AnalysisRegistrationMethod { get; private set; }
        protected static IMethod ParameterInfoTypeCtor { get; private set; }
        private static bool prepared = false;

        public BaseInjector(InjectionContext context, AnalysisEventType type)
        {
            this.Context = context;
            this.Type = type;
            this.Name = Enum.GetName(typeof(AnalysisEventType), type);

            PrepareEventDispatcher();
        }

        private void PrepareEventDispatcher()
        {
            if (!prepared)
            {
                var eventDispatcherTypeDef = Context.InjectedTypes[(
                    Definitions.EventDispatcherTypeName,
                    Definitions.FrameworkNamespace)];
                EventDispatcherInstanceGetterMethod = eventDispatcherTypeDef.Methods.Single(m => m.Name == "get_Instance");

                var analysisModule = ModuleDefMD.Load(typeof(EventDispatcherFrontend).Module);
                var eventDispatcherFrontendTypeDef = analysisModule.Types.Single(t => t.Name == nameof(EventDispatcherFrontend));
                var eventDispatcherFrontendRegistrator = eventDispatcherFrontendTypeDef.Methods.Single(m => m.Name == "GetInstance");
                AnalysisRegistrationMethod = eventDispatcherFrontendRegistrator;

                ParameterInfoTypeCtor = Definitions.ParameterCtor;
                ParameterTypeSig = Definitions.ParameterType.ToTypeSig();

                prepared = true;
            }
        }

        protected void AddExtensionEventRaiser(MethodDef method, AnalysisEvent extension, Local parameters, List<Instruction> implementation)
        {
            Log.Information(CodeInjector.InjectionExtensionLogMessageTemplate, extension.Name);
            var importedExtensionEventRaiser = method.Module.Import(extension.RaiserMethod);
            implementation.Add(CreateEventDispatcherLoadInstruction(method));
            implementation.Add(Instruction.Create(OpCodes.Ldloc, parameters));
            implementation.Add(Instruction.Create(OpCodes.Call, importedExtensionEventRaiser));
        }

        protected Instruction CreateAnalysisRegistrationInstruction(MethodDef method)
            => MakeCallInstruction(method, AnalysisRegistrationMethod);
        protected Instruction CreateEventDispatcherLoadInstruction(MethodDef method)
            => MakeCallInstruction(method, EventDispatcherInstanceGetterMethod);
        protected Instruction CreateEventInfoCtorInstruction(MethodDef method)
        {
            method.Module.Import(EventInfoTypeDefinition);
            var importedCtor = method.Module.Import(EventInfoTypeCtor);
            return Instruction.Create(OpCodes.Call, importedCtor);
        }
        protected Instruction CreateParameterCtorInstruction(MethodDef method)
            => MakeCallInstruction(method, ParameterInfoTypeCtor);
        protected Instruction CreateEventRaiserCallInstruction(MethodDef method)
            => MakeCallInstruction(method, EventRaiserMethod);

        protected Instruction MakeCallInstruction(MethodDef caller, IMethod called)
        {
            var importedCalled = caller.Module.Import(called);
            return Instruction.Create(OpCodes.Call, importedCalled);
        } 
    }
}
