/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Common;
using SharpDetect.Injector.PatternRecognizers.Patterns;
using SharpDetect.Injector.Runtime;
using SharpDetect.Injector.Utilities;

namespace SharpDetect.Injector.Injectors
{
    /// <summary>
    /// Injector responsible for injecting cctor right before RET instruction
    /// </summary>
    class ClassConstructedInjector : BaseInjector
    {
        public override ushort IncreasesMaxStackBy { get; } = 1;
        public override bool AddsVariables { get; } = false;
        internal OpCode OpCode = OpCodes.Ret;

        public ClassConstructedInjector(InjectionContext context)
            : base(context, AnalysisEventType.ClassCreate)
        {
            EventInfoTypeDefinition = Definitions.ClassConstruct.ArgsType;
            EventInfoTypeCtor = Definitions.ClassConstruct.ArgsCtor;
            EventRaiserMethod = Definitions.ClassConstruct.RaiserMethod;
        }

        public override void Inject(MethodDef method, Instruction instruction, BasePattern pattern)
        {
            var classConstructPattern = (ClassConstructPattern)pattern;

            method.InjectBefore(instruction, new List<Instruction>()
            {
                CreateEventDispatcherLoadInstruction(method),
                Instruction.Create(OpCodes.Ldstr, classConstructPattern.FullTypename),
                CreateEventRaiserCallInstruction(method)
            });
        }
    }
}
