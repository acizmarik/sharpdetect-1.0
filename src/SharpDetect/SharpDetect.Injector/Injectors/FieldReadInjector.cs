/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Common;
using SharpDetect.Injector.PatternRecognizers.Patterns;
using SharpDetect.Injector.Runtime;
using SharpDetect.Injector.Utilities;
using Serilog;
using SharpDetect.Common.Events;

namespace SharpDetect.Injector.Injectors
{
    /// <summary>
    /// Injector responsible for injecting analysis call around LD(S)FLD* instructions
    /// </summary>
    class FieldReadInjector : BaseInjector
    {
        public override ushort IncreasesMaxStackBy { get; } = 5;
        public override bool AddsVariables { get; } = true;

        public FieldReadInjector(InjectionContext context)
            : base(context, AnalysisEventType.FieldRead)
        {
            EventInfoTypeDefinition = Definitions.FieldRead.ArgsType;
            EventInfoTypeSig = Definitions.FieldRead.ArgsType.ToTypeSig();
            EventInfoTypeCtor = Definitions.FieldRead.ArgsCtor;
            EventRaiserMethod = Definitions.FieldRead.RaiserMethod;
        }

        public override void Inject(MethodDef method, Instruction instruction, BasePattern pattern)
        {
            var fieldPattern = (FieldPattern)pattern;
            var variablePool = VariableMethodPool.Instance;
            var importedEventInfoSig = method.Module.Import(EventInfoTypeSig);
            var eventInfoVariable = variablePool.GetVariable(method, importedEventInfoSig);
            var instanceVariable = (Local)null;

            // Check if we need to pack instance
            if (!fieldPattern.FieldFlags.HasFlag(FieldFlags.Static))
            {
                // If declaring type is value type we pack as IntPtr
                if (fieldPattern.HasDeclaringValueType)
                {
                    // TODO: this is an unresolved issue
                    Log.Warning("Capturing instance information for structs when dealing with fields in not yet implemented.");
                    return;
                }
                // Otherwise provide reference to actual object
                else if (fieldPattern.HasDeclaringReferenceType)
                {
                    var importedDeclaringTypeSig = method.Module.Import(fieldPattern.DeclaringTypeSig);
                    instanceVariable = variablePool.GetVariable(method, importedDeclaringTypeSig);
                }
            }
            
            var injectBefore = GenerateInjectBeforeCode(instanceVariable);
            var injectAfter = GenerateInjectAterCode(method, fieldPattern, eventInfoVariable, instanceVariable);

            // Inject into caller
            method.InjectBefore(instruction, injectBefore);
            method.InjectAfter(instruction, injectAfter);

            // Release variables
            variablePool.StoreVariable(method, eventInfoVariable);
            if (instanceVariable != null)
                variablePool.StoreVariable(method, instanceVariable);
        }

        private List<Instruction> GenerateInjectBeforeCode(Local instance)
        {
            var instructionsList = new List<Instruction>();

            // Instance field
            if (instance != null)
            {
                instructionsList.Add(Instruction.Create(OpCodes.Dup));
                // Replicate stack
                instructionsList.Add(Instruction.Create(OpCodes.Stloc, instance));
            }

            return instructionsList;
        }

        private List<Instruction> GenerateInjectAterCode(MethodDef method, FieldPattern pattern, Local info, Local instance)
        {
            var instructionsList = new List<Instruction>();
            var importedFieldType = method.Module.Import(pattern.FieldDef);

            // Load local struct by address
            instructionsList.Add(Instruction.Create(OpCodes.Ldloca, info));
            // Try load instance
            if (instance != null)
            {
                // Load object or IntPtr based on declaring type
                instructionsList.Add(Instruction.Create(OpCodes.Ldloc, instance));
            }
            else
            {
                // Load instance to the closest reference type
                if (pattern.HasDeclaringValueType)
                {
                    // TODO: this is an unresolved issue (null is a placeholder)
                    
                }
                // Load null if declaring type is reference type
                else if (pattern.HasDeclaringReferenceType || pattern.FieldFlags.HasFlag(FieldFlags.Static))
                    instructionsList.Add(Instruction.Create(OpCodes.Ldnull));
            }
            // Load field descriptor
            instructionsList.Add(Instruction.Create(OpCodes.Ldstr, pattern.FieldDef.FullName));
            // Load field flags
            instructionsList.Add(Instruction.Create(OpCodes.Ldc_I4, (int)pattern.FieldFlags));
            // Construct event info
            instructionsList.Add(CreateEventInfoCtorInstruction(method));
            // Get event dispatcher instance
            instructionsList.Add(CreateEventDispatcherLoadInstruction(method));
            // Load event info
            instructionsList.Add(Instruction.Create(OpCodes.Ldloc, info));
            // Raise event
            instructionsList.Add(CreateEventRaiserCallInstruction(method));

            return instructionsList;
        }
    }
}
