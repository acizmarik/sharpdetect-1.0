/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Common;
using SharpDetect.Injector.PatternRecognizers.Patterns;
using SharpDetect.Injector.Runtime;
using SharpDetect.Injector.Utilities;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using SharpDetect.Common.Events;

namespace SharpDetect.Injector.Injectors
{
    /// <summary>
    /// Injector responsible for injecting analysis call around ST(S)FLD* instructions
    /// </summary>
    class FieldWriteInjector : BaseInjector
    {
        public override ushort IncreasesMaxStackBy { get; } = 6;
        public override bool AddsVariables { get; } = true;

        internal HashSet<OpCode> Opcodes = new HashSet<OpCode>()
        {
            OpCodes.Stsfld, OpCodes.Stfld
        };

        public FieldWriteInjector(InjectionContext context)
            : base(context, AnalysisEventType.FieldWrite)
        {
            EventInfoTypeDefinition = Definitions.FieldWritten.ArgsType;
            EventInfoTypeSig = Definitions.FieldWritten.ArgsType.ToTypeSig();
            EventInfoTypeCtor = Definitions.FieldWritten.ArgsCtor;
            EventRaiserMethod = Definitions.FieldWritten.RaiserMethod;
        }

        public override void Inject(MethodDef method, Instruction instruction, BasePattern pattern)
        {
            var fieldPattern = (FieldPattern)pattern;
            Inject(method, instruction, fieldPattern);
        }

        public void Inject(MethodDef method, Instruction instruction, FieldPattern pattern)
        {
            var variablePool = VariableMethodPool.Instance;
            var importedEventInfoSig = method.Module.Import(EventInfoTypeSig);
            var eventInfoVariable = variablePool.GetVariable(method, importedEventInfoSig);
            var instanceVariable = (Local)null;
            var valueVariable = (Local)null;

            // Check if we need to pack instance
            if (!pattern.FieldFlags.HasFlag(FieldFlags.Static))
            {
                // If declaring type is value type we pack as IntPtr
                if (pattern.HasDeclaringValueType)
                {
                    // TODO: this is an unresolved issue
                    Log.Warning("Capturing instance information for structs when dealing with fields in not yet implemented.");
                    return;
                }
                // Otherwise provide reference to actual object
                else if (pattern.HasDeclaringReferenceType)
                {
                    var importedDeclaringTypeSig = method.Module.Import(pattern.DeclaringTypeSig);
                    instanceVariable = variablePool.GetVariable(method, importedDeclaringTypeSig);
                }
            }
            var fieldType = Context.ResolveField(pattern.FieldRef).FieldType;
            valueVariable = variablePool.GetVariable(method, fieldType);

            var injectBefore = GenerateInjectBeforeCode(instanceVariable, valueVariable, pattern.FieldTypeSig);
            var injectAfter = GenerateInjectAterCode(method, pattern, eventInfoVariable, instanceVariable, valueVariable);

            // Inject into caller
            method.InjectBefore(instruction, injectBefore);
            method.InjectAfter(instruction, injectAfter);

            // Release variables
            variablePool.StoreVariable(method, eventInfoVariable);
            variablePool.StoreVariable(method, valueVariable);
            if (instanceVariable != null)
                variablePool.StoreVariable(method, instanceVariable);
        }

        private List<Instruction> GenerateInjectBeforeCode(Local instance, Local value, TypeSig valueTypeSig)
        {
            var instructionsList = new List<Instruction>();
            // Copy value from stack
            /*if (valueTypeSig.IsValueType)
                instructionsList.Add(Instruction.Create(OpCodes.Box, valueTypeSig.ToTypeDefOrRef()));*/
            instructionsList.Add(Instruction.Create(OpCodes.Stloc, value));
            // Copy and reconstruct instance (if available)
            if (instance != null)
            {
                instructionsList.Add(Instruction.Create(OpCodes.Dup));
                instructionsList.Add(Instruction.Create(OpCodes.Stloc, instance));
            }
            // Reconstruct value on stack
            instructionsList.Add(Instruction.Create(OpCodes.Ldloc, value));
            /*
            if (valueTypeSig.IsValueType)
                instructionsList.Add(Instruction.Create(OpCodes.Unbox_Any, valueTypeSig.ToTypeDefOrRef()));*/

            return instructionsList;
        }

        private List<Instruction> GenerateInjectAterCode(MethodDef method, FieldPattern pattern, Local info, Local instance, Local value)
        {
            var instructionsList = new List<Instruction>();
            var importedFieldType = method.Module.Import(pattern.FieldDef.FieldType);

            // Load local struct by address
            instructionsList.Add(Instruction.Create(OpCodes.Ldloca, info));
            // Try load instance
            if (instance != null)
            {
                // Load object or IntPtr based on declaring type
                instructionsList.Add(Instruction.Create(OpCodes.Ldloc, instance));
            }
            else
            {
                // Load IntPtr.Zero if declaring type is value type
                if (pattern.HasDeclaringValueType)
                {
                    // TODO: this is an unresolved issue
                }
                // Load null if declaring type is reference type
                else if (pattern.HasDeclaringReferenceType || pattern.FieldFlags.HasFlag(FieldFlags.Static))
                    instructionsList.Add(Instruction.Create(OpCodes.Ldnull));
            }
            // Load written value
            instructionsList.Add(Instruction.Create(OpCodes.Ldloc, value));
            // Check if we need to box the value
            if (value.Type.IsValueType || value.Type.IsGenericParameter || value.Type.IsGenericTypeParameter || value.Type.IsGenericMethodParameter)
                instructionsList.Add(Instruction.Create(OpCodes.Box, value.Type.ToTypeDefOrRef()));
            // Load field descriptor
            instructionsList.Add(Instruction.Create(OpCodes.Ldstr, pattern.FieldDef.FullName));
            // Load field flags
            instructionsList.Add(Instruction.Create(OpCodes.Ldc_I4, (int)pattern.FieldFlags));
            // Construct event info
            instructionsList.Add(CreateEventInfoCtorInstruction(method));
            // Get event dispatcher instance
            instructionsList.Add(CreateEventDispatcherLoadInstruction(method));
            // Load event info
            instructionsList.Add(Instruction.Create(OpCodes.Ldloc, info));
            // Raise event
            instructionsList.Add(CreateEventRaiserCallInstruction(method));

            return instructionsList;
        }
    }
}
