/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Common;
using SharpDetect.Injector.PatternRecognizers.Patterns;
using SharpDetect.Injector.Runtime;
using SharpDetect.Injector.Utilities;
using Serilog;

namespace SharpDetect.Injector.Injectors
{
    /// <summary>
    /// Injector responsible for injecting analysis call around CALL* instructions
    /// </summary>
    class MethodCallInjector : BaseInjector
    {
        public override ushort IncreasesMaxStackBy { get; } = 5;
        public override bool AddsVariables { get; } = true;
        internal PackedParametersCache PackedParameters { get; private set; }

        public MethodCallInjector(InjectionContext context, PackedParametersCache parametersCache)
            : base(context, AnalysisEventType.MethodCall)
        {
            this.PackedParameters = parametersCache;
            EventInfoTypeDefinition = Definitions.MethodCall.ArgsType;
            EventInfoTypeSig = Definitions.MethodCall.ArgsType.ToTypeSig();
            EventInfoTypeCtor = Definitions.MethodCall.ArgsCtor;
            EventRaiserMethod = Definitions.MethodCall.RaiserMethod;
        }

        public override void Inject(MethodDef caller, Instruction instruction, BasePattern pattern)
        {
            var callPattern = (CallsPattern)pattern;
            var variablesPool = VariableMethodPool.Instance;

            var importedTypeInfoSig = caller.Module.Import(EventInfoTypeSig);
            var eventInfoVariable = variablesPool.GetVariable(caller, importedTypeInfoSig);
            var calledMethod = callPattern.CalledMethodDef;

            // Pack parameters
            var (instructionsList, parametersVariable) = PackedParameters.PackParameters(calledMethod, caller, instruction, Context);
            var parametersLoadInstruction = (parametersVariable != null) ?
                Instruction.Create(OpCodes.Ldloc, parametersVariable) : Instruction.Create(OpCodes.Ldnull);

            // Prepare event info struct
            instructionsList.Add(Instruction.Create(OpCodes.Ldloca, eventInfoVariable));
            instructionsList.Add(parametersLoadInstruction);
            instructionsList.Add(Instruction.Create(OpCodes.Ldstr, ((IMethod)instruction.Operand).FullName));
            instructionsList.Add(CreateEventInfoCtorInstruction(caller));

            // Raise method call event
            instructionsList.Add(CreateEventDispatcherLoadInstruction(caller));
            instructionsList.Add(Instruction.Create(OpCodes.Ldloc, eventInfoVariable));
            instructionsList.Add(CreateEventRaiserCallInstruction(caller));

            // Check if we need to raise more specific events
            foreach (var extension in callPattern.CallExtensions)
                AddExtensionEventRaiser(caller, extension, eventInfoVariable, instructionsList);

            // Inject all into caller
            caller.InjectBefore(instruction, instructionsList);

            // Return variable
            variablesPool.StoreVariable(caller, eventInfoVariable);
        }
    }
}
