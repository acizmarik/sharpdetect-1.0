/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Common;
using SharpDetect.Injector.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpDetect.Injector.Injectors
{
    class MethodCallsTracker
    {
        public Dictionary<Instruction, Local> PackedParams { get; private set; }
        public Dictionary<Local, List<(int, ByRefSig, Local)>> ByRefParameters { get; private set; }
        public InjectionContext Context { get; private set; }
        public MethodDef Caller { get; private set; }
        internal IType ImportedParameterType { get; private set; }
        internal TypeSig ImportedParameterTypeSig { get; private set; }
        internal IMethod ImportedParameterCtor { get; private set; }

        public MethodCallsTracker(InjectionContext context, MethodDef callerMethod)
        {
            this.Context = context;
            this.Caller = callerMethod;

            this.PackedParams = new Dictionary<Instruction, Local>();
            this.ByRefParameters = new Dictionary<Local, List<(int, ByRefSig, Local)>>();

            var parameterType = Definitions.ParameterType;
            var parameterTypeSig = Definitions.ParameterType.ToTypeSig();
            var parameterCtor = Definitions.ParameterCtor;

            this.ImportedParameterType = Caller.Module.Import(parameterType);
            this.ImportedParameterTypeSig = Caller.Module.Import(parameterTypeSig);
            this.ImportedParameterCtor = Caller.Module.Import(parameterCtor);
        }

        public void PackParameters(MethodDef calledMethod, Instruction instruction, out Local variable, out List<Instruction> instructions)
        {
            var instructionsList = new List<Instruction>();
            var variables = new List<Local>();
            var valuesArrayVariable = VariableMethodPool.Instance.GetVariable(Caller, new SZArraySig(ImportedParameterTypeSig));
            var actualParametersCount = calledMethod.Parameters.Where(p => !(p.IsHiddenThisParameter && calledMethod.IsConstructor)).Count();
            var importedParamSigs = new Dictionary<Local, TypeSig>(actualParametersCount);

            if (actualParametersCount > 0)
            {
                // Copy parameters to variables
                foreach (var parameter in calledMethod.Parameters.ReverseList())
                {
                    var (result, importedParamSig) = TryCopyParam(parameter, instruction, calledMethod, out var ParameterValueVariable, out var preprocessInstruction);
                    if (!result)
                        continue;

                    variables.Add(ParameterValueVariable);
                    importedParamSigs.Add(ParameterValueVariable, importedParamSig);
                    if (preprocessInstruction != null)
                        instructionsList.Add(preprocessInstruction);
                    instructionsList.Add(Instruction.Create(OpCodes.Stloc, ParameterValueVariable));
                }

                // Create array that will contain parameters
                var arraySize = variables.Count;
                instructionsList.Add(Instruction.Create(OpCodes.Ldc_I4, arraySize));
                instructionsList.Add(Instruction.Create(OpCodes.Newarr, (TypeSpec)ImportedParameterType));
                instructionsList.Add(Instruction.Create(OpCodes.Stloc, valuesArrayVariable));
                var arrayIndex = 0;
                var parameterIndex = (calledMethod.IsStatic) ? 0 : -1;

                // Store parameters to array
                var variablesEnumerator = variables.ReverseList().GetEnumerator();
                variablesEnumerator.MoveNext();
                foreach (var parameter in calledMethod.Parameters)
                {
                    var currentParamValue = variablesEnumerator.Current;
                    foreach (var storeInstruction in StoreParam(parameter, parameterIndex, currentParamValue, importedParamSigs[currentParamValue], 
                        calledMethod, valuesArrayVariable, arrayIndex))
                        instructionsList.Add(storeInstruction);
                    ++arrayIndex;
                    ++parameterIndex;
                    variablesEnumerator.MoveNext();
                }

                // Replicate parameters on stack
                foreach (var parameterValueVariable in variables.ReverseList())
                    instructionsList.Add(Instruction.Create(OpCodes.Ldloc, parameterValueVariable));
            }
            else
            {
                instructionsList.Add(Instruction.Create(OpCodes.Nop));
                VariableMethodPool.Instance.StoreVariable(Caller, valuesArrayVariable);
                valuesArrayVariable = null;
            }

            if (valuesArrayVariable != null)
            {
                // Store parameters
                PackedParams.Add(instruction, valuesArrayVariable);

                // Free temporary variables
                foreach (var tempVariable in variables.Where(v => !v.Type.IsByRef))
                    VariableMethodPool.Instance.StoreVariable(Caller, tempVariable);
            }

            instructions = instructionsList;
            variable = valuesArrayVariable;
        }

        private (bool, TypeSig) TryCopyParam(Parameter parameter, Instruction instruction, MethodDef called, out Local variable, out Instruction preprocessInstruction)
        {
            variable = null;
            preprocessInstruction = null;

            // Skip hidden this parameter for constructors
            if (parameter.IsHiddenThisParameter && called.IsConstructor)
                return (false, null);

            // Retrieve missing information about generics
            var importedParamSig = Caller.Module.Import(DnlibExtensions.GetParameterSignature(called, instruction, parameter, Context));

            // If this is hidden "this" parameter and we are not in constructor handle this as a special case
            if (parameter.IsHiddenThisParameter)
            {
                var instructionIndex = Caller.Body.Instructions.IndexOf(instruction);
                // Check if previous instruction is constrained
                if (instructionIndex > 0 && Caller.Body.Instructions[instructionIndex - 1].OpCode == OpCodes.Constrained)
                {
                    var constrainedInstruction = Caller.Body.Instructions[instructionIndex - 1];
                    importedParamSig = new ByRefSig(((ITypeDefOrRef)constrainedInstruction.Operand).ToTypeSig());

                    // This parameter was actually passed as pointer
                    variable = VariableMethodPool.Instance.GetVariable(Caller, importedParamSig);
                    //preprocessInstruction = Instruction.Create(OpCodes.Box, (ITypeDefOrRef)constrainedInstruction.Operand);

                    return (true, importedParamSig);
                }
            }
            variable = VariableMethodPool.Instance.GetVariable(Caller, importedParamSig);

            return (true, importedParamSig);
        }

        private IEnumerable<Instruction> StoreParam(Parameter parameter, int paramIndex, Local paramValue, TypeSig paramSig, MethodDef called, Local paramsArray, int arrayIndex)
        {
            // Skip hidden this parameter for constructors
            if (parameter.IsHiddenThisParameter && called.IsConstructor)
                yield break;

            // Store parameter to array
            yield return Instruction.Create(OpCodes.Ldloc, paramsArray);
            yield return Instruction.Create(OpCodes.Ldc_I4, arrayIndex);

            // Create parameter
            yield return Instruction.Create(OpCodes.Ldc_I4, paramIndex);
            yield return Instruction.Create(OpCodes.Ldloc, paramValue);
            // Parameter is passed by reference => we need to make indirect load first
            if (paramSig.IsByRef)
            {
                var elementType = paramSig.Next;

                // Constrained calls
                if (elementType.IsGenericParameter)
                {
                    yield return Instruction.Create(OpCodes.Ldobj, elementType.ToTypeDefOrRef());
                    yield return Instruction.Create(OpCodes.Box, elementType.ToTypeDefOrRef());
                }
                // Regular parameters
                else
                {
                    yield return paramSig.ToByRefSig().MakeIndirectLoadOfType();
                    if (!ByRefParameters.ContainsKey(paramsArray))
                        ByRefParameters.Add(paramsArray, new List<(int, ByRefSig, Local)>());
                    ByRefParameters[paramsArray].Add((arrayIndex, paramSig.ToByRefSig(), paramValue));

                    // Check if we need to box it
                    if (elementType.IsValueType)
                        yield return Instruction.Create(OpCodes.Box, elementType.ToTypeDefOrRef());
                }
            }
            // Parameter is of value type and has to be boxed first
            else if (paramValue.Type.IsValueType)
                yield return Instruction.Create(OpCodes.Box, paramSig.ToTypeDefOrRef());
            yield return Instruction.Create(OpCodes.Newobj, ImportedParameterCtor);
            yield return Instruction.Create(OpCodes.Stelem, (ITypeDefOrRef)ImportedParameterType);
        }

        public bool TryGetParameters(IMethod callerMethod, Instruction instruction, out List<Instruction> preprocess, out Local parameters)
        {
            if (PackedParams.ContainsKey(instruction))
            {
                preprocess = null;
                parameters = PackedParams[instruction];

                // Check if we need to preprocess any parameters passed by reference
                if (ByRefParameters.ContainsKey(parameters))
                {
                    preprocess = new List<Instruction>();
                    foreach (var (index, type, variable) in ByRefParameters[parameters])
                    {
                        preprocess.Add(Instruction.Create(OpCodes.Ldloc, parameters));
                        preprocess.Add(Instruction.Create(OpCodes.Ldc_I4, index));
                        preprocess.Add(Instruction.Create(OpCodes.Dup));
                        preprocess.Add(Instruction.Create(OpCodes.Ldloc, variable));
                        preprocess.Add(type.MakeIndirectLoadOfType());

                        // Check if we need to box it
                        if (type.Next.IsValueType)
                        {
                            var importedType = callerMethod.Module.Import(type.Next);
                            preprocess.Add(Instruction.Create(OpCodes.Box, importedType.ToTypeDefOrRef()));
                        }

                        // Replace parameter
                        preprocess.Add(Instruction.Create(OpCodes.Newobj, ImportedParameterCtor));
                        preprocess.Add(Instruction.Create(OpCodes.Stelem, (ITypeDefOrRef)ImportedParameterType));
                    }
                }

                return true;
            }

            preprocess = null;
            parameters = null;
            return false;
        }
    }
}
