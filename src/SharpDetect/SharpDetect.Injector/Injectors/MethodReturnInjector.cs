/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using SharpDetect.Injector.Utilities;
using SharpDetect.Core;
using SharpDetect.Injector.Runtime;
using dnlib.DotNet.Emit;
using dnlib.DotNet;
using SharpDetect.Common;
using SharpDetect.Injector.PatternRecognizers.Patterns;

namespace SharpDetect.Injector.Injectors
{
    /// <summary>
    /// Injector responsible for injecting analysis code around method return values (just after CALL* instructions)
    /// </summary>
    class MethodReturnInjector : BaseInjector
    {
        public override ushort IncreasesMaxStackBy { get; } = 5;
        public override bool AddsVariables { get; } = true;

        internal PackedParametersCache ParametersCache { get; private set; }
        internal string SystemVoidFullName { get; } = typeof(void).FullName;

        public MethodReturnInjector(InjectionContext context, PackedParametersCache paramsCache)
            : base(context, AnalysisEventType.MethodReturn)
        {
            this.ParametersCache = paramsCache;
            EventInfoTypeDefinition = Definitions.MethodReturn.ArgsType;
            EventInfoTypeSig = Definitions.MethodReturn.ArgsType.ToTypeSig();
            EventInfoTypeCtor = Definitions.MethodReturn.ArgsCtor;
            EventRaiserMethod = Definitions.MethodReturn.RaiserMethod;
        }

        public override void Inject(MethodDef method, Instruction instruction, BasePattern pattern)
        {
            var callPattern = (CallsPattern)pattern;
            var variablesPool = VariableMethodPool.Instance;
            var importedTypeInfoSig = method.Module.Import(EventInfoTypeSig);
            var returnValueVariable = (Local)null;
            var eventInfoVariable = variablesPool.GetVariable(method, importedTypeInfoSig);
            var instructionsList = new List<Instruction>();

            // Get return type of the called method
            var importedReturnType = method.Module.Import(DnlibExtensions.GetReturnTypeSignature(
                callPattern.CalledMethodDef, instruction, 
                callPattern.CalledMethodDef.ReturnType, Context));
            var hasReturnValue = importedReturnType.FullName != SystemVoidFullName;

            // Prepare information about return value
            var (hasParameters, preprocessInstructions, parametersVariable) = ParametersCache.TryGetParameters(method, instruction);

            // Additional code needs to be inserted to refresh value of parameters captured by reference
            if (hasParameters && preprocessInstructions != null)
            {
                foreach (var preprocessInstruction in preprocessInstructions)
                    instructionsList.Add(preprocessInstruction);
            }         

            // Get return value if available
            if (hasReturnValue)
            {
                returnValueVariable = VariableMethodPool.Instance.GetVariable(method, method.Module.CorLibTypes.Object);
                // Copy return value on stack
                instructionsList.Add(Instruction.Create(OpCodes.Dup));
                // Box it if it is value type
                if (importedReturnType.IsValueType)
                    instructionsList.Add(Instruction.Create(OpCodes.Box, importedReturnType.ToTypeDefOrRef()));
                // Store the copy
                instructionsList.Add(Instruction.Create(OpCodes.Stloc, returnValueVariable));
            }
            // Prepare event info struct
            instructionsList.Add(Instruction.Create(OpCodes.Ldloca, eventInfoVariable));
            // Get return value (if available) and set flag indicating whether it is available
            if (hasReturnValue)
            {
                instructionsList.Add(Instruction.Create(OpCodes.Ldloc, returnValueVariable));
                instructionsList.Add(Instruction.Create(OpCodes.Ldc_I4_1));
            }
            else
            {
                instructionsList.Add(Instruction.Create(OpCodes.Ldnull));
                instructionsList.Add(Instruction.Create(OpCodes.Ldc_I4_0));
            }
            // Load packed parameters
            if (hasParameters)
                instructionsList.Add(Instruction.Create(OpCodes.Ldloc, parametersVariable));
            else
                instructionsList.Add(Instruction.Create(OpCodes.Ldnull));
            // Load called method description
            instructionsList.Add(Instruction.Create(OpCodes.Ldstr, ((IMethod)instruction.Operand).FullName));
            // Construct event descriptor
            instructionsList.Add(CreateEventInfoCtorInstruction(method));

            // Check if we need to raise multiple events
            foreach (var extension in callPattern.ReturnExtensions)
                AddExtensionEventRaiser(method, extension, eventInfoVariable, instructionsList);

            // Raise method return event
            instructionsList.Add(CreateEventDispatcherLoadInstruction(method));
            instructionsList.Add(Instruction.Create(OpCodes.Ldloc, eventInfoVariable));
            instructionsList.Add(CreateEventRaiserCallInstruction(method));

            // Inject into caller method
            method.InjectAfter(instruction, instructionsList);

            // Release acquired variables
            if (hasReturnValue)
                VariableMethodPool.Instance.StoreVariable(method, returnValueVariable);
            variablesPool.StoreVariable(method, eventInfoVariable);
        }
    }
}
