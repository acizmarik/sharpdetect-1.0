/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Common;
using SharpDetect.Injector.PatternRecognizers.Patterns;
using SharpDetect.Injector.Utilities;

namespace SharpDetect.Injector.Injectors
{
    class ObjectCreateInjector : BaseInjector
    {
        public override ushort IncreasesMaxStackBy { get; } = 2;
        public override bool AddsVariables { get; } = true;

        public ObjectCreateInjector(InjectionContext context)
            : base(context, AnalysisEventType.ObjectCreate)
        {
            EventInfoTypeDefinition = Definitions.ObjectCreate.ArgsType;
            EventInfoTypeCtor = Definitions.ObjectCreate.ArgsCtor;
            EventRaiserMethod = Definitions.ObjectCreate.RaiserMethod;
        }

        public override void Inject(MethodDef method, Instruction instruction, BasePattern pattern)
        {
            var newobjPattern = (NewobjPattern)pattern;
            var variablePool = VariableMethodPool.Instance;
            var calledCtor = newobjPattern.CtorDef;
            var objVariable = variablePool.GetVariable(method, method.Module.CorLibTypes.Object);

            var instructionsList = new List<Instruction>();
            // Copy new object and store it to variable
            instructionsList.Add(Instruction.Create(OpCodes.Dup));
            instructionsList.Add(Instruction.Create(OpCodes.Stloc, objVariable));

            // Raise newobj event
            instructionsList.Add(CreateEventDispatcherLoadInstruction(method));
            instructionsList.Add(Instruction.Create(OpCodes.Ldloc, objVariable));
            instructionsList.Add(CreateEventRaiserCallInstruction(method));

            // Raise any extensions
            foreach (var extension in newobjPattern.NewobjExtensions)
                AddExtensionEventRaiser(method, extension, objVariable, instructionsList);

            // Inject into caller
            method.InjectAfter(instruction, instructionsList);

            // Return acquired variables
            variablePool.StoreVariable(method, objVariable);
        }
    }
}
