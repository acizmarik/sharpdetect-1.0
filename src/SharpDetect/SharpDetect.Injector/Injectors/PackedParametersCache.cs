/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using dnlib.DotNet;
using dnlib.DotNet.Emit;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.Injector.Injectors
{
    class PackedParametersCache
    {
        public InjectionContext Context { get; private set; }
        public Dictionary<MethodDef, MethodCallsTracker> TrackedMethods { get; private set; }

        public PackedParametersCache()
        {
            TrackedMethods = new Dictionary<MethodDef, MethodCallsTracker>();
        }

        public (List<Instruction>, Local) PackParameters(MethodDef called, MethodDef caller, Instruction instruction, InjectionContext context)
        {
            if (!TrackedMethods.ContainsKey(caller))
                TrackedMethods.Add(caller, new MethodCallsTracker(context, caller));

            TrackedMethods[caller].PackParameters(called, instruction, out var variable, out var instructions);
            return (instructions, variable);
        }

        public (bool, List<Instruction>, Local) TryGetParameters(MethodDef caller, Instruction instruction)
        {
            if (!TrackedMethods.ContainsKey(caller))
                return (false, null, null);
            if (TrackedMethods[caller].TryGetParameters(caller, instruction, out var instructions, out var variable))
                return (true, instructions, variable);

            return (false, null, null);
        }

        public void FinishMethod(MethodDef caller)
        {
            TrackedMethods.Remove(caller);
        }
    }
}
