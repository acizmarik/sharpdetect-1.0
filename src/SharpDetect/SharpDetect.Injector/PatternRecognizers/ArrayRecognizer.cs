/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Common;
using SharpDetect.Core;
using SharpDetect.Injector.Injectors;
using SharpDetect.Injector.PatternRecognizers.Patterns;
using SharpDetect.Injector.Utilities;

namespace SharpDetect.Injector.PatternRecognizers
{
    class ArrayRecognizer : BaseRecognizer
    {
        protected Dictionary<Code, (AnalysisEventType, BaseInjector)> Injectors;

        public ArrayRecognizer(InjectionContext context, BaseInjector arrayCreate, BaseInjector arrayElementRead, BaseInjector arrayElementWrite)
            : base(context)
        {
            Injectors = new Dictionary<Code, (AnalysisEventType, BaseInjector)>()
            {
                { OpCodes.Newarr.Code, (AnalysisEventType.ArrayCreate, arrayCreate) },
                { OpCodes.Ldelem.Code, (AnalysisEventType.ArrayRead, arrayElementRead) },
                { OpCodes.Ldelema.Code, (AnalysisEventType.ArrayRead, arrayElementRead) },
                { OpCodes.Ldelem_I.Code, (AnalysisEventType.ArrayRead, arrayElementRead) },
                { OpCodes.Ldelem_I1.Code, (AnalysisEventType.ArrayRead, arrayElementRead) },
                { OpCodes.Ldelem_I2.Code, (AnalysisEventType.ArrayRead, arrayElementRead) },
                { OpCodes.Ldelem_I4.Code, (AnalysisEventType.ArrayRead, arrayElementRead) },
                { OpCodes.Ldelem_I8.Code, (AnalysisEventType.ArrayRead, arrayElementRead) },
                { OpCodes.Ldelem_R4.Code, (AnalysisEventType.ArrayRead, arrayElementRead) },
                { OpCodes.Ldelem_R8.Code, (AnalysisEventType.ArrayRead, arrayElementRead) },
                { OpCodes.Ldelem_Ref.Code, (AnalysisEventType.ArrayRead, arrayElementRead) },
                { OpCodes.Ldelem_U1.Code, (AnalysisEventType.ArrayRead, arrayElementRead) },
                { OpCodes.Ldelem_U2.Code, (AnalysisEventType.ArrayRead, arrayElementRead) },
                { OpCodes.Ldelem_U4.Code, (AnalysisEventType.ArrayRead, arrayElementRead) },

                { OpCodes.Stelem.Code, (AnalysisEventType.ArrayWrite, arrayElementWrite) },
                { OpCodes.Stelem_I.Code, (AnalysisEventType.ArrayWrite, arrayElementWrite) },
                { OpCodes.Stelem_I1.Code, (AnalysisEventType.ArrayWrite, arrayElementWrite) },
                { OpCodes.Stelem_I2.Code, (AnalysisEventType.ArrayWrite, arrayElementWrite) },
                { OpCodes.Stelem_I4.Code, (AnalysisEventType.ArrayWrite, arrayElementWrite) },
                { OpCodes.Stelem_I8.Code, (AnalysisEventType.ArrayWrite, arrayElementWrite) },
                { OpCodes.Stelem_R4.Code, (AnalysisEventType.ArrayWrite, arrayElementWrite) },
                { OpCodes.Stelem_R8.Code, (AnalysisEventType.ArrayWrite, arrayElementWrite) },
                { OpCodes.Stelem_Ref.Code, (AnalysisEventType.ArrayWrite, arrayElementWrite) },
            };
        }

        internal override bool IsMatch(MethodDef method, Instruction current, CodeInspector inspector)
        {
            if (!CanRecognize(method, current, inspector.InjectingParameters))
                return false;

            return inspector.IsMatch(method);
        }

        public override bool CanRecognize(MethodDef method, Instruction current, IParametersInjector parameters)
        {
            return parameters.ArrayInjectors && Injectors.ContainsKey(current.OpCode.Code);
        }
        
        private TypeSig GetArrayElementTypeSig(int elementIndex, MethodDef method, Instruction current, int currentInstructionIndex)
        {
            if (current.Operand != null)
                return new SZArraySig(((ITypeDefOrRef)current.Operand).ToTypeSig());

            var currentIndex = currentInstructionIndex - 1;
            var totalPushes = 0;
            while (currentIndex >= 0)
            {
                // Search for instruction that pushed last item to evaluation stack
                var currentInstruction = method.Body.Instructions[currentIndex--];
                currentInstruction.CalculateStackUsage(out var pushes, out var pops);
                totalPushes += pushes;
                if (totalPushes != elementIndex)
                {
                    totalPushes -= pops;
                    if (totalPushes != elementIndex)
                        continue;
                }

                // If current instruction is Dup than find the previous push
                while (currentInstruction.OpCode.Code == Code.Dup)
                {
                    totalPushes = 0;
                    do
                    {
                        currentInstruction = method.Body.Instructions[currentIndex--];
                        currentInstruction.CalculateStackUsage(out pushes, out pops);
                        totalPushes += pushes;
                        if (totalPushes == 0)
                            break;
                        totalPushes -= pops;
                    } while (totalPushes != 0);
                }
                // It might be obtained as a return value of a regular method call or an constructor
                if (currentInstruction.OpCode.FlowControl == FlowControl.Call)
                    return method.ReturnType;
                // It might be obtained from newarr instruction
                else if (currentInstruction.OpCode.Code == Code.Newarr)
                    return ((ITypeDefOrRef)currentInstruction.Operand).MakeArrayType();
                // It might be obtained from an argument
                else if (currentInstruction.IsLdarg())
                    return currentInstruction.GetArgumentType(method.MethodSig, method.DeclaringType);
                // It might be obtained from a variable
                else if (currentInstruction.IsLdloc())
                    return currentInstruction.GetLocal(method.Body.Variables.Locals).Type;
                else if (currentInstruction.OpCode.Code == Code.Ldfld || currentInstruction.OpCode.Code == Code.Ldsfld)
                    return ((IField)currentInstruction.Operand).FieldSig.Type;
            }

            throw new DynamicAnalysisException("Could not determine type of item on stack");
        }

        public override (BasePattern, IEnumerable<BaseInjector>) Recognize(MethodDef method, Instruction current, int currentInstructionIndex)
        {
            var eventPattern = new ArrayPattern
            {
                EventType = Injectors[current.OpCode.Code].Item1,
                Instruction = current,
                InstructionIndex = currentInstructionIndex
            };
            var eventInjector = Injectors[current.OpCode.Code].Item2;

            switch (eventPattern.EventType)
            {
                case AnalysisEventType.ArrayWrite:
                    var writeArrayType = GetArrayElementTypeSig(3, method, current, currentInstructionIndex);
                    eventPattern.ValueTypeSig = writeArrayType.Next;
                    eventPattern.IndexTypeSig = method.Module.CorLibTypes.Int32;
                    eventPattern.ArrayTypeSig = writeArrayType;
                    eventPattern.StoringValueType = eventPattern.ValueTypeSig.IsValueType;
                    break;
                case AnalysisEventType.ArrayRead:
                    var readArrayType = GetArrayElementTypeSig(2, method, current, currentInstructionIndex);
                    eventPattern.IndexTypeSig = method.Module.CorLibTypes.Int32;
                    eventPattern.ArrayTypeSig = readArrayType;
                    break;
                case AnalysisEventType.ArrayCreate:
                    eventPattern.ArrayTypeSig = new SZArraySig(((ITypeDefOrRef)current.Operand).ToTypeSig());
                    break;
            }           

            return (eventPattern, new BaseInjector[] { eventInjector });
        }
    }
}
