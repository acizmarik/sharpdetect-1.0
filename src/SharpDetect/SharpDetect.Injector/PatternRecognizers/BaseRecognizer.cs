/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Injector.Injectors;
using SharpDetect.Injector.PatternRecognizers.Patterns;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.Injector.PatternRecognizers
{
    abstract class BaseRecognizer
    {
        protected InjectionContext Context { get; private set; }

        public abstract bool CanRecognize(MethodDef method, Instruction current, IParametersInjector parameters);
        public abstract (BasePattern, IEnumerable<BaseInjector>) Recognize(MethodDef method, Instruction current, int currentInstructionIndex);

        internal virtual bool IsMatch(MethodDef method, Instruction current, CodeInspector inspector)
            => CanRecognize(method, current, inspector.InjectingParameters);

        public BaseRecognizer(InjectionContext context)
        {
            this.Context = context;
        }

        protected IEnumerable<Instruction> GetPrefixInstructions(MethodDef method, int currentInstructionIndex)
        {
            var currentIndex = currentInstructionIndex - 1;
            while (currentIndex > 0 && method.Body.Instructions[currentIndex].OpCode.OpCodeType == OpCodeType.Prefix)
            {
                yield return method.Body.Instructions[currentIndex];
                --currentIndex;
            }
        }
    }
}
