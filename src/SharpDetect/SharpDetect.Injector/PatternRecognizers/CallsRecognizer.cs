/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Common;
using SharpDetect.Common.Events;
using SharpDetect.Common.Extensions;
using SharpDetect.Injector.Injectors;
using SharpDetect.Injector.PatternRecognizers.Patterns;
using Serilog;
using SharpDetect.Core;

namespace SharpDetect.Injector.PatternRecognizers
{
    class CallsRecognizer : BaseRecognizer
    {
        protected BaseInjector MethodCallInjector { get; private set; }
        protected BaseInjector MethodReturnInjector { get; private set; }

        public CallsRecognizer(InjectionContext context, BaseInjector methodCall, BaseInjector methodReturn)
            : base(context)
        {
            this.MethodCallInjector = methodCall;
            this.MethodReturnInjector = methodReturn;
        }

        internal override bool IsMatch(MethodDef method, Instruction current, CodeInspector inspector)
        {
            if (!CanRecognize(method, current, inspector.InjectingParameters))
                return false;

            var called = (IMethod)current.Operand;
            return inspector.IsMatch(called);
        }

        public override bool CanRecognize(MethodDef method, Instruction current, IParametersInjector parameters)
        {
            if (!parameters.MethodInjectors)
                return false;

            if (current.OpCode.Code != Code.Call && current.OpCode.Code != Code.Callvirt)
                return false;

            return true;
        }

        public override (BasePattern, IEnumerable<BaseInjector>) Recognize(MethodDef method, Instruction current, int currentInstructionIndex)
        {
            var eventPattern = new CallsPattern
            {
                EventType = AnalysisEventType.MethodCall,
                CalledMethodDef = Context.ResolveMethod((IMethod)current.Operand),
                Instruction = current,
                InstructionIndex = currentInstructionIndex
            };

            // Method call can have prefix instructions
            foreach (var prefix in GetPrefixInstructions(method, currentInstructionIndex))
            {
                eventPattern.HasPrefixInstruction = true;

                if (prefix.OpCode == OpCodes.Constrained)
                    eventPattern.IsConstrained = true;
                else
                {
                    Log.Error("Unrecognized prefix instruction {prefix} for instruction {instruction}", prefix, current);
                    throw new DynamicAnalysisException("Unrecognized instruction prefix");
                }
            }

            // Get all method call extensions for the called method
            var extensions = MethodExtensions.GetExtensions(AnalysisEventType.MethodCall, ((IMethod)current.Operand).FullName);
            if (extensions != null)
            {
                var callExtensions = new List<AnalysisEvent>();
                foreach (var extension in extensions)
                {
                    var dynamicAnalysisEvents = MethodExtensions.GetEventsFromType(extension);
                    if (dynamicAnalysisEvents == null)
                        continue;

                    foreach (var item in dynamicAnalysisEvents)
                        callExtensions.Add(item);
                }
                eventPattern.CallExtensions = callExtensions;
            }
            else
                eventPattern.CallExtensions = Enumerable.Empty<AnalysisEvent>();
            // Get all method return extensions for the called method
            extensions = MethodExtensions.GetExtensions(AnalysisEventType.MethodReturn, ((IMethod)current.Operand).FullName);
            if (extensions != null)
            {
                var returnExtensions = new List<AnalysisEvent>();
                foreach (var extension in extensions)
                {
                    var dynamicAnalysisEvents = MethodExtensions.GetEventsFromType(extension);
                    if (dynamicAnalysisEvents == null)
                        continue;

                    foreach (var item in dynamicAnalysisEvents)
                        returnExtensions.Add(item);
                }
                eventPattern.ReturnExtensions = returnExtensions;
            }
            else
                eventPattern.ReturnExtensions = Enumerable.Empty<AnalysisEvent>();

            return (eventPattern, new BaseInjector[] { MethodCallInjector, MethodReturnInjector });
        }
    }
}
