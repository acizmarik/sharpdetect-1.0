/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Text;
using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Injector.Injectors;
using SharpDetect.Injector.PatternRecognizers.Patterns;

namespace SharpDetect.Injector.PatternRecognizers
{
    class ClassConstructRecognizer : BaseRecognizer
    {
        protected BaseInjector ClassCreateInjector { get; private set; }

        public ClassConstructRecognizer(InjectionContext context, BaseInjector classCreate)
            : base(context)
        {
            this.ClassCreateInjector = classCreate;
        }

        internal override bool IsMatch(MethodDef method, Instruction current, CodeInspector inspector)
        {
            if (!CanRecognize(method, current, inspector.InjectingParameters))
                return false;

            return inspector.IsMatch(method.DeclaringType);
        }

        public override bool CanRecognize(MethodDef method, Instruction current, IParametersInjector parameters)
        {
            if (!parameters.ClassCreateInjector)
                return false;

            if (!method.IsStaticConstructor)
                return false;

            if (current.OpCode.Code != Code.Ret)
                return false;

            return true;
        }

        public override (BasePattern, IEnumerable<BaseInjector>) Recognize(MethodDef method, Instruction current, int currentInstructionIndex)
        {
            var pattern = new ClassConstructPattern()
            {
                EventType = Common.AnalysisEventType.ClassCreate,
                HasPrefixInstruction = false,
                Instruction = current,
                InstructionIndex = currentInstructionIndex,
                FullTypename = method.DeclaringType.FullName
            };

            return (pattern, new BaseInjector[] { ClassCreateInjector });
        }
    }
}
