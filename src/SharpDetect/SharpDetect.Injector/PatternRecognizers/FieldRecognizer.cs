/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Common;
using SharpDetect.Injector.Injectors;
using SharpDetect.Injector.PatternRecognizers.Patterns;
using SharpDetect.Injector.Utilities;
using Serilog;
using SharpDetect.Core;
using SharpDetect.Common.Events;

namespace SharpDetect.Injector.PatternRecognizers
{
    class FieldRecognizer : BaseRecognizer
    {
        protected Dictionary<Code, (AnalysisEventType, BaseInjector)> Injectors;

        public FieldRecognizer(InjectionContext context, BaseInjector fieldRead, BaseInjector fieldWrite)
            : base(context)
        {
            Injectors = new Dictionary<Code, (AnalysisEventType, BaseInjector)>()
            {
                { OpCodes.Ldfld.Code, (AnalysisEventType.FieldRead, fieldRead) },
                { OpCodes.Ldflda.Code, (AnalysisEventType.FieldRead, fieldRead) },
                { OpCodes.Ldsfld.Code, (AnalysisEventType.FieldRead, fieldRead) },
                { OpCodes.Ldsflda.Code, (AnalysisEventType.FieldRead, fieldRead) },

                { OpCodes.Stfld.Code, (AnalysisEventType.FieldWrite, fieldWrite) },
                { OpCodes.Stsfld.Code, (AnalysisEventType.FieldWrite, fieldWrite) }
            };
        }

        internal override bool IsMatch(MethodDef method, Instruction current, CodeInspector inspector)
        {
            if (!CanRecognize(method, current, inspector.InjectingParameters))
                return false;

            var field = (IField)current.Operand;
            return inspector.IsMatch(field);
        }

        public override bool CanRecognize(MethodDef method, Instruction current, IParametersInjector parameters)
        {
            if (method.Name == ".ctor")
                return false;

            return parameters.FieldInjectors && Injectors.ContainsKey(current.OpCode.Code);
        }

        public override (BasePattern, IEnumerable<BaseInjector>) Recognize(MethodDef method, Instruction current, int currentInstructionIndex)
        {
            var eventPattern = new FieldPattern();
            var eventInjector = (BaseInjector)null;

            if (!Injectors.TryGetValue(current.OpCode.Code, out (AnalysisEventType eventType, BaseInjector injector) result))
            {
                Log.Error("CodeInspector {inspector} can not recognize {instruction}", nameof(FieldRecognizer), current);
                throw new DynamicAnalysisException("Invalid instruction for ");
            }
            if (result.eventType == AnalysisEventType.FieldRead)
                eventPattern.EventType = AnalysisEventType.FieldRead;                
            else if (result.eventType == AnalysisEventType.FieldWrite)
                eventPattern.EventType = AnalysisEventType.FieldWrite;

            var fieldRef = (IField)current.Operand;            
            var fieldDef = Context.ResolveField(fieldRef);
            var fieldTypeSig = Context.ResolveField(fieldRef).FieldType;

            eventInjector = result.injector;
            eventPattern.Instruction = current;
            eventPattern.InstructionIndex = currentInstructionIndex;

            eventPattern.FieldRef = fieldRef;
            eventPattern.FieldDef = fieldDef;
            eventPattern.FieldTypeSig = fieldTypeSig;
            // Get information about declaring type
            if (fieldRef is FieldDef)
                eventPattern.DeclaringTypeSig = ((FieldDef)fieldRef).DeclaringType.ToTypeSig();
            else
            {
                var fieldDeclaringTypeParentRef = ((MemberRef)fieldRef).Class;
                if (((MemberRef)fieldRef).Class is TypeRef)
                    eventPattern.DeclaringTypeSig = ((TypeRef)fieldDeclaringTypeParentRef).ToTypeSig();
                else if (((MemberRef)fieldRef).Class is TypeSpec)
                    eventPattern.DeclaringTypeSig = ((TypeSpec)fieldDeclaringTypeParentRef).ToTypeSig();
            }

            var fieldFlags = default(FieldFlags);

            // Check if field is readonly
            if (fieldDef.IsInitOnly)
                fieldFlags |= FieldFlags.Readonly;

            // Check if field is static
            if (fieldDef.Attributes.HasFlag(FieldAttributes.Static))
                fieldFlags |= FieldFlags.Static;

            // Field might have [ThreadStatic] attribute
            if (fieldDef.CustomAttributes.SingleOrDefault(a => a.TypeFullName == typeof(ThreadStaticAttribute).FullName) != null)
                fieldFlags |= FieldFlags.ThreadStatic;

            // Get declaring type information if field is instance
            if (!fieldFlags.HasFlag(FieldFlags.Static))
            {
                var declaringType = fieldDef.DeclaringType;
                eventPattern.HasDeclaringReferenceType = !declaringType.IsValueType;
                eventPattern.HasDeclaringValueType = declaringType.IsValueType;
            }

            // Fields can be prefixed with either volatile. or unaligned.
            foreach (var prefix in GetPrefixInstructions(method, currentInstructionIndex))
            {
                eventPattern.HasPrefixInstruction = true;

                if (prefix.OpCode == OpCodes.Volatile)
                    fieldFlags |= FieldFlags.Volatile;
                else if (prefix.OpCode == OpCodes.Unaligned)
                    fieldFlags |= FieldFlags.Unaligned;
                else
                {
                    Log.Error("Unrecognized prefix instruction {prefix} for instruction {instruction}", prefix, current);
                    throw new DynamicAnalysisException("Unrecognized instruction prefix");
                }
            }

            eventPattern.FieldFlags = fieldFlags;
            return (eventPattern, new BaseInjector[] { eventInjector });
        }
    }
}
