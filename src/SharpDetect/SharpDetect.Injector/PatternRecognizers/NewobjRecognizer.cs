/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Common;
using SharpDetect.Common.Events;
using SharpDetect.Common.Extensions;
using SharpDetect.Injector.Injectors;
using SharpDetect.Injector.PatternRecognizers.Patterns;

namespace SharpDetect.Injector.PatternRecognizers
{
    class NewobjRecognizer : BaseRecognizer
    {
        protected BaseInjector NewObjInjector { get; private set; }

        public NewobjRecognizer(InjectionContext context, BaseInjector newObj)
            : base(context)
        {
            this.NewObjInjector = newObj;
        }

        internal override bool IsMatch(MethodDef method, Instruction current, CodeInspector inspector)
        {
            if (!CanRecognize(method, current, inspector.InjectingParameters))
                return false;

            var called = (IMethod)current.Operand;
            return inspector.IsMatch(called);
        }

        public override bool CanRecognize(MethodDef method, Instruction current, IParametersInjector parameters)
        {
            if (!parameters.ObjectCreateInjector)
                return false;

            if (current.OpCode.Code != Code.Newobj)
                return false;

            // Check if reference-type object is being created
            var ctor = (IMethod)current.Operand;
            if (ctor.DeclaringType.IsValueType)
                return false;

            return true;
        }

        public override (BasePattern, IEnumerable<BaseInjector>) Recognize(MethodDef method, Instruction current, int currentInstructionIndex)
        {
            var eventPattern = new NewobjPattern
            {
                EventType = AnalysisEventType.ObjectCreate,
                CtorDef = Context.ResolveMethod((IMethod)current.Operand),
                Instruction = current,
                InstructionIndex = currentInstructionIndex
            };

            // Get all method call extensions for the called method
            var extensions = MethodExtensions.GetExtensions(AnalysisEventType.ObjectCreate, ((IMethod)current.Operand).FullName);
            if (extensions != null)
            {
                var newobjExtensions = new List<AnalysisEvent>();
                foreach (var extension in extensions)
                {
                    var dynamicAnalysisEvents = MethodExtensions.GetEventsFromType(extension);
                    if (dynamicAnalysisEvents == null)
                        continue;

                    foreach (var item in dynamicAnalysisEvents)
                        newobjExtensions.Add(item);
                }
                eventPattern.NewobjExtensions = newobjExtensions;
            }
            else
                eventPattern.NewobjExtensions = Enumerable.Empty<AnalysisEvent>();

            return (eventPattern, new BaseInjector[] { NewObjInjector });
        }
    }
}
