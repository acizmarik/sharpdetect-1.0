/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Injector.Utilities;
using SharpDetect.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SharpDetect.Injector.Runtime.CodeConstructs
{
    class EventBuilder
    {
        public AssemblyDef CoreLibrary { get; set; }
        public InjectionContext Context { get; set; }

        /// <summary>
        /// Implements new event with safe registering/unregistering
        /// </summary>
        /// <param name="name">Event name</param>
        /// <param name="type">Type defining the new event</param>
        /// <param name="handler">Event handler type</param>
        /// <param name="raiseMethodName">Event raiser method name</param>
        /// <param name="assembly">Assembly defining the type</param>
        /// <returns>Event field, event raiser</returns>
        public (FieldDefUser, MethodDef) BuildEvent(string name, TypeDef type, ITypeDefOrRef handler, ITypeDefOrRef args, string raiseMethodName, AssemblyDef assembly)
        {
            var newEvent = new EventDefUser(name, args);
            newEvent.EventType = handler;
            var newEventField = new FieldDefUser(name, new FieldSig(handler.ToTypeSig()));
            newEventField.Attributes = FieldAttributes.Private;
            type.Fields.Add(newEventField);

            // Find delegate handlers inside provided assembly
            var delegateType = assembly.ManifestModule.Types.Single(t => t.Namespace == "System" && t.Name == "Delegate");
            var delegateCombineMethod = delegateType.Methods.Single(m => m.Name == "Combine" && m.Parameters.Count == 2);
            var delegateRemoveMethod = delegateType.Methods.Single(m => m.Name == "Remove" && m.Parameters.Count == 2);
            // Find compare exchange method
            var intelockedType = assembly.ManifestModule.Types.Single(t => t.Namespace == "System.Threading" && t.Name == "Interlocked");
            var compareExchangeMethod = intelockedType.Methods.Single(m => m.Name == "CompareExchange" && m.HasGenericParameters);
            var genericCompareExchangeMethod = compareExchangeMethod.MakeGenericMethod(handler.ToTypeSig());

            var methodAttributes = MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.SpecialName;
            // Create add function for the event
            var handlerAdder = new MethodDefUser($"add_{name}", MethodSig.CreateInstance(CoreLibrary.ManifestModule.CorLibTypes.Void, handler.ToTypeSig()));
            handlerAdder.Attributes = methodAttributes;
            // Add parameter - new event handler
            handlerAdder.ParamDefs.Add(new ParamDefUser("handler", 1));
            // We need 3 temporary variables
            handlerAdder.Body = new CilBody() { KeepOldMaxStack = true };
            handlerAdder.Body.Variables.Add(new Local(handler.ToTypeSig()));
            handlerAdder.Body.Variables.Add(new Local(handler.ToTypeSig()));
            handlerAdder.Body.Variables.Add(new Local(handler.ToTypeSig()));
            handlerAdder.Body.InitLocals = true;
            handlerAdder.Body.MaxStack = 3;
            // Implement method
            handlerAdder.Body.Instructions.Add(Instruction.Create(OpCodes.Ldarg_0));
            handlerAdder.Body.Instructions.Add(Instruction.Create(OpCodes.Ldfld, newEventField));
            handlerAdder.Body.Instructions.Add(Instruction.Create(OpCodes.Stloc_0));
            {
                // Loop start
                var loopStartLabel = Instruction.Create(OpCodes.Nop);
                handlerAdder.Body.Instructions.Add(loopStartLabel);
                handlerAdder.Body.Instructions.Add(Instruction.Create(OpCodes.Ldloc_0));
                handlerAdder.Body.Instructions.Add(Instruction.Create(OpCodes.Stloc_1));
                handlerAdder.Body.Instructions.Add(Instruction.Create(OpCodes.Ldloc_1));
                handlerAdder.Body.Instructions.Add(Instruction.Create(OpCodes.Ldarg_1));
                handlerAdder.Body.Instructions.Add(Instruction.Create(OpCodes.Call, delegateCombineMethod));
                handlerAdder.Body.Instructions.Add(Instruction.Create(OpCodes.Castclass, handler));
                handlerAdder.Body.Instructions.Add(Instruction.Create(OpCodes.Stloc_2));
                handlerAdder.Body.Instructions.Add(Instruction.Create(OpCodes.Ldarg_0));
                handlerAdder.Body.Instructions.Add(Instruction.Create(OpCodes.Ldflda, newEventField));
                handlerAdder.Body.Instructions.Add(Instruction.Create(OpCodes.Ldloc_2));
                handlerAdder.Body.Instructions.Add(Instruction.Create(OpCodes.Ldloc_1));
                handlerAdder.Body.Instructions.Add(Instruction.Create(OpCodes.Call, genericCompareExchangeMethod));
                handlerAdder.Body.Instructions.Add(Instruction.Create(OpCodes.Stloc_0));
                handlerAdder.Body.Instructions.Add(Instruction.Create(OpCodes.Ldloc_0));
                handlerAdder.Body.Instructions.Add(Instruction.Create(OpCodes.Ldloc_1));
                handlerAdder.Body.Instructions.Add(Instruction.Create(OpCodes.Bne_Un_S, loopStartLabel));    
            }
            handlerAdder.Body.Instructions.Add(Instruction.Create(OpCodes.Ret));
            handlerAdder.Body.OptimizeBranches();
            handlerAdder.Body.OptimizeMacros();
            type.Methods.Add(handlerAdder);

            // Create remove function
            var handlerRemover = new MethodDefUser($"remove_{name}", MethodSig.CreateInstance(CoreLibrary.ManifestModule.CorLibTypes.Void, handler.ToTypeSig()));
            handlerRemover.Attributes = methodAttributes;
            // Add parameter - event handler to remove
            handlerRemover.ParamDefs.Add(new ParamDefUser("args", 1));
            // We need 3 temporary variables
            handlerRemover.Body = new CilBody() { KeepOldMaxStack = true };
            handlerRemover.Body.Variables.Add(new Local(handler.ToTypeSig()));
            handlerRemover.Body.Variables.Add(new Local(handler.ToTypeSig()));
            handlerRemover.Body.Variables.Add(new Local(handler.ToTypeSig()));
            handlerRemover.Body.InitLocals = true;
            handlerRemover.Body.MaxStack = 3;
            // Implement method
            handlerRemover.Body.Instructions.Add(Instruction.Create(OpCodes.Ldarg_0));
            handlerRemover.Body.Instructions.Add(Instruction.Create(OpCodes.Ldfld, newEventField));
            handlerRemover.Body.Instructions.Add(Instruction.Create(OpCodes.Stloc_0));
            {
                // Loop start
                var loopStartLabel = Instruction.Create(OpCodes.Nop);
                handlerRemover.Body.Instructions.Add(loopStartLabel);
                handlerRemover.Body.Instructions.Add(Instruction.Create(OpCodes.Ldloc_0));
                handlerRemover.Body.Instructions.Add(Instruction.Create(OpCodes.Stloc_1));
                handlerRemover.Body.Instructions.Add(Instruction.Create(OpCodes.Ldloc_1));
                handlerRemover.Body.Instructions.Add(Instruction.Create(OpCodes.Ldarg_1));
                handlerRemover.Body.Instructions.Add(Instruction.Create(OpCodes.Call, delegateRemoveMethod));
                handlerRemover.Body.Instructions.Add(Instruction.Create(OpCodes.Castclass, handler));
                handlerRemover.Body.Instructions.Add(Instruction.Create(OpCodes.Stloc_2));
                handlerRemover.Body.Instructions.Add(Instruction.Create(OpCodes.Ldarg_0));
                handlerRemover.Body.Instructions.Add(Instruction.Create(OpCodes.Ldflda, newEventField));
                handlerRemover.Body.Instructions.Add(Instruction.Create(OpCodes.Ldloc_2));
                handlerRemover.Body.Instructions.Add(Instruction.Create(OpCodes.Ldloc_1));
                handlerRemover.Body.Instructions.Add(Instruction.Create(OpCodes.Call, genericCompareExchangeMethod));
                handlerRemover.Body.Instructions.Add(Instruction.Create(OpCodes.Stloc_0));
                handlerRemover.Body.Instructions.Add(Instruction.Create(OpCodes.Ldloc_0));
                handlerRemover.Body.Instructions.Add(Instruction.Create(OpCodes.Ldloc_1));
                handlerRemover.Body.Instructions.Add(Instruction.Create(OpCodes.Bne_Un_S, loopStartLabel));             
            }
            handlerRemover.Body.Instructions.Add(Instruction.Create(OpCodes.Ret));
            handlerRemover.Body.OptimizeBranches();
            handlerRemover.Body.OptimizeMacros();

            newEvent.AddMethod = handlerAdder;
            newEvent.RemoveMethod = handlerRemover;
            type.Methods.Add(handlerRemover);
            type.Events.Add(newEvent);

            // Build event raiser for this event (so that other assemblies can also easily raise events)
            var eventRaiser = BuildEventRaiser(type, args, newEventField, raiseMethodName);

            return (newEventField, eventRaiser);
        }

        /// <summary>
        /// Implements safe event raising (same as when using ?. operator)
        /// </summary>
        /// <param name="type">Type with event definition</param>
        /// <param name="args">Type of event args</param>
        /// <param name="eventField">Event field</param>
        /// <param name="raiserMethodName">Raiser method for the event</param>
        /// <returns>Raiser method</returns>
        public MethodDef BuildEventRaiser(TypeDef type, ITypeDefOrRef args, FieldDef eventField, string raiserMethodName)
        {
            var eventRaiser = new MethodDefUser(raiserMethodName, MethodSig.CreateInstance(CoreLibrary.ManifestModule.CorLibTypes.Void, args.ToTypeSig()));
            eventRaiser.Attributes = MethodAttributes.Public;
            eventRaiser.ParamDefs.Add(new ParamDefUser("args", 0));
            type.Methods.Add(eventRaiser);
            
            // Method labels
            var labeledEventRaisePrepareInstruction = Instruction.Create(OpCodes.Nop);

            #region METHOD_IMPLEMENTATION
            eventRaiser.Body = new CilBody();
            // Load event field
            eventRaiser.Body.Instructions.Add(Instruction.Create(OpCodes.Ldarg_0));
            eventRaiser.Body.Instructions.Add(Instruction.Create(OpCodes.Ldfld, eventField));
            // Check that we have registered event handlers
            eventRaiser.Body.Instructions.Add(Instruction.Create(OpCodes.Dup));
            eventRaiser.Body.Instructions.Add(Instruction.Create(OpCodes.Brtrue_S, labeledEventRaisePrepareInstruction));
            {
                // If not => clear stack an exit
                eventRaiser.Body.Instructions.Add(Instruction.Create(OpCodes.Pop));
                eventRaiser.Body.Instructions.Add(Instruction.Create(OpCodes.Ret));
            }
            {
                // If yes => proceed with event raising
                eventRaiser.Body.Instructions.Add(labeledEventRaisePrepareInstruction);
                // Sender is null, EventArgs are passed as method parameter
                eventRaiser.Body.Instructions.Add(Instruction.Create(OpCodes.Ldnull));
                eventRaiser.Body.Instructions.Add(Instruction.Create(OpCodes.Ldarg_1));

                // Get event invoker
                var corlibTypes = eventRaiser.Module.CorLibTypes;
                var eventTypeRef = (TypeSpec)eventField.FieldType.ToTypeDefOrRef();
                var eventTypeDef = Context.ResolveType(eventTypeRef);
                var eventInvokerDef = eventTypeDef.Methods.Single(m => m.Name == "Invoke" && m.Parameters.Count == 3);
                var eventInvokerSpec = Context.MakeCoreLibMethodReference("Invoke", MethodSig.CreateInstance(
                    corlibTypes.Void, corlibTypes.Object, new GenericVar(0)), eventTypeRef);

                eventRaiser.Body.Instructions.Add(Instruction.Create(OpCodes.Callvirt, eventInvokerSpec));
            }
            eventRaiser.Body.Instructions.Add(Instruction.Create(OpCodes.Ret));
            eventRaiser.Body.OptimizeBranches();
            eventRaiser.Body.OptimizeMacros();
            #endregion

            return eventRaiser;
        }
    }
}
