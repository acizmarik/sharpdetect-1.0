/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Common;
using SharpDetect.Injector.Runtime.CodeConstructs;
using SharpDetect.Injector.Utilities;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace SharpDetect.Injector.Runtime
{
    partial class InjectedRuntime
    {
        public TypeDef EventDispatcher { get; private set; }
        internal HashSet<MethodDef> InjectedMethods { get; private set; }
        internal CodeInspector CodeInspector { get; private set; }    

        public InjectedRuntime(CodeInspector inspector)
        {
            CodeInspector = inspector;

            Definitions.SetupDescriptors(CodeInspector.CoreLibrary);
            CreateEventDispatcher();
            CreateEvents();
            Definitions.SetupFromInstrumented(CodeInspector.CoreLibrary);
        }

        private void CreateEventDispatcher()
        {
            // Create singleton event dispatcher class
            var eventDispatcherType = new TypeDefUser(
                @namespace: Definitions.FrameworkNamespace,
                name: Definitions.EventDispatcherTypeName,
                baseType: CodeInspector.CoreLibrary.ManifestModule.CorLibTypes.Object.ToTypeDefOrRef());
            eventDispatcherType.Attributes = TypeAttributes.Public | TypeAttributes.Class;
            // Add instance field
            var instanceFieldDefinition = new FieldDefUser("instance", new FieldSig(eventDispatcherType.ToTypeSig()));
            instanceFieldDefinition.Attributes = FieldAttributes.Static | FieldAttributes.Private;
            eventDispatcherType.Fields.Add(instanceFieldDefinition);

            // Create class constructor
            var cctor = new MethodDefUser(".cctor", MethodSig.CreateStatic(CodeInspector.CoreLibrary.ManifestModule.CorLibTypes.Void));
            cctor.Attributes = MethodAttributes.Private | MethodAttributes.HideBySig | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName | MethodAttributes.Static;
            cctor.Body = new CilBody();
            cctor.Body.Instructions.Add(Instruction.Create(OpCodes.Ldnull));
            cctor.Body.Instructions.Add(Instruction.Create(OpCodes.Stsfld, instanceFieldDefinition));
            cctor.Body.Instructions.Add(Instruction.Create(OpCodes.Ret));
            eventDispatcherType.Methods.Add(cctor);

            var objectCtor = CodeInspector.CoreLibrary.ManifestModule.CorLibTypes.Object.ToTypeDefOrRef().ResolveTypeDef().Methods.Single(
                m => m.Name == ".ctor" && m.Parameters.Count == 1);
            // Create private constructor
            var ctor = new MethodDefUser(".ctor", MethodSig.CreateInstance(CodeInspector.CoreLibrary.ManifestModule.CorLibTypes.Void));
            ctor.Attributes = MethodAttributes.Private | MethodAttributes.HideBySig | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName;
            ctor.Body = new CilBody();
            ctor.Body.Instructions.Add(Instruction.Create(OpCodes.Ldarg_0));
            ctor.Body.Instructions.Add(Instruction.Create(OpCodes.Call, objectCtor));
            ctor.Body.Instructions.Add(Instruction.Create(OpCodes.Ret));
            eventDispatcherType.Methods.Add(ctor);
            // Create instance getter
            var getter = new MethodDefUser("get_Instance", MethodSig.CreateStatic(eventDispatcherType.ToTypeSig()));
            getter.Attributes = MethodAttributes.Public | MethodAttributes.Static | MethodAttributes.HideBySig;
            // Add variables to instance getter
            getter.Body = new CilBody();
            getter.Body.Variables.Add(new Local(CodeInspector.CoreLibrary.ManifestModule.CorLibTypes.Boolean));
            getter.Body.Variables.Add(new Local(eventDispatcherType.ToTypeSig()));
            getter.Body.InitLocals = true;
            // Implement instance getter
            getter.Body.Instructions.Add(Instruction.Create(OpCodes.Ldsfld, instanceFieldDefinition));
            getter.Body.Instructions.Add(Instruction.Create(OpCodes.Ldnull));
            getter.Body.Instructions.Add(Instruction.Create(OpCodes.Ceq));
            getter.Body.Instructions.Add(Instruction.Create(OpCodes.Stloc_0));
            getter.Body.Instructions.Add(Instruction.Create(OpCodes.Ldloc_0));

            var loadInstanceLabeledInstruction = Instruction.Create(OpCodes.Ldsfld, instanceFieldDefinition);
            {
                // If object was not constructed create it
                getter.Body.Instructions.Add(Instruction.Create(OpCodes.Brfalse_S, loadInstanceLabeledInstruction));
                getter.Body.Instructions.Add(Instruction.Create(OpCodes.Newobj, ctor));
                getter.Body.Instructions.Add(Instruction.Create(OpCodes.Stsfld, instanceFieldDefinition));
            }
            // Whether it was created previously or now we return the instance
            getter.Body.Instructions.Add(loadInstanceLabeledInstruction);
            getter.Body.Instructions.Add(Instruction.Create(OpCodes.Ret));
            getter.Body.OptimizeBranches();
            getter.Body.OptimizeMacros();
            eventDispatcherType.Methods.Add(getter);
            EventDispatcher = eventDispatcherType;
            CodeInspector.Context.RegisterNewType(EventDispatcher);
            CodeInspector.CoreLibrary.ManifestModule.Types.Add(EventDispatcher);
            Log.Debug("Injected {EventDispatcherBackend} into BCL {CoreLib}", EventDispatcher, CodeInspector.CoreLibrary);
        }

        private void CreateEvents()
        {
            var eventBuilder = new EventBuilder() { CoreLibrary = CodeInspector.CoreLibrary, Context = CodeInspector.Context };
            var eventHandlerType = CodeInspector.CoreLibrary.ManifestModule.Types.Single(t => t.Name == "EventHandler`1");

            foreach (var eventDescriptor in Definitions.RegisteredEvents)
            {
                var eventName = eventDescriptor.Name;
                var eventArgs = eventDescriptor.ArgsType;
                var eventRaiser = eventDescriptor.RaiserMethodName;

                var eventHandler = new ClassSig(eventHandlerType).MakeGenericInstanceType(eventArgs.ToTypeSig());
                eventBuilder.BuildEvent(eventName, EventDispatcher, eventHandler, eventArgs, eventRaiser, CodeInspector.CoreLibrary);
                Log.Debug("Injected event {Event} with raiser method {Raiser} into type {EventDispatcher}", eventDescriptor.Name, eventDescriptor.RaiserMethodName, EventDispatcher.Name);
            }
        }
    }
}
