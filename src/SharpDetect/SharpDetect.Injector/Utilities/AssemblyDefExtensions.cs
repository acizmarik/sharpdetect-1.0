/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using dnlib.DotNet;
using dnlib.DotNet.Writer;
using dnlib.PE;
using Serilog;
using SharpDetect.Core;
using System;
using System.Collections.Generic;
using System.Text;
using Machine = dnlib.PE.Machine;

namespace SharpDetect.Injector.Utilities
{
    public static class AssemblyDefExtensions
    {
        public static void WriteAssembly(this AssemblyDef assembly, string path)
        {
            if (assembly.ManifestModule.IsILOnly)
                assembly.WriteManagedAssembly(path);
            else
                assembly.WriteNativeAssembly(path);
        }

        private static void WriteNativeAssembly(this AssemblyDef assembly, string path)
        {
            assembly.StripNativeCode();
            assembly.WriteManagedAssembly(path);
        }

        private static void WriteManagedAssembly(this AssemblyDef assembly, string path)
        {
            var writeOptions = new ModuleWriterOptions(assembly.ManifestModule)
            {
                WritePdb = true,
                PdbOptions = PdbWriterOptions.Deterministic,
            };
            assembly.Write(path, writeOptions);
        }

        private static void StripNativeCode(this AssemblyDef assembly)
        {
            // If assembly contains native code => strip it (this is a work-around)
            // See issue: https://github.com/0xd4d/dnlib/issues/305

            // If assembly is ILOnly => there is nothing to do
            if (assembly.ManifestModule.IsILOnly)
                return;

            // Strip native code
            assembly.ManifestModule.IsILOnly = true;
            assembly.ManifestModule.Cor20HeaderFlags &= ~dnlib.DotNet.MD.ComImageFlags.ILLibrary;
            // Set assembly as managed-only
            assembly.ManifestModule.Cor20HeaderFlags |= dnlib.DotNet.MD.ComImageFlags.ILOnly;

            // CPU architecture might be set to native
            // This needs to be changed as well
            if (assembly.IsProcessorArchitectureNone)
            {
                var nativeMachine = ((ModuleDefMD)assembly.ManifestModule).Machine;
                if (nativeMachine == Machine.Unknown)
                    throw new DynamicAnalysisException($"Unknown target CPU machine {nativeMachine}");

                // See: https://github.com/0xd4d/dnlib/blob/1f703ade894977bb7ea32a7c132c35e17a156d1e/src/PE/Machine.cs#L74
                var managedMachine = Machine.Unknown;
                if (nativeMachine.IsAMD64())
                    managedMachine = Machine.AMD64;
                else if (nativeMachine.IsARM64())
                    managedMachine = Machine.ARM64;
                else if (nativeMachine.IsARMNT())
                    managedMachine = Machine.ARMNT;
                else if (nativeMachine.IsI386())
                    managedMachine = Machine.I386;
                else
                    throw new DynamicAnalysisException($"Unknown native target CPU machine {nativeMachine}");

                ((ModuleDefMD)assembly.ManifestModule).Machine = managedMachine;
            }
        }
    }
}
