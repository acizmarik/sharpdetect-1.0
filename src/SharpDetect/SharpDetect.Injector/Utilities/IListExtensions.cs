/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpDetect.Injector.Utilities
{
    static class IListExtensions
    {
        public static IEnumerable<T> ReverseList<T>(this IList<T> items)
        {
            for (int i = items.Count - 1; i >= 0; --i)
                yield return items[i];
        }

        public static T LastList<T>(this IList<T> items)
        {
            return items[items.Count - 1];
        }
    }
}
