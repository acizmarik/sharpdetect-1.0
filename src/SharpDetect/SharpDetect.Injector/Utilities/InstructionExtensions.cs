/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using dnlib.DotNet;
using dnlib.DotNet.Emit;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.Injector.Utilities
{
    static class InstructionExtensions
    {
        /// <summary>
        /// Return type information for pop effect of instruction
        /// </summary>
        /// <param name="method">Method which contains instruction</param>
        /// <param name="instruction">Instruction</param>
        /// <returns>List of types which will be poped from stack</returns>
        public static List<ITypeDefOrRef> GetInstructionPopType(this Instruction instruction, MethodDef method)
        {
            var module = method.Module;
            switch (instruction.OpCode.StackBehaviourPop)
            {
                case StackBehaviour.Pop0:
                    return null;
                case StackBehaviour.Pop1:
                    return new List<ITypeDefOrRef> { module.CorLibTypes.Object.TypeDefOrRef };
                case StackBehaviour.Popi:
                    return new List<ITypeDefOrRef> { module.CorLibTypes.Int32.TypeDefOrRef };
                case StackBehaviour.Pop1_pop1:
                    return new List<ITypeDefOrRef> { module.CorLibTypes.Object.TypeDefOrRef, module.CorLibTypes.Object.TypeDefOrRef };
                case StackBehaviour.Popi_pop1:
                    return new List<ITypeDefOrRef> { module.CorLibTypes.Int32.TypeDefOrRef, module.CorLibTypes.Object.TypeDefOrRef };
                case StackBehaviour.Popi_popi:
                    return new List<ITypeDefOrRef> { module.CorLibTypes.Int32.TypeDefOrRef, module.CorLibTypes.Int32.TypeDefOrRef };
                case StackBehaviour.Popi_popi_popi:
                    return new List<ITypeDefOrRef> { module.CorLibTypes.Int32.TypeDefOrRef, module.CorLibTypes.Int32.TypeDefOrRef, module.CorLibTypes.Int32.TypeDefOrRef };
                case StackBehaviour.Popi_popi8:
                    return new List<ITypeDefOrRef> { module.CorLibTypes.Int32.TypeDefOrRef, module.CorLibTypes.Int64.TypeDefOrRef };
                case StackBehaviour.Popi_popr8:
                    return new List<ITypeDefOrRef> { module.CorLibTypes.Int32.TypeDefOrRef, module.CorLibTypes.Double.TypeDefOrRef };
                case StackBehaviour.Popi_popr4:
                    return new List<ITypeDefOrRef> { module.CorLibTypes.Int32.TypeDefOrRef, module.CorLibTypes.Single.TypeDefOrRef };
                case StackBehaviour.Popref:
                    return new List<ITypeDefOrRef> { module.CorLibTypes.Object.TypeDefOrRef };
                case StackBehaviour.Popref_pop1:
                    return new List<ITypeDefOrRef> { module.CorLibTypes.Object.TypeDefOrRef, module.CorLibTypes.Object.TypeDefOrRef };
                case StackBehaviour.Popref_popi:
                    return new List<ITypeDefOrRef> { module.CorLibTypes.Object.TypeDefOrRef, module.CorLibTypes.Int32.TypeDefOrRef };
                case StackBehaviour.Popref_popi_pop1:
                    return new List<ITypeDefOrRef> { module.CorLibTypes.Object.TypeDefOrRef, module.CorLibTypes.Int32.TypeDefOrRef, module.CorLibTypes.Object.TypeDefOrRef };
                case StackBehaviour.Popref_popi_popi:
                    return new List<ITypeDefOrRef> { module.CorLibTypes.Object.TypeDefOrRef, module.CorLibTypes.Int32.TypeDefOrRef, module.CorLibTypes.Int32.TypeDefOrRef };
                case StackBehaviour.Popref_popi_popi8:
                    return new List<ITypeDefOrRef> { module.CorLibTypes.Object.TypeDefOrRef, module.CorLibTypes.Int32.TypeDefOrRef, module.CorLibTypes.Int64.TypeDefOrRef };
                case StackBehaviour.Popref_popi_popr4:
                    return new List<ITypeDefOrRef> { module.CorLibTypes.Object.TypeDefOrRef, module.CorLibTypes.Int32.TypeDefOrRef, module.CorLibTypes.Single.TypeDefOrRef };
                case StackBehaviour.Popref_popi_popr8:
                    return new List<ITypeDefOrRef> { module.CorLibTypes.Object.TypeDefOrRef, module.CorLibTypes.Int32.TypeDefOrRef, module.CorLibTypes.Double.TypeDefOrRef };
                case StackBehaviour.Popref_popi_popref:
                    return new List<ITypeDefOrRef> { module.CorLibTypes.Object.TypeDefOrRef, module.CorLibTypes.Int32.TypeDefOrRef, module.CorLibTypes.Object.TypeDefOrRef };
                default:
                    throw new NotImplementedException($"Invalid StackBehaviour found {instruction.OpCode.StackBehaviourPop}");
            }
        }
    }
}
