/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using dnlib.DotNet;
using dnlib.DotNet.Emit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpDetect.Injector.Utilities
{
    public static class MethodDefExtensions
    {
        private static bool HasPrefixInstruction(MethodDef method, int instructionIndex)
            => instructionIndex != 0 && method.Body.Instructions[instructionIndex - 1].OpCode.OpCodeType == OpCodeType.Prefix;

        private static bool IsStartingTry(MethodDef method, Instruction instruction, int instructionIndex, out ExceptionHandler handler)
        {
            handler = method.Body.ExceptionHandlers.FirstOrDefault(h => h.TryStart == instruction);
            return handler != null;
        }

        private static bool IsEndingTry(MethodDef method, Instruction instruction, int instructionIndex, out ExceptionHandler handler)
        {
            handler = method.Body.ExceptionHandlers.FirstOrDefault(h => h.TryEnd == instruction);
            return handler != null;
        }

        private static bool IsStartingFilter(MethodDef method, Instruction instruction, int instructionIndex, out ExceptionHandler handler)
        {
            handler = method.Body.ExceptionHandlers.FirstOrDefault(h => h.FilterStart == instruction);
            return handler != null;
        }

        private static bool IsStartingHandler(MethodDef method, Instruction instruction, int instructionIndex, out ExceptionHandler handler)
        {
            handler = method.Body.ExceptionHandlers.FirstOrDefault(h => h.HandlerStart == instruction);
            return handler != null;
        }

        private static bool IsEndingHandler(MethodDef method, Instruction instruction, int instructionIndex, out ExceptionHandler handler)
        {
            handler = method.Body.ExceptionHandlers.FirstOrDefault(h => h.HandlerEnd == instruction);
            return handler != null;
        }

        private static void ExtendTryStart(MethodDef method, Instruction oldTryStart, Instruction newTryStart, ExceptionHandler handler)
            => handler.TryStart = newTryStart;

        private static void ExtendFilterStart(MethodDef method, Instruction oldFilterStart, Instruction newFilterStart, ExceptionHandler handler)
        {
            // Shift filter start
            handler.FilterStart = newFilterStart;

            // We have to shift also end of try block
            handler.TryEnd = newFilterStart;
        }

        private static void ExtendHandlerStart(MethodDef method, Instruction oldHandlerStart, Instruction newHandlerStart, ExceptionHandler handler)
        {
            // Shift handler start
            handler.HandlerStart = newHandlerStart;

            // Check if we have to shift also end of try block
            if (handler.TryEnd == oldHandlerStart)
                handler.TryEnd = newHandlerStart;
        }

        private static void ExtendHandlerEnd(MethodDef method, Instruction oldHandlerEnd, Instruction newHandlerEnd, ExceptionHandler handler)
        {
            // If handler end is valid until method end we do not need to extend anything
            if (handler.HandlerEnd == null)
                return;

            var index = method.Body.Instructions.IndexOf(newHandlerEnd);
            handler.HandlerEnd = method.Body.Instructions[index];

            // Shift handler leave instruction
            foreach (var leaveInstruction in method.Body.Instructions.Where(i => IsLeaveInstructionForBlock(i, oldHandlerEnd)))
                leaveInstruction.Operand = newHandlerEnd;
        }

        private static bool IsLeaveInstructionForBlock(Instruction instruction, Instruction oldLabel)
        {
            if (instruction.OpCode.Code != Code.Leave && instruction.OpCode.Code != Code.Leave_S)
                return false;
            if (instruction.Operand != oldLabel)
                return false;

            return true;
        }

        private static int GetLabelBeforeOrSelf(MethodDef method, int instructionIndex)
        {
            if (HasPrefixInstruction(method, instructionIndex))
            {
                var currentInstructionIndex = instructionIndex - 1;
                while (method.Body.Instructions[currentInstructionIndex - 1].OpCode.OpCodeType == OpCodeType.Prefix)
                    --currentInstructionIndex;

                return currentInstructionIndex;
            }

            return instructionIndex;
        }

        private static void RedirectBranchTargets(MethodDef method, Instruction oldTarget, Instruction newTarget)
        {
            for (var instructionIndex = 0; instructionIndex < method.Body.Instructions.Count; instructionIndex++)
            {
                var currentInstruction = method.Body.Instructions[instructionIndex];
                // Check if the instruction is branching
                if (!currentInstruction.IsConditionalBranch() && !currentInstruction.IsBr() && currentInstruction.OpCode.Code != Code.Jmp)
                    continue;

                var target = (Instruction)currentInstruction.Operand;
                // Check that target is correct
                if (target != oldTarget)
                    continue;

                // Fix branch target
                currentInstruction.Operand = newTarget;
            }
        }

        private static void FixHandlers(MethodDef method, Instruction target, int targetIndex, Instruction firstInjectedInstruction)
        {
            // Make sure that we did not break any try block
            if (IsStartingTry(method, target, targetIndex, out var handlerBlock))
                ExtendTryStart(method, target, firstInjectedInstruction, handlerBlock);
            // Make sure that we did not break any filter block
            else if (IsStartingFilter(method, target, targetIndex, out handlerBlock))
                ExtendFilterStart(method, target, firstInjectedInstruction, handlerBlock);
            // Make sure that we did not break any handler block
            else if (IsStartingHandler(method, target, targetIndex, out handlerBlock))
                ExtendHandlerStart(method, target, firstInjectedInstruction, handlerBlock);
            else if (IsEndingHandler(method, target, targetIndex, out handlerBlock))
                ExtendHandlerEnd(method, target, firstInjectedInstruction, handlerBlock);
        }

        public static void InjectBefore(this MethodDef method, Instruction target, IEnumerable<Instruction> instructions)
        {
            // If instructions are empty then there is nothing to do
            if (instructions.FirstOrDefault() == null)
                return;

            // Simplify branch instructions so that we want break short branches
            method.Body.SimplifyBranches();

            var index = method.Body.Instructions.IndexOf(target);
            index = GetLabelBeforeOrSelf(method, index);

            foreach (var instruction in instructions)
            {
                method.Body.Instructions.Insert(index, instruction);
                ++index;
            }

            // Make sure that we fix branches, handlers
            RedirectBranchTargets(method, target, instructions.First());
            FixHandlers(method, target, index, instructions.First());

            method.Body.OptimizeBranches();
            method.Body.OptimizeMacros();
        }

        public static void InjectAfter(this MethodDef method, Instruction target, IEnumerable<Instruction> instructions)
        {
            // If instructions are empty then there is nothing to do
            if (instructions.FirstOrDefault() == null)
                return;

            // Simplify branch instructions so that we want break short branches
            method.Body.SimplifyBranches();

            var index = method.Body.Instructions.IndexOf(target);
            var getValidBeforeTarget = target;

            var injectingIndex = index;
            foreach (var instruction in instructions)
            {
                method.Body.Instructions.Insert(injectingIndex + 1, instruction);
                ++injectingIndex;
            }

            method.Body.OptimizeBranches();
            method.Body.OptimizeMacros();
        }
    }
}
