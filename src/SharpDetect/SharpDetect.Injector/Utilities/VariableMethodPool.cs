/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using dnlib.DotNet;
using dnlib.DotNet.Emit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpDetect.Injector.Utilities
{
    sealed class VariableMethodPool
    {
        private Dictionary<MethodDef, HashSet<Local>> variablesPool;
        private TypeEqualityComparer typeComparer = TypeEqualityComparer.Instance;
        
        public Local GetVariable(MethodDef method, TypeSig variableSig)
        {
            // Types might be assossiated with modifiers, for example, volatile fields
            // Make sure to create variable for the correct type
            if (variableSig.IsModifier)
                return GetVariable(method, variableSig.Next);

            if (!variablesPool.ContainsKey(method))
                variablesPool.Add(method, new HashSet<Local>());

            Local variable = null;
            foreach (var pooledVariable in variablesPool[method])
            {
                if (typeComparer.Equals(pooledVariable.Type, variableSig))
                {
                    variable = pooledVariable;
                    break;
                }
            }

            // We have sufficient variable in the pool
            if (variable != null)
            {
                variablesPool[method].Remove(variable);
                return variable;
            }
            // Otherwise we generate a new variable
            variable = new Local(variableSig);
            method.Body.Variables.Add(variable);
            return variable;
        }

        public void StoreVariable(MethodDef method, Local variable)
        {
            if (variable == null)
                return;

            if (!variablesPool.ContainsKey(method))
                variablesPool.Add(method, new HashSet<Local>());

            variablesPool[method].Add(variable);
        }

        #region SINGLETON_IMPLEMENTATION
        private static VariableMethodPool instance = null;
        public static VariableMethodPool Instance
        {
            get
            {
                if (instance == null)
                    instance = new VariableMethodPool();
                return instance;
            }
        }

        private VariableMethodPool()
        {
            variablesPool = new Dictionary<MethodDef, HashSet<Local>>();
        }
        #endregion
    }
}
