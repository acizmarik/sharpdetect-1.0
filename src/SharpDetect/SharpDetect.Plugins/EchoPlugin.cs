/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Plugins;
using SharpDetect.Core.Utilities;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SharpDetect.Core.EchoPlugin
{
    public class EchoPlugin : BasePlugin
    {
        public override string PluginName { get { return "EchoPlugin"; } }

        public override void AnalysisStart(MethodDescriptor method)
        {
            Log.Logger = EventDispatcherFrontend.LoggerConfig;
            Log.Information("# Analysis begins at {Method} #", method);

            base.AnalysisStart(method);
        }

        public override void AnalysisEnd(MethodDescriptor method)
        {
            Log.Information("# Analysis ends at {Method} #", method);

            base.AnalysisEnd(method);
        }

        public override void ArrayCreated(int threadId, MethodDescriptor method, Array array)
        {
            Log.Information("[{Thread}] Array: {Array} created in method {Method}", 
                threadId, array.ToString(), method);

            base.ArrayCreated(threadId, method, array);
        }

        public override void ArrayElementRead(int threadId, MethodDescriptor method, Array array, int index)
        {
            Log.Information("[{Thread}] Array: {Array} read element with index {Index} in method {Method}.", 
                threadId, array.ToString(), index, method);

            base.ArrayElementRead(threadId, method, array, index);
        }

        public override void ArrayElementWritten(int threadId, MethodDescriptor method, Array array, int index, object value)
        {
            Log.Information("[{ThreadId}] Array: {Array} wrote value {Value} on index {Index} in method {Method}.",
                threadId, array.ToString(), value, index, method);

            base.ArrayElementWritten(threadId, method, array, index, value);
        }

        public override void ClassConstructed(int threadId, TypeDescriptor type)
        {
            Log.Information("[{ThreadId}] Class: {Type} has been constructed.",
                threadId, type);

            base.ClassConstructed(threadId, type);
        }

        public override void FieldRead(int threadId, object instance, FieldDescriptor field)
        {
            Log.Information("[{ThreadId}] Field: {Field} was read from.",
                threadId, field);

            base.FieldRead(threadId, instance, field);
        }

        public override void FieldWritten(int threadId, object instance, FieldDescriptor field, object value)
        {
            Log.Information("[{ThreadId}] Field: {Field} was written with value {Value}.",
                threadId, field, value);

            base.FieldWritten(threadId, instance, field, value);
        }

        public override void LockAcquireAttempted(int threadId, MethodDescriptor method, object lockObj, (int, object)[] parameters)
        {
            Log.Information("[{ThreadId}] Lock: {@Lock} attempt to acquire in method: {Method}.",
                threadId, lockObj, method);

            base.LockAcquireAttempted(threadId, method, lockObj, parameters);
        }

        public override void LockAcquireReturned(int threadId, MethodDescriptor method, object lockObj, bool result, (int, object)[] parameters)
        {
            if (result)
                Log.Information("[{ThreadId}] Lock: {@Lock} acquired in method: {Method}.",
                    threadId, lockObj, method);
            else
                Log.Information("[{threadId}] Lock: {@Lock} was NOT acquired in method: {Method}.",
                    threadId, lockObj, method);

            base.LockAcquireReturned(threadId, method, lockObj, result, parameters);
        }

        public override void LockReleased(int threadId, MethodDescriptor method, object lockObj)
        {
            Log.Information("[{ThreadId}] Lock: {@Lock} was released in method: {Method}.",
                threadId, lockObj, method);

            base.LockReleased(threadId, method, lockObj);
        }

        public override void ObjectWaitAttempted(int threadId, MethodDescriptor method, object waitObj, (int, object)[] parameters)
        {
            Log.Information("[{ThreadId}] Wait: {@Obj} was called in method: {Method}.",
                threadId, waitObj, method);

            base.ObjectWaitAttempted(threadId, method, waitObj, parameters);
        }

        public override void ObjectWaitReturned(int threadId, MethodDescriptor method, object waitObj, bool result, (int, object)[] parameters)
        {
            var sb = new StringBuilder();
            sb.Append("[{ThreadId}] Wait: {@Obj} was ");
            if (result)
                sb.Append("successful. ");
            else
                sb.Append("unsuccessful. ");
            sb.Append("Called in method: {Method}.");

            Log.Information(sb.ToString(), threadId, waitObj, method);

            base.ObjectWaitReturned(threadId, method, waitObj, result, parameters);
        }

        public override void ObjectPulsedOne(int threadId, MethodDescriptor method, object pulseObj)
        {
            Log.Information("[{ThreadId}] Pulse: {@Obj} pulsed one.", threadId, pulseObj);

            base.ObjectPulsedOne(threadId, method, pulseObj);
        }

        public override void ObjectPulsedAll(int threadId, MethodDescriptor method, object pulseObj)
        {
            Log.Information("[{ThreadId}] Pulse: {@Obj} pulsed all.", threadId, pulseObj);

            base.ObjectPulsedAll(threadId, method, pulseObj);
        }

        public override void MethodCalled(int threadId, (int, object)[] parameters, MethodDescriptor method)
        {
            if (parameters != null)
            {
                Log.Information("[{ThreadId}] Method: {Method} called with parameters {Params}.",
                    threadId, method, parameters);
            }
            else
                Log.Information("[{ThreadId}] Method: {Method} called with no parameters.",
                    threadId, method);

            base.MethodCalled(threadId, parameters, method);
        }

        public override void MethodReturned(int threadId, object returnValue, bool isValid, (int, object)[] parameters, MethodDescriptor method)
        {
            if (parameters != null && isValid)
                Log.Information("[{ThreadId}] Method: {Method} called with parameters: {Params} returned {Return}", threadId, method, parameters, returnValue);
            else if (parameters != null && !isValid)
                Log.Information("[{ThreadId}] Method: {Method} called with parameters: {Params} returned", threadId, method, parameters);
            else if (parameters == null && isValid)
                Log.Information("[{ThreadId}] Method: {Method} returned {Return}", threadId, method, returnValue);
            else if (parameters == null && !isValid)
                Log.Information("[{ThreadId}] Method: {Method} returned", threadId, method);

            base.MethodReturned(threadId, returnValue, isValid, parameters, method);
        }

        public override void ObjectCreated(int threadId, object obj)
        {
            Log.Information("[{ThreadId}] Object {Obj} created",
                threadId, obj);

            base.ObjectCreated(threadId, obj);
        }

        public override void UserThreadCreated(int threadId, Thread newThread)
        {
            Log.Information("[{ThreadId}] Thread: with id={NewThreadId} was created.",
                threadId, newThread.ManagedThreadId);

            base.UserThreadCreated(threadId, newThread);
        }

        public override void UserThreadStarted(int threadId, Thread thread)
        {
            Log.Information("[{ThreadId}] Thread: with id={OtherThreadId} was started.",
                threadId, thread.ManagedThreadId);

            base.UserThreadStarted(threadId, thread);
        }

        public override void UserThreadJoined(int threadId, Thread thread)
        {
            Log.Information("[{ThreadId}] Thread: with id={OtherThreadId} was joined.",
                threadId, thread.ManagedThreadId);

            base.UserThreadJoined(threadId, thread);
        }
    }
}