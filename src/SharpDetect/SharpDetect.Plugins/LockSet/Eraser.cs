/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using SharpDetect.Core.Utilities;
using Serilog;
using SharpDetect.Core;
using SharpDetect.Common.Events;

namespace SharpDetect.Plugins.LockSet
{
    public class Eraser : BasePlugin
    {
        public override string PluginName { get; } = "Eraser";
        
        private ConditionalWeakTable<Array, Dictionary<int, Variable>> arrayElements;
        private ConditionalWeakTable<object, Dictionary<FieldDescriptor, Variable>> fieldsOnReferenceInstancedTypes;
        private Dictionary<FieldDescriptor, Variable> fieldsOnReferenceStaticTypes;
        private Dictionary<int, HashSet<object>> takenLocks;
        private readonly object collectionsLock = new object();

        public Eraser()
        {
            arrayElements = new ConditionalWeakTable<Array, Dictionary<int, Variable>>();
            fieldsOnReferenceInstancedTypes = new ConditionalWeakTable<object, Dictionary<FieldDescriptor, Variable>>();
            fieldsOnReferenceStaticTypes = new Dictionary<FieldDescriptor, Variable>();
            takenLocks = new Dictionary<int, HashSet<object>>();
        }

        public override void AnalysisStart(MethodDescriptor method)
        {
            Log.Logger = EventDispatcherFrontend.LoggerConfig;
        }

        /// <summary>
        /// Checks for possible data-races when reading an array element with given thread
        /// </summary>
        /// <param name="threadId">Thread performing the event</param>
        /// <param name="method">Method where was the event invoked</param>
        /// <param name="array">Array whose element is being read</param>
        /// <param name="index">Index in the array</param>
        public override void ArrayElementRead(int threadId, MethodDescriptor method, Array array, int index)
        {           
            lock (collectionsLock)
            {
                TryArrayElementAccess(threadId, array, index, (takenLocks) =>
                {
                    arrayElements.TryGetValue(array, out var tracked);
                    return tracked[index].TryUpdateRead(threadId, takenLocks);
                });
            }

            base.ArrayElementRead(threadId, method, array, index);
        }

        /// <summary>
        /// Checks for possible data-races when writing to an array element with given thread
        /// </summary>
        /// <param name="threadId">Thread performing the event</param>
        /// <param name="method">Method where was the event invoked</param>
        /// <param name="array">Array whose element is being written to</param>
        /// <param name="index">Index in the array</param>
        /// <param name="value">Value that is being written</param>
        public override void ArrayElementWritten(int threadId, MethodDescriptor method, Array array, int index, object value)
        {
            lock (collectionsLock)
            {
                TryArrayElementAccess(threadId, array, index, (takenLocks) =>
                {
                    arrayElements.TryGetValue(array, out var tracked);
                    return tracked[index].TryUpdateWrite(threadId, takenLocks);
                });
            }

            base.ArrayElementWritten(threadId, method, array, index, value);
        }

        /// <summary>
        /// Checks for possible data-races when reading field declared on a reference type with given thread
        /// </summary>
        /// <param name="threadId">Thread performing the event</param>
        /// <param name="instance">Instance which owns the field</param>
        /// <param name="field">Field which is being read</param>
        public override void FieldRead(int threadId, object instance, FieldDescriptor field)
        {
            // Skip readonly and threadstatic fields
            if (!field.FieldFlags.HasFlag(FieldFlags.Readonly) && !field.FieldFlags.HasFlag(FieldFlags.ThreadStatic))
            {
                lock (collectionsLock)
                {
                    // If field is static
                    if (instance == null)
                    {
                        TryAccessStaticField(threadId, field, (takenLocks)
                            => fieldsOnReferenceStaticTypes[field].TryUpdateRead(threadId, takenLocks));
                    }
                    // If field is instance
                    else
                    {
                        TryAccessReferenceTypeInstanceField(threadId, field, instance, (takenLocks) =>
                        {
                            fieldsOnReferenceInstancedTypes.TryGetValue(instance, out var tracked);
                            return tracked[field].TryUpdateRead(threadId, takenLocks);
                        });
                    }
                }
            }

            base.FieldRead(threadId, instance, field);
        }

        /// <summary>
        /// Checks for possible data-races when writing field declared on a reference type with given thread
        /// </summary>
        /// <param name="threadId">Thread performing the event</param>
        /// <param name="instance">Instance which owns the field</param>
        /// <param name="field">Field which is being written to</param>
        /// <param name="value">Value which is being written</param>
        public override void FieldWritten(int threadId, object instance, FieldDescriptor field, object value)
        {
            // Skip readonly and threadstatic fields
            if (!field.FieldFlags.HasFlag(FieldFlags.Readonly) && !field.FieldFlags.HasFlag(FieldFlags.ThreadStatic))
            {
                lock (collectionsLock)
                {
                    // If field is static
                    if (instance == null)
                    {
                        TryAccessStaticField(threadId, field, (takenLocks)
                            => fieldsOnReferenceStaticTypes[field].TryUpdateWrite(threadId, takenLocks));
                    }
                    // If field is instance
                    else
                    {
                        TryAccessReferenceTypeInstanceField(threadId, field, instance, (takenLocks) =>
                        {
                            fieldsOnReferenceInstancedTypes.TryGetValue(instance, out var tracked);
                            return tracked[field].TryUpdateWrite(threadId, takenLocks);
                        });
                    }
                }
            }

            base.FieldWritten(threadId, instance, field, value);
        }

        public override void LockAcquireReturned(int threadId, MethodDescriptor method, object lockObj, bool result, (int, object)[] parameters)
        {
            lock (collectionsLock)
            {
                if (!takenLocks.ContainsKey(threadId))
                    takenLocks.Add(threadId, new HashSet<object>());

                takenLocks[threadId].Add(lockObj);
            }

            base.LockAcquireReturned(threadId, method, lockObj, result, parameters);
        }

        public override void LockReleased(int threadId, MethodDescriptor method, object lockObj)
        {
            lock (collectionsLock)
            {
                takenLocks[threadId].Remove(lockObj);
            }

            base.LockReleased(threadId, method, lockObj);
        }

        private bool TryArrayElementAccess(int threadId, Array array, int index, Func<HashSet<object>, bool> validator)
        {
            var currentLocks = GetCurrentLocks(threadId);

            // Make sure we are tracking current array
            arrayElements.TryGetValue(array, out var tracked);
            if (tracked == null)
            {
                tracked = new Dictionary<int, Variable>();
                arrayElements.Add(array, tracked);
            }

            // Make sure we are tracking current array element
            arrayElements.TryGetValue(array, out var trackedArray);
            if (!trackedArray.ContainsKey(index))
                trackedArray.Add(index, new Variable(threadId));

            // Update access information for the current element
            var result = validator(currentLocks);
            if (!result)
                RaiseViolationFound($"detected data-race on array element {array}[{index}]");

            return result;
        }

        private bool TryAccessStaticField(int threadId, FieldDescriptor field, Func<HashSet<object>, bool> validator)
        {
            var currentLocks = GetCurrentLocks(threadId);

            // Make sure we are tracking current field
            fieldsOnReferenceStaticTypes.TryGetValue(field, out var tracked);
            if (tracked == null)
            {
                tracked = new Variable(threadId);
                fieldsOnReferenceStaticTypes.Add(field, tracked);
            }

            // Update access information
            var result = validator(currentLocks);
            if (!result)
                RaiseViolationFound($"detected data-race on a static field {field}");

            return result;
        }

        private bool TryAccessReferenceTypeInstanceField(int threadId, FieldDescriptor field, object instance, Func<HashSet<object>, bool> validator)
        {
            var currentLocks = GetCurrentLocks(threadId);

            // Make sure we are tracking current instance
            fieldsOnReferenceInstancedTypes.TryGetValue(instance, out var tracked);
            if (tracked == null)
            {
                tracked = new Dictionary<FieldDescriptor, Variable>();
                fieldsOnReferenceInstancedTypes.Add(instance, tracked);
            }

            // Make sure we are tracking current field
            if (!tracked.ContainsKey(field))
                tracked.Add(field, new Variable(threadId));

            // Update access information
            var result = validator(currentLocks);
            if (!result)
                RaiseViolationFound($"detected data-race on an instance field {field} on instance {instance}");

            return result;
        }

        private HashSet<object> GetCurrentLocks(int threadId)
        {
            if (!takenLocks.ContainsKey(threadId))
            {
                var newLocksTracker = new HashSet<object>();
                takenLocks.Add(threadId, newLocksTracker);
            }

            return takenLocks[threadId];
        }
    }
}