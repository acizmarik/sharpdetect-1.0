/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Core.Utilities;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace SharpDetect.Plugins.LockSet
{
    public class Variable
    {
        public VariableState State { get; protected set; } = VariableState.Virgin;
        protected int CreatorThreadId { get; set; }
        protected HashSet<object> CandidateLocks { get; set; }
        private static HashSet<object> emptyLocks { get; } = new HashSet<object>();

        public Variable(int threadId)
        {
            CreatorThreadId = threadId;
        }

        public bool TryUpdateRead(int byThread, HashSet<object> currentLocks)
        {
            // Update variable state
            if (State == VariableState.Exclusive && byThread != CreatorThreadId)
                State = VariableState.Shared;

            return CheckVariable(currentLocks);
        }

        public bool TryUpdateWrite(int byThread, HashSet<object> currentLocks)
        {
            // Update variable state
            switch (State)
            {
                case VariableState.Virgin:
                case VariableState.Exclusive:              
                    if (byThread == CreatorThreadId)
                        State = VariableState.Exclusive;
                    else
                        State = VariableState.SharedModified;
                    break;
                case VariableState.Shared:
                    State = VariableState.SharedModified;
                    break;
            }

            return CheckVariable(currentLocks);
        }

        private bool CheckVariable(HashSet<object> currentLocks)
        {
            // Variables in SharedModified state must be checked
            if (State == VariableState.SharedModified)
            {
                LockRefinement(currentLocks);
                if (CheckForRace())
                    return false;
            }

            return true;
        }

        private void LockRefinement(HashSet<object> currentLocks)
        {
            // If there are no locks taken the candidate locks are always empty
            if (currentLocks.Count == 0)
                CandidateLocks = emptyLocks;
            // For the first time just copy locks
            else if (CandidateLocks == null)
                CandidateLocks = new HashSet<object>(currentLocks);
            // Check if refinement makes sense
            else if (CandidateLocks.Count != 0)
                CandidateLocks.IntersectWith(currentLocks);
        }

        private bool CheckForRace()
        {
            // There are no locks guarding access to the variable
            if (CandidateLocks.Count == 0)
                return true;

            return false;
        }
    }
}
