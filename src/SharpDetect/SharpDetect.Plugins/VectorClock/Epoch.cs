/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.Plugins.VectorClock
{
    struct Epoch
    {
        public uint Value;

        public const uint ReadSharedEpoch = 0xFF_FF_FF_FF;
        private const uint ThreadIdMark = 0xFF_00_00_00;      
        private const uint ClockMask = 0x00_FF_FF_FF;
        private const int ThreadShift = 24;

        public int GetThreadId()
            => (int)((Value & ThreadIdMark) >> ThreadShift);

        public int GetClock()
            => (int)(Value & ClockMask);

        internal static void SetThreadId(ref Epoch epoch, int threadId)
        {
            epoch.Value &= ~ThreadIdMark;
            epoch.Value |= (uint)(threadId << ThreadShift);
        }

        internal static void SetClock(ref Epoch epoch, int clock)
        {
            epoch.Value &= ~ClockMask;
            epoch.Value |= (uint)(clock & ClockMask);
        }

        internal static void SetEpoch(ref Epoch epoch, uint newValue)
        {
            epoch.Value = newValue;
        }

        internal static void Increment(ref Epoch epoch)
        {
            epoch.Value++;
        }

        public override string ToString()
        {
            return $"EPOCH={Value};TID={GetThreadId()};CLK={GetClock()}";
        }
    }
}
