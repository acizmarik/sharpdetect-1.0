/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Linq;
using Serilog;

namespace SharpDetect.Plugins.VectorClock
{
    public partial class FastTrack : BasePlugin
    {
        public override string PluginName { get; } = "FastTrack";
        private ConcurrentDictionary<int, ThreadState> threads;
        private ConditionalWeakTable<object, LockState> locks;
        private ConditionalWeakTable<object, Dictionary<FieldDescriptor, VariableState>> instanceFields;
        private ConcurrentDictionary<FieldDescriptor, VariableState> staticFields;
        private ConditionalWeakTable<Array, Dictionary<int, VariableState>> arrayElements;
        private object ftLock = new object();
        // Thread with ManagedId=0 is not used
        private int threadsCount = 0;
        private int highestThreadId = 1;

        public FastTrack()
        {
            threads = new ConcurrentDictionary<int, ThreadState>();
            // Initialize the main thread
            threads.TryAdd(1, new ThreadState(1) { Clock = new List<uint> { 1 << 24, 1 << 24 } });

            locks = new ConditionalWeakTable<object, LockState>();
            instanceFields = new ConditionalWeakTable<object, Dictionary<FieldDescriptor, VariableState>>();
            staticFields = new ConcurrentDictionary<FieldDescriptor, VariableState>();
            arrayElements = new ConditionalWeakTable<Array, Dictionary<int, VariableState>>();
        }

        /// <summary>
        /// Perform data-race detection on read access
        /// </summary>
        /// <param name="variable">State of accessed memory</param>
        /// <param name="thread">State of the thread that performed the read</param>
        /// <returns>True if no data-race detected, false otherwise</returns>
        private bool Read(VariableState variable, ThreadState thread)
        {
            bool valid = true;

            // Same epoch 63.4%
            if (variable.Read.Value == thread.Epoch.Value)
                return true;

            // Check for write-read data-race
            if (variable.Write.Value > thread.Clock[variable.Write.GetThreadId()])
                valid = false;

            // Shared 20.8%
            if (variable.Read.Value == Epoch.ReadSharedEpoch)
            {
                ExtendVectorClockTo(variable.Clock, thread.Clock.Count);
                variable.Clock[thread.ThreadId] = thread.Epoch.Value;
            }
            else
            {
                // Exclusive access 15.7%
                if (variable.Read.Value <= thread.Clock[variable.Read.GetThreadId()])
                    variable.Read = thread.Epoch;
                else
                {
                    // Share 0.1%
                    if (variable.Clock == null)
                    {
                        variable.Clock = new List<uint>();
                        NewVectorClock(variable.Clock, thread.Clock.Count + 1);
                    }
                    variable.Clock[variable.Read.GetThreadId()] = variable.Read.Value;
                    variable.Clock[thread.ThreadId] = thread.Epoch.Value;
                    Epoch.SetEpoch(ref variable.Read, Epoch.ReadSharedEpoch);
                }
            }

            return valid;
        }

        /// <summary>
        /// Perform data-race detection on write access
        /// </summary>
        /// <param name="variable">State of accessed memory</param>
        /// <param name="thread">State of the thread that performed the write</param>
        /// <returns>True if no data-race detected, false otherwise</returns>
        private bool Write(VariableState variable, ThreadState thread)
        {
            bool valid = true;

            // Same epoch 71.0%
            if (variable.Write.Value == thread.Epoch.Value)
                return true;

            // Check for write-write data-race
            if (variable.Write.Value > thread.Clock[variable.Write.GetThreadId()])
                valid = false;

            // Check for read-write data-race
            if (variable.Read.Value != Epoch.ReadSharedEpoch)
            {
                // Shared 28.9%
                if (variable.Read.Value > thread.Clock[variable.Read.GetThreadId()])
                    valid = false;
            }
            else
            {
                // Exclusive 0.1%
                for (var i = 0; i < Math.Min(variable.Clock.Count, threadsCount); i++)
                {
                    if (variable.Clock[i] > thread.Clock[i])
                        valid = false;               
                }
            }

            Epoch.SetEpoch(ref variable.Write, thread.Epoch.Value);
            return valid;
        }

        /// <summary>
        /// Notifies about a thread fork
        /// </summary>
        /// <param name="original">The thread that started another thread</param>
        /// <param name="forked">The new started thread</param>
        private void Fork(ThreadState original, ThreadState forked)
        {
            Interlocked.Increment(ref threadsCount);

            var clockSize = highestThreadId + 1;

            ExtendVectorClockTo(original.Clock, clockSize);
            ExtendVectorClockTo(forked.Clock, clockSize);

            for (var i = 0; i < forked.Clock.Count; i++)
                forked.Clock[i] = Math.Max(original.Clock[i], forked.Clock[i]);
            foreach (var thread in threads.Values)
            {
                for (var i = thread.Clock.Count; i < clockSize; i++)
                {
                    var epoch = (uint)i << 24;
                    if (thread.ThreadId == i)
                        epoch++;

                    thread.Clock.Add(epoch);
                }
            }

            forked.UpdateEpoch();
            original.IncrementEpoch();
        }

        /// <summary>
        /// Notifies about a thread join
        /// </summary>
        /// <param name="original">The thread to which a another thread is being joined</param>
        /// <param name="joined">The joined thread</param>
        private void Join(ThreadState original, ThreadState joined)
        {
            Interlocked.Decrement(ref threadsCount);

            var clockSize = Math.Max(original.Clock.Count, joined.Clock.Count);
            ExtendVectorClockTo(original.Clock, clockSize);
            ExtendVectorClockTo(joined.Clock, clockSize);

            for (var i = 0; i < joined.Clock.Count; i++)
                original.Clock[i] = Math.Max(original.Clock[i], joined.Clock[i]);

            original.UpdateEpoch();
            joined.IncrementEpoch();
        }

        /// <summary>
        /// Notifies about a lock acquire
        /// </summary>
        /// <param name="thread">Thread that acquired a lock</param>
        /// <param name="lock">Lock that was acquired</param>
        private void Acquire(ThreadState thread, LockState @lock)
        {
            var clockSize = Math.Max(thread.Clock.Count, @lock.Clock.Count);
            ExtendVectorClockTo(thread.Clock, clockSize);
            ExtendVectorClockTo(@lock.Clock, clockSize);

            for (var i = 0; i < @lock.Clock.Count; ++i)
                thread.Clock[i] = Math.Max(thread.Clock[i], @lock.Clock[i]);

            thread.UpdateEpoch();
            @lock.Taken = true;
        }

        /// <summary>
        /// Notifies about a lock release
        /// </summary>
        /// <param name="thread">Thread that released a lock</param>
        /// <param name="lock">Lock that was released</param>
        private void Release(ThreadState thread, LockState @lock)
        {
            var clockSize = Math.Max(thread.Clock.Count, @lock.Clock.Count);
            ExtendVectorClockTo(thread.Clock, clockSize);
            ExtendVectorClockTo(@lock.Clock, clockSize);

            for (var i = 0; i < @lock.Clock.Count; i++)
                @lock.Clock[i] = thread.Clock[i];

            thread.IncrementEpoch();
            @lock.Taken = false;
        }

        private void NewVectorClock(List<uint> clock, int size)
        {
            clock.Capacity = size;

            for (uint i = 0; i < size; i++)
                clock.Add(i << 24);
        }

        private void ExtendVectorClockTo(List<uint> clock, int size)
        {
            var extendBy = size - clock.Count;
            if (extendBy <= 0)
                return;

            for (int i = clock.Count; i < size; i++)
                clock.Add((uint)i << 24);
        }
    }
}
