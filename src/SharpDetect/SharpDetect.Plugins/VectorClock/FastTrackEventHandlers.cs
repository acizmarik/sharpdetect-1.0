/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using SharpDetect.Common.Events;
using SharpDetect.Core.Utilities;

namespace SharpDetect.Plugins.VectorClock
{
    public partial class FastTrack
    {
        private ThreadState GetThreadState(int threadId, Thread thread = null)
        {
            highestThreadId = (highestThreadId < threadId) ? threadId : highestThreadId;

            if (!threads.ContainsKey(threadId))
            {
                var newThread = new ThreadState(threadId);
                threads.TryAdd(threadId, newThread);

                if (threads.Count != 1 && thread == null)
                    Fork(threads[1], newThread);
            }

            var threadState = threads[threadId];
            threadState.IncrementEpoch();
            return threads[threadId];
        }

        private VariableState GetArrayElementVariableState(ThreadState thread, bool isWrite, Array array, int index)
        {
            // Get all tracked variables for the given array
            var trackedArrayElements = arrayElements.GetOrCreateValue(array);
            // Get the variable determined by the given index
            if (!trackedArrayElements.ContainsKey(index))
            {
                if (isWrite)
                    trackedArrayElements.Add(index, VariableState.InitFromWrite(thread));
                else
                    trackedArrayElements.Add(index, VariableState.InitFromRead(thread));
            }

            var variable = trackedArrayElements[index];
            return variable;
        }

        private VariableState GetFieldVariableState(ThreadState thread, bool isWrite, FieldDescriptor field, object instance = null)
        {
            // Static field
            if (instance == null)
            {
                var variableState = (isWrite) ? VariableState.InitFromWrite(thread) : VariableState.InitFromRead(thread);
                var trackedStaticField = staticFields.GetOrAdd(field, variableState);
                return trackedStaticField;
            }
            // Instance field
            var result = instanceFields.TryGetValue(instance, out var trackedInstanceFields);
            if (!result)
            {
                trackedInstanceFields = new Dictionary<FieldDescriptor, VariableState>();
                instanceFields.Add(instance, trackedInstanceFields);                
            }
            if (!trackedInstanceFields.ContainsKey(field))
            {
                if (isWrite)
                    trackedInstanceFields.Add(field, VariableState.InitFromWrite(thread));
                else if (!isWrite)
                    trackedInstanceFields.Add(field, VariableState.InitFromRead(thread));
            }

            var variable = trackedInstanceFields[field];
            return variable;
        }

        private LockState GetLockState(ThreadState thread, object lockObj)
        {
            var trackedLock = locks.GetOrCreateValue(lockObj);
            if (trackedLock.Clock == null)
            {
                var clockSize = thread.Clock.Count + 1;
                trackedLock.Clock = new List<uint>(clockSize);
                NewVectorClock(trackedLock.Clock, clockSize);
            }
            return trackedLock;
        }

        public override void LockAcquireReturned(int threadId, MethodDescriptor method, object lockObj, bool result, (int, object)[] parameters)
        {
            // Lock was not acquired
            if (!result)
            {
                lock (ftLock)
                {
                    var thread = GetThreadState(threadId);
                    var @lock = GetLockState(thread, lockObj);
                    // Make sure we processed the Release event if the lock was previously locked
                    while (@lock.Taken)
                        Monitor.Wait(ftLock);

                    Acquire(thread, @lock);
                }
            }

            base.LockAcquireReturned(threadId, method, lockObj, result, parameters);
        }

        public override void LockReleased(int threadId, MethodDescriptor method, object lockObj)
        {
            lock (ftLock)
            {             
                var thread = GetThreadState(threadId);
                var @lock = GetLockState(thread, lockObj);

                Release(thread, @lock);
                Monitor.PulseAll(ftLock);
            }

            base.LockReleased(threadId, method, lockObj);
        }     

        public override void ArrayElementRead(int threadId, MethodDescriptor method, Array array, int index)
        {
            lock (ftLock)
            {
                var thread = GetThreadState(threadId);
                var variable = GetArrayElementVariableState(thread, false, array, index);
                if (!Read(variable, thread))
                    RaiseViolationFound($"detected data-race on array element {array}[{index}]");
            }

            base.ArrayElementRead(threadId, method, array, index);
        }

        public override void ArrayElementWritten(int threadId, MethodDescriptor method, Array array, int index, object value)
        {
            lock (ftLock)
            {
                var thread = GetThreadState(threadId);
                var variable = GetArrayElementVariableState(thread, true, array, index);
                if (!Write(variable, thread))
                    RaiseViolationFound($"detected data-race on array element {array}[{index}]");
            }

            base.ArrayElementWritten(threadId, method, array, index, value);
        }

        public override void FieldRead(int threadId, object instance, FieldDescriptor field)
        {
            if (!field.FieldFlags.HasFlag(FieldFlags.Readonly) && !field.FieldFlags.HasFlag(FieldFlags.ThreadStatic))
            {
                lock (ftLock)
                {
                    var thread = GetThreadState(threadId);
                    var variable = GetFieldVariableState(thread, false, field, instance);
                    if (!Read(variable, thread))
                    {
                        if (instance == null)
                            RaiseViolationFound($"detected data-race on a static field {field}");
                        else
                            RaiseViolationFound($"detected data-race on an instance field {field} on instance {instance}");
                    }
                }
            }

            base.FieldRead(threadId, instance, field);
        }

        public override void FieldWritten(int threadId, object instance, FieldDescriptor field, object value)
        {
            if (!field.FieldFlags.HasFlag(FieldFlags.Readonly) && !field.FieldFlags.HasFlag(FieldFlags.ThreadStatic))
            {
                lock (ftLock)
                {
                    var thread = GetThreadState(threadId);
                    var variable = GetFieldVariableState(thread, true, field, instance);
                    if (!Write(variable, thread))
                    {
                        if (instance == null)
                            RaiseViolationFound($"detected data-race on a static field {field}");
                        else
                            RaiseViolationFound($"detected data-race on an instance field {field} on instance {instance}");
                    }
                }
            }

            base.FieldWritten(threadId, instance, field, value);
        }

        public override void UserThreadStarted(int threadId, Thread thread)
        { 
            lock (ftLock)
            {
                var originalThread = GetThreadState(threadId, thread);
                var newThread = GetThreadState(thread.ManagedThreadId);
                Fork(originalThread, newThread);
            }

            base.UserThreadStarted(threadId, thread);
        }

        public override void UserThreadJoined(int threadId, Thread thread)
        {
            lock (ftLock)
            {
                var originalThread = GetThreadState(threadId);
                var newThread = GetThreadState(thread.ManagedThreadId);
                Join(originalThread, newThread);
            }

            base.UserThreadJoined(threadId, thread);
        }
    }
}
