/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.Plugins.VectorClock
{
    class ThreadState
    {       
        public int ThreadId { get; internal set; }
        internal List<uint> Clock;
        internal Epoch Epoch;

        public ThreadState(int threadId)
        {
            Clock = new List<uint>();
            ThreadId = threadId;
            Epoch.SetEpoch(ref Epoch, ((uint)threadId << 24));
        }

        public void UpdateEpoch()
        {
            Epoch.SetEpoch(ref Epoch, Clock[ThreadId]);
        }

        public void IncrementEpoch()
        {
            Epoch.Increment(ref Epoch);
            Clock[ThreadId] = Epoch.Value;
        }

        public override string ToString()
        {
            var state = (Epoch.Value == Clock[ThreadId]) ? "OK" : "INVALID";
            return $"Thread: [TID={ThreadId}; {Epoch}; STATE={state}]";
        }
    }
}
