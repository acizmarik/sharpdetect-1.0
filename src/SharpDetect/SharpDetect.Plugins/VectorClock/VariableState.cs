/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.Plugins.VectorClock
{
    class VariableState
    {       
        internal Epoch Read;
        internal Epoch Write;
        internal List<uint> Clock;

        public static VariableState InitFromRead(ThreadState thread)
            => new VariableState(thread, false);

        public static VariableState InitFromWrite(ThreadState thread)
            => new VariableState(thread, true);

        private VariableState(ThreadState thread, bool isWrite)
        {
            if (isWrite)
                Write = thread.Epoch;
            else
                Read = thread.Epoch;
        }

        public override string ToString()
        {
            return $"Variable: [Read={Read}; Write={Write}]";
        }
    }
}
