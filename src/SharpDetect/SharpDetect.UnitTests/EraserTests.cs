/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Plugins.LockSet;
using SharpDetect.Core.Utilities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SharpDetect.Common.Events;

namespace SharpDetect.UnitTests
{
    class EraserTests
    {
        internal SignatureResolver Resolver;
        internal Eraser Eraser;
        internal int ViolationsOccurred;

        [SetUp]
        public void Setup()
        {
            Resolver = new SignatureResolver();
            Eraser = new Eraser();
            Eraser.ViolationFound += (sender, msg) => ViolationsOccurred++;
            ViolationsOccurred = 0;
        }

        [Test]
        public void CheckDataRaceOnStaticFields()
        {
            var field = Resolver.ResolveField("System.Int32 Program::Method()", FieldFlags.None);

            // Initialize by thread 0
            Eraser.FieldWritten(0, null, field, 1);
            Assert.AreEqual(0, ViolationsOccurred);

            // Write by thread 1
            Eraser.FieldWritten(1, null, field, 1);
            Assert.AreEqual(1, ViolationsOccurred);
        }

        [Test]
        public void CheckNoDataRaceOnThreadStaticFields()
        {
            var field = Resolver.ResolveField("System.Int32 Program::Method()", FieldFlags.ThreadStatic);

            // Initialize by thread 0
            Eraser.FieldWritten(0, null, field, 1);
            Assert.AreEqual(0, ViolationsOccurred);

            // Write by thread 1
            Eraser.FieldWritten(1, null, field, 1);
            Assert.AreEqual(0, ViolationsOccurred);
        }

        [Test]
        public void CheckDataRaceOnInstanceFields()
        {
            var field = Resolver.ResolveField("System.Int32 Program::Method()", FieldFlags.None);
            var instance = new object();

            // Initialize by thread 0
            Eraser.FieldWritten(0, instance, field, 1);
            Assert.AreEqual(0, ViolationsOccurred);

            // Write by thread 1
            Eraser.FieldWritten(1, instance, field, 1);
            Assert.AreEqual(1, ViolationsOccurred);
        }

        [Test]
        public void CheckNoDataRaceOnDifferentInstances()
        {
            var field = Resolver.ResolveField("System.Int32 Program::Method()", FieldFlags.None);

            // Initialize by thread 0
            Eraser.FieldWritten(0, new object(), field, 1);
            Assert.AreEqual(0, ViolationsOccurred);

            // Write by thread 1
            Eraser.FieldWritten(1, new object(), field, 1);
            Assert.AreEqual(0, ViolationsOccurred);
        }
    }
}
