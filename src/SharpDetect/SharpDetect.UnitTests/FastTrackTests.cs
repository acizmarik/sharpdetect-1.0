/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using NUnit.Framework;
using SharpDetect.Common.Events;
using SharpDetect.Core.Utilities;
using SharpDetect.Plugins.LockSet;
using SharpDetect.Plugins.VectorClock;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.UnitTests
{
    class FastTrackTests
    {
        internal SignatureResolver Resolver;
        internal FastTrack FastTrack;
        internal int ViolationsOccurred;

        [SetUp]
        public void Setup()
        {
            Resolver = new SignatureResolver();
            FastTrack = new FastTrack();
            FastTrack.ViolationFound += (sender, msg) => ViolationsOccurred++;
            ViolationsOccurred = 0;
        }

        [Test]
        public void CheckDataRaceOnStaticFields()
        {
            var field = Resolver.ResolveField("System.Int32 Program::Field", FieldFlags.Static);

            // Initialize by thread 1
            FastTrack.FieldWritten(1, null, field, 11);
            Assert.AreEqual(0, ViolationsOccurred);

            FastTrack.FieldRead(1, null, field);
            Assert.AreEqual(0, ViolationsOccurred);

            // Write by thread 2
            FastTrack.FieldWritten(2, null, field, 11);
            Assert.AreEqual(0, ViolationsOccurred);

            // Write by thread 3
            FastTrack.FieldWritten(3, null, field, 11);
            Assert.AreEqual(1, ViolationsOccurred);
        }

        [Test]
        public void CheckDataRaceOnInstanceFields()
        {
            var field = Resolver.ResolveField("System.Int32 Program::Field", FieldFlags.None);
            var instance = new object();

            // Initialize by thread 1
            FastTrack.FieldWritten(1, instance, field, 11);
            Assert.AreEqual(0, ViolationsOccurred);

            // Exclusive thread 1
            FastTrack.FieldRead(1, instance, field);
            Assert.AreEqual(0, ViolationsOccurred);

            // Exclusive thread 1
            FastTrack.FieldWritten(1, instance, field, 11);
            Assert.AreEqual(0, ViolationsOccurred);

            // Read by thread 2 (shared)
            FastTrack.FieldRead(2, instance, field);
            Assert.AreEqual(0, ViolationsOccurred);

            // Shared (threads 1,2)
            FastTrack.FieldRead(2, instance, field);
            Assert.AreEqual(0, ViolationsOccurred);

            // Shared (threads 1,2)
            FastTrack.FieldWritten(2, instance, field, 11);
            Assert.AreEqual(0, ViolationsOccurred);
            
            // Data-race
            FastTrack.FieldWritten(3, instance, field, 11);
            Assert.AreEqual(1, ViolationsOccurred);
        }

        [Test]
        public void CheckDataRaceOnArrayElements()
        {
            var array = new int[10];
            var index1 = 0;
            var index2 = 1;

            FastTrack.ArrayElementWritten(1, null, array, index1, 11);
            Assert.AreEqual(0, ViolationsOccurred);

            FastTrack.ArrayElementWritten(2, null, array, index2, 11);
            Assert.AreEqual(0, ViolationsOccurred);

            FastTrack.ArrayElementWritten(3, null, array, index2, 11);
            Assert.AreEqual(1, ViolationsOccurred);
        }

        [Test]
        public void CheckNoDataRaceOnLockedAccess()
        {
            var field = Resolver.ResolveField("System.Int32 Program::Field", FieldFlags.None);
            var lockObj = new object();

            // Initialize by thread 1
            FastTrack.LockAcquireReturned(1, null, lockObj, true, null);
            FastTrack.FieldWritten(1, null, field, 11);
            FastTrack.LockReleased(1, null, lockObj);
            Assert.AreEqual(0, ViolationsOccurred);

            FastTrack.LockAcquireReturned(1, null, lockObj, true, null);
            FastTrack.FieldRead(1, null, field);
            FastTrack.LockReleased(1, null, lockObj);
            Assert.AreEqual(0, ViolationsOccurred);

            // Write by thread 2
            FastTrack.LockAcquireReturned(2, null, lockObj, true, null);
            FastTrack.FieldWritten(2, null, field, 11);
            FastTrack.LockReleased(2, null, lockObj);
            Assert.AreEqual(0, ViolationsOccurred);
        }

        [Test]
        public void CheckNoDataRace()
        {
            var field1 = Resolver.ResolveField("System.Int32 Program::Field1)", FieldFlags.Static);
            var field2 = Resolver.ResolveField("System.Int32 Program::Field2)", FieldFlags.Static);
            var lockObj = new object();

            FastTrack.FieldWritten(1, null, field1, 11);
            Assert.AreEqual(0, ViolationsOccurred);

            FastTrack.FieldRead(3, null, field1);
            Assert.AreEqual(0, ViolationsOccurred);

            FastTrack.FieldRead(4, null, field1);
            Assert.AreEqual(0, ViolationsOccurred);

            FastTrack.FieldRead(5, null, field2);
            Assert.AreEqual(0, ViolationsOccurred);

            FastTrack.FieldRead(6, null, field2);
            Assert.AreEqual(0, ViolationsOccurred);
        }
    }
}
