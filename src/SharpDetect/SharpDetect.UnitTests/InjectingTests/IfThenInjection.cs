/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Injector.Utilities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpDetect.UnitTests.InjectingTests
{
    class IfThenInjection
    {
        private MethodDef GetIfThenMethod(
            out Instruction conditionBranch,
            out Instruction ifLabel, 
            out Instruction exitBranch,
            out Instruction elseLabel,
            out Instruction exitLabel)
        {
            /*     [NO OPTIMIZATIONS]
             *     
             *     if (true) { }
             *     else { }
             */
            var method = new MethodDefUser() { Body = new CilBody() };
            var instructions = method.Body.Instructions;         
            ifLabel = Instruction.Create(OpCodes.Nop);
            elseLabel = Instruction.Create(OpCodes.Nop);
            exitLabel = Instruction.Create(OpCodes.Ret);
            conditionBranch = Instruction.Create(OpCodes.Brfalse, elseLabel);
            exitBranch = Instruction.Create(OpCodes.Br, exitLabel);

            // if (true)
            instructions.Add(Instruction.Create(OpCodes.Ldc_I4_1));
            instructions.Add(conditionBranch);
            // { /* IF-BLOCK */ }
            instructions.Add(ifLabel);
            instructions.Add(exitBranch);
            // { /* ELSE-BLOCK */ }
            instructions.Add(elseLabel);
            instructions.Add(exitLabel);

            return method;
        }

        [Test]
        public void InjectAtTheStartOfIfBlock()
        {
            var method = GetIfThenMethod(out var conditionBranch, out var ifLabel, out var exitBranch, out var elseLabel, out var exitLabel);

            var insertedInstruction = Instruction.Create(OpCodes.Nop);
            method.InjectBefore(ifLabel, new List<Instruction>() { insertedInstruction });
            var insertedInstructionIndex = method.Body.Instructions.IndexOf(insertedInstruction);

            // Check that branches were not modified
            Assert.True(conditionBranch.Operand == elseLabel);
            Assert.True(exitBranch.Operand == exitLabel);
        }

        [Test]
        public void InjectAtTheStartOfElseBlock()
        {
            var method = GetIfThenMethod(out var conditionBranch, out var ifLabel, out var exitBranch, out var elseLabel, out var exitLabel);

            var insertedInstruction = Instruction.Create(OpCodes.Nop);
            method.InjectBefore(elseLabel, new List<Instruction>() { insertedInstruction });
            var insertedInstructionIndex = method.Body.Instructions.IndexOf(insertedInstruction);

            // Check that condition branch points to inserted instruction
            Assert.False(conditionBranch.Operand == elseLabel);
            Assert.AreEqual(insertedInstruction, conditionBranch.Operand);
            // Check that other branches were not modified
            Assert.True(exitBranch.Operand == exitLabel);
        }

        [Test]
        public void InjectBeforeExitLabel()
        {
            var method = GetIfThenMethod(out var conditionBranch, out var ifLabel, out var exitBranch, out var elseLabel, out var exitLabel);

            var insertedInstruction = Instruction.Create(OpCodes.Nop);
            method.InjectBefore(exitLabel, new List<Instruction>() { insertedInstruction });
            var insertedInstructionIndex = method.Body.Instructions.IndexOf(insertedInstruction);

            // Check that exit branch points to inserted instruction
            Assert.False(exitBranch.Operand == exitLabel);
            Assert.AreEqual(insertedInstruction, exitBranch.Operand);
            // Check that other branches were not modified
            Assert.True(conditionBranch.Operand == elseLabel);
        }
    }
}
