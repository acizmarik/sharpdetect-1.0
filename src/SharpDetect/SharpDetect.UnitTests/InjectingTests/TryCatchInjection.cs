/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using dnlib.DotNet;
using dnlib.DotNet.Emit;
using SharpDetect.Injector.Utilities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpDetect.UnitTests.InjectingTests
{
    class TryCatchInjection
    {
        private MethodDef GetTryCatchMethod(
            out Instruction tryStartLabel,
            out Instruction tryLeaveLabel,
            out Instruction catchStartLabel,
            out Instruction catchLeaveLabel,
            out Instruction exitLabel,
            out ExceptionHandler handler)
        {
            /*     [NO OPTIMIZATIONS]
             *     
             *     try { }
             *     catch (Exception) { }
             */
            var method = new MethodDefUser() { Body = new CilBody() };
            var instructions = method.Body.Instructions;

            exitLabel = Instruction.Create(OpCodes.Ret);
            tryStartLabel = Instruction.Create(OpCodes.Nop);
            tryLeaveLabel = Instruction.Create(OpCodes.Leave, exitLabel);
            catchStartLabel = Instruction.Create(OpCodes.Pop);
            catchLeaveLabel = Instruction.Create(OpCodes.Leave, exitLabel);

            // try { /* TRY-BLOCK */ }
            instructions.Add(tryStartLabel);
            instructions.Add(Instruction.Create(OpCodes.Nop));
            instructions.Add(tryLeaveLabel);
            // catch { /* CATCH-BLOCK */ }
            instructions.Add(catchStartLabel);
            instructions.Add(Instruction.Create(OpCodes.Nop));
            instructions.Add(Instruction.Create(OpCodes.Nop));
            instructions.Add(catchLeaveLabel);
            // method return
            instructions.Add(exitLabel);

            method.Body.ExceptionHandlers.Add(new ExceptionHandler()
            {
                // Injector ignores this field
                CatchType = null,
                FilterStart = null,
                TryStart = tryStartLabel,
                TryEnd = catchStartLabel,
                HandlerStart = catchStartLabel,
                HandlerEnd = exitLabel
            });

            handler = method.Body.ExceptionHandlers.First();

            return method;
        }

        [Test]
        public void InjectAtTheBeginningOfTryBlock()
        {
            var method = GetTryCatchMethod(
                out var tryStartLabel, 
                out var tryLeaveLabel, 
                out var catchStartLabel, 
                out var catchLeaveLabel, 
                out var exitLabel,
                out var handler);

            var insertedInstruction = Instruction.Create(OpCodes.Nop);
            method.InjectBefore(tryStartLabel, new List<Instruction>() { insertedInstruction });
            var insertedInstructionIndex = method.Body.Instructions.IndexOf(insertedInstruction);

            // Check that branches were not modified
            Assert.True(tryLeaveLabel.Operand == exitLabel);
            Assert.True(catchLeaveLabel.Operand == exitLabel);

            // Check that try is extended
            Assert.True(handler.TryStart != tryStartLabel);
            Assert.AreEqual(insertedInstruction, handler.TryStart);

            // Check that other handler items were not modified
            Assert.True(handler.TryEnd == catchStartLabel);
            Assert.True(handler.HandlerStart == catchStartLabel);
            Assert.True(handler.HandlerEnd == exitLabel);
        }

        [Test]
        public void InjectAtTheBeginningOfCatchBlock()
        {
            var method = GetTryCatchMethod(
                out var tryStartLabel,
                out var tryLeaveLabel,
                out var catchStartLabel,
                out var catchLeaveLabel,
                out var exitLabel,
                out var handler);

            var insertedInstruction = Instruction.Create(OpCodes.Nop);
            method.InjectBefore(catchStartLabel, new List<Instruction>() { insertedInstruction });
            var insertedInstructionIndex = method.Body.Instructions.IndexOf(insertedInstruction);

            // Check that branches were not modified
            Assert.True(tryLeaveLabel.Operand == exitLabel);
            Assert.True(catchLeaveLabel.Operand == exitLabel);

            // Check that catch-begin and try-end is extended
            Assert.True(handler.HandlerStart != catchStartLabel);
            Assert.True(handler.TryEnd != catchStartLabel);
            Assert.AreEqual(insertedInstruction, handler.HandlerStart);
            Assert.AreEqual(insertedInstruction, handler.TryEnd);

            // Check that other handler items were not modified
            Assert.True(handler.TryStart == tryStartLabel);
            Assert.True(handler.HandlerEnd == exitLabel);
        }

        [Test]
        public void InjectBeforeHandlerExit()
        {
            var method = GetTryCatchMethod(
                out var tryStartLabel,
                out var tryLeaveLabel,
                out var catchStartLabel,
                out var catchLeaveLabel,
                out var exitLabel,
                out var handler);

            var insertedInstruction = Instruction.Create(OpCodes.Nop);
            method.InjectBefore(exitLabel, new List<Instruction>() { insertedInstruction });
            var insertedInstructionIndex = method.Body.Instructions.IndexOf(insertedInstruction);

            // Check that branches were modified
            Assert.True(tryLeaveLabel.Operand != exitLabel);
            Assert.True(catchLeaveLabel.Operand != exitLabel);
            Assert.AreEqual(insertedInstruction, tryLeaveLabel.Operand);
            Assert.AreEqual(insertedInstruction, catchLeaveLabel.Operand);
            // Check that handler end is modified
            Assert.True(handler.HandlerEnd != exitLabel);
            Assert.AreEqual(insertedInstruction, handler.HandlerEnd);

            // Check that handler items were not modified
            Assert.True(handler.TryStart == tryStartLabel);
            Assert.True(handler.TryEnd == catchStartLabel);
            Assert.True(handler.HandlerStart == catchStartLabel);            
        }
    }
}
