/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Core.Utilities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using SharpDetect.Core;
using SharpDetect.Common.Events;

namespace SharpDetect.UnitTests.SignatureResolverTests
{
    class SignatureResolverFieldTests
    {
        [Test]
        public void SimpleField()
        {
            var resolver = new SignatureResolver();
            var result = (FieldDescriptor)null;

            Assert.DoesNotThrow(() => result = resolver.ResolveField("System.Int32 Namespace.Program::field", FieldFlags.None));
            Assert.NotNull(result);
            Assert.NotNull(result.DeclaringType);
            Assert.NotNull(result.FieldType);
            Assert.NotNull(result.Name);
            Assert.AreEqual("System.Int32 Namespace.Program::field", result.ToString());
        }

        [Test]
        public void CachedFields()
        {
            var resolver = new SignatureResolver();
            var result1 = (FieldDescriptor)null;
            var result2 = (FieldDescriptor)null;

            Assert.DoesNotThrow(() => result1 = resolver.ResolveField("System.Int32 Program::field", FieldFlags.None));
            Assert.DoesNotThrow(() => result2 = resolver.ResolveField("System.Int32 Program::field", FieldFlags.None));
            Assert.AreEqual(result1, result2);
            Assert.IsTrue(object.ReferenceEquals(result1, result2));
        }

        [Test]
        public void VolatileField()
        {
            var resolver = new SignatureResolver();
            var result = (FieldDescriptor)null;
            var isVolatile = typeof(IsVolatile);

            Assert.DoesNotThrow(()
                => result = resolver.ResolveField($"System.Int32 modreq({isVolatile.FullName}) Namespace.Program::field", FieldFlags.Volatile));
            Assert.NotNull(result);
            Assert.NotNull(result.DeclaringType);
            Assert.NotNull(result.FieldType);
            Assert.NotNull(result.Name);
            Assert.True(result.FieldFlags.HasFlag(FieldFlags.Volatile));
            Assert.AreEqual("System.Int32 Namespace.Program::field", result.ToString());
        }

        [Test]
        public void InvalidFields()
        {
            var resolver = new SignatureResolver();

            Assert.Throws<DynamicAnalysisException>(() => resolver.ResolveField("Program::field", FieldFlags.None));
            Assert.Throws<DynamicAnalysisException>(() => resolver.ResolveField("System.Int32", FieldFlags.None));
            Assert.Throws<DynamicAnalysisException>(() => resolver.ResolveField("System.Int32 Program.field", FieldFlags.None));
        }
    }
}
