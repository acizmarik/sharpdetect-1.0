/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using SharpDetect.Core;
using SharpDetect.Core.Utilities;
using NUnit.Framework;

namespace SharpDetect.UnitTests.SignatureResolverTests
{
    public class SignatureResolverMethodTests
    {
        [Test]
        public void SimpleMethod()
        {
            var resolver = new SignatureResolver();
            var result = (MethodDescriptor)null;

            Assert.DoesNotThrow(() => result = resolver.ResolveMethod("System.Void System.Threading.Monitor::Enter(System.Object,System.Boolean&)"));
            Assert.NotNull(result);
            Assert.NotNull(result.Name);
            Assert.NotNull(result.DeclaringType);
            Assert.NotNull(result.Parameters);
            Assert.AreEqual("System.Threading.Monitor::Enter", $"{result.DeclaringType.Namespace}.{result.DeclaringType.Name}::{result.Name}");
            Assert.AreEqual("System.Void", $"{result.ReturnValue.ToString()}");
            Assert.IsFalse(result.ReturnValue.IsGeneric);
            Assert.IsFalse(result.DeclaringType.IsGeneric);
            
            Assert.AreEqual(2, result.Parameters.Length);
            Assert.IsFalse(result.Parameters[0].IsGeneric);
            Assert.IsFalse(result.Parameters[1].IsGeneric);
            Assert.AreEqual("System.Object", $"{result.Parameters[0].ToString()}");
            Assert.AreEqual("System.Boolean&", $"{result.Parameters[1].ToString()}");
        }

        [Test]
        public void MethodNoParams()
        {
            var resolver = new SignatureResolver();
            var result = (MethodDescriptor)null;

            Assert.DoesNotThrow(() => result = resolver.ResolveMethod("System.Void Namespace::Method()"));
            Assert.NotNull(result.Parameters);
            Assert.AreEqual(0, result.Parameters.Length);
        }

        [Test]
        public void GenericMethod()
        {
            var resolver = new SignatureResolver();
            var result = (MethodDescriptor)null;

            Assert.DoesNotThrow(() => result = resolver.ResolveMethod("System.Void Namespace::Method<System.Int32>()"));
            Assert.IsTrue(result.IsGeneric);
        }

        [Test]
        public void CachedMethods()
        {
            var resolver = new SignatureResolver();
            var result1 = (MethodDescriptor)null;
            var result2 = (MethodDescriptor)null;

            Assert.DoesNotThrow(() => result1 = resolver.ResolveMethod("System.Void System.Threading.Monitor::Enter(System.Object, System.Boolean&)"));
            Assert.DoesNotThrow(() => result2 = resolver.ResolveMethod("System.Void System.Threading.Monitor::Enter(System.Object, System.Boolean&)"));
            Assert.AreEqual(result1, result2);
            Assert.IsTrue(object.ReferenceEquals(result1, result2));
        }

        [Test]
        public void InvalidMethods()
        {
            var resolver = new SignatureResolver();
            Assert.Throws<DynamicAnalysisException>(() => resolver.ResolveMethod("Namespace.Method()"));
            Assert.Throws<DynamicAnalysisException>(() => resolver.ResolveMethod("System.Void Namespace.Method("));
            Assert.Throws<DynamicAnalysisException>(() => resolver.ResolveMethod("System.Void Namespace.Method)"));
            Assert.Throws<DynamicAnalysisException>(() => resolver.ResolveMethod("System.Void Namespace:Method()"));
        }
    }
}