/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Core.Utilities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDetect.UnitTests.SignatureResolverTests
{
    class SignatureResolverParameterTests
    {
        [Test]
        public void SimpleParameter()
        {
            var resolver = new SignatureResolver();
            var result = (ParameterDescriptor)null;

            Assert.DoesNotThrow(() => result = resolver.ResolveParameter("System.Int32"));
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Name);
            Assert.IsNotNull(result.Namespace);
            Assert.IsFalse(result.IsGeneric);
            Assert.IsFalse(result.IsByReference);
            Assert.AreEqual("System.Int32", result.ToString());
        }

        [Test]
        public void CachedParameters()
        {
            var resolver = new SignatureResolver();
            var result1 = (ParameterDescriptor)null;
            var result2 = (ParameterDescriptor)null;

            Assert.DoesNotThrow(() => result1 = resolver.ResolveParameter("System.Int32"));
            Assert.DoesNotThrow(() => result2 = resolver.ResolveParameter("System.Int32"));
            Assert.AreEqual(result1, result2);
            Assert.IsTrue(object.ReferenceEquals(result1, result2));
        }

        [Test]
        public void GenericParameter()
        {
            var resolver = new SignatureResolver();
            var result = (ParameterDescriptor)null;

            Assert.DoesNotThrow(() => result = resolver.ResolveParameter("System.KeyValuePair<System.Int32, System.Int32>"));
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Name);
            Assert.IsNotNull(result.Namespace);
            Assert.IsTrue(result.IsGeneric);
            Assert.IsFalse(result.IsByReference);
            Assert.AreEqual("System.KeyValuePair<?>", result.ToString());
        }

        [Test]
        public void ByRefParameter()
        {
            var resolver = new SignatureResolver();
            var result = (ParameterDescriptor)null;

            Assert.DoesNotThrow(() => result = resolver.ResolveParameter("System.Int32&"));
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Name);
            Assert.IsNotNull(result.Namespace);
            Assert.IsFalse(result.IsGeneric);
            Assert.IsTrue(result.IsByReference);
            Assert.AreEqual("System.Int32&", result.ToString());
        }

        [Test]
        public void ByRefGenericParameter()
        {
            var resolver = new SignatureResolver();
            var result = (ParameterDescriptor)null;

            Assert.DoesNotThrow(() => result = resolver.ResolveParameter("System.KeyValuePair<System.Int32, System.Int32>&"));
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Name);
            Assert.IsNotNull(result.Namespace);
            Assert.IsTrue(result.IsGeneric);
            Assert.IsTrue(result.IsByReference);
            Assert.AreEqual("System.KeyValuePair<?>&", result.ToString());
        }
    }
}
