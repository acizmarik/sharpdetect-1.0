/*
 * Copyright (C) 2020, Andrej Čižmárik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

﻿using SharpDetect.Core.Utilities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using SharpDetect.Core;

namespace SharpDetect.UnitTests.SignatureResolverTests
{
    public class SignatureResolverTypeTests
    {
        [Test]
        public void SimpleType()
        {
            var resolver = new SignatureResolver();
            var result = (TypeDescriptor)null;

            Assert.DoesNotThrow(() => result = resolver.ResolveType("System.Threading.Monitor"));
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Namespace);
            Assert.IsNotNull(result.Name);
            Assert.IsFalse(result.IsGeneric);
            Assert.AreEqual("System.Threading.Monitor", $"{result.Namespace}.{result.Name}");
        }

        [Test]
        public void CachedTypes()
        {
            var resolver = new SignatureResolver();
            var result1 = (TypeDescriptor)null;
            var result2 = (TypeDescriptor)null;

            Assert.DoesNotThrow(() => result1 = resolver.ResolveType("System.Threading.Monitor"));
            Assert.DoesNotThrow(() => result2 = resolver.ResolveType("System.Threading.Monitor"));
            Assert.AreEqual(result1, result2);
            Assert.IsTrue(object.ReferenceEquals(result1, result2));
        }

        [Test]
        public void GenericType()
        {
            var resolver = new SignatureResolver();
            var result = (TypeDescriptor)null;

            Assert.DoesNotThrow(() => result = resolver.ResolveType("Namespace.SubNamespace.Type<System.Int32>"));
            Assert.NotNull(result);
            Assert.IsTrue(result.IsGeneric);
            Assert.AreEqual("Namespace.SubNamespace.Type<?>", $"{result.ToString()}");
        }

        [Test]
        public void InvalidTypes()
        {
            var resolver = new SignatureResolver();
            Assert.Throws<DynamicAnalysisException>(() => resolver.ResolveMethod("InvalidType"));
        }
    }
}
