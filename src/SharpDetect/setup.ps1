<# .SYNOPSIS #>
param ( 
	[parameter(Mandatory=$true)] ## Target analysis directory
	[string]$dir,
	[parameter(Mandatory=$true)]
	[string]$type				 ## Debug/Release
)

$targetPlatform = "netcoreapp2.1"
$libraryFramework = "netstandard2.0"

# Create directories if they do not exist
Write-Host "Creating analysis directory in $dir"
if (![System.IO.Directory]::Exists("$dir")) { [System.IO.Directory]::CreateDirectory("$dir") }
if (![System.IO.Directory]::Exists("$dir\\bin")) { [System.IO.Directory]::CreateDirectory("$dir\\bin") }
if (![System.IO.Directory]::Exists("$dir\\Plugins")) { [System.IO.Directory]::CreateDirectory("$dir\\Plugins") }
if (![System.IO.Directory]::Exists("$dir\\Examples")) { [System.IO.Directory]::CreateDirectory("$dir\\Examples") }

# Try build solution
if (Get-Command dotnet -errorAction SilentlyContinue)
{
	Write-Host "Building solution..."
	dotnet build -c "$type"
}
else
{
	Write-Host "Unable to locate 'dotnet' command"
	Write-Host "Make sure you have correctly installed .NET Core"
	return
}

# Copy files
Write-Host "Copying files..."
Copy-Item -Path ".\\SharpDetect.Console\\bin\\$type\\$targetPlatform\\*" -Destination "$dir\\bin" -recurse -Force
Copy-Item -Path ".\\SharpDetect.Plugins\\bin\\$type\\$libraryFramework\\*" -Destination "$dir\\Plugins" -recurse -Force
Copy-Item -Path ".\\SharpDetect.Examples\\*" -Destination "$dir\\Examples" -recurse -Force

Write-Host "Setup finished."
